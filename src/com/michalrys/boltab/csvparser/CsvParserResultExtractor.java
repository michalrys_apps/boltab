package com.michalrys.boltab.csvparser;

import com.michalrys.boltab.results.CbarResults;
import com.michalrys.boltab.results.FemResults;
import com.michalrys.boltab.results.Rbe2NoMagResults;
import com.michalrys.boltab.results.Rbe2Results;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class CsvParserResultExtractor {
    private String csvFilePath;
    private Map<String, FemResults> results;
    private ResultType resultType;

    public CsvParserResultExtractor() {
        results = new HashMap<>();
    }

    public boolean extractResults(String csvFilePath) {
        this.csvFilePath = updateBackSlashes2Slashes(csvFilePath);

        // extract results from csv file to collector results - set of proper result objects
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(this.csvFilePath))) {

            // check if give file contains a proper results for cbars or rbe2, or other types of results
            String line = bufferedReader.readLine();

            if (line.contains("ResultType,CBAR Shear1,CBAR Shear2,CBAR Axial Force")) {
                System.out.println("[OK] Input data is correct for: " + csvFilePath);
                System.out.println("[OK] CBAR results will be read.");
                resultType = ResultType.CBAR;
            } else if (line.contains("ResultType,Mag,X,Y,Z")) {
                System.out.println("[OK] Input data is correct for: " + csvFilePath);
                System.out.println("[OK] RBE2/RBE3 results will be read - data with Fmag, Fx, Fy, Fz.");
                resultType = ResultType.RBE2;
            } else if (line.contains("ResultType,X,Y,Z")) {
                System.out.println("[OK] Input data is correct for: " + csvFilePath);
                System.out.println("[OK] RBE2/RBE3 results will be read - data without Fmag, with Fx, Fy, Fz.");
                resultType = ResultType.RBE2NOMAG;
            } else {
                System.out.println("\t[!!] Wrong input data for file " + csvFilePath);
                System.out.println("\t[!!] for CBAR export: CBAR Shear1,CBAR Shear2,CBAR Axial Force");
                System.out.println("\t[!!] for RBE2/RBE3 export: ResultType,Mag,X,Y,Z");
                System.out.println("\t[!!] for RBE2/RBE3 export: ResultType,X,Y,Z");
                resultType = ResultType.WRONG_CSV_DATA;
            }

            // in case of other results, program will stop
            if (resultType == ResultType.WRONG_CSV_DATA) {
                System.out.println("\t[!!] This file has been skipped.");
                // do nothing and go back
                return false;
            }

            // so, give file is OK. read packages of results - create cbar/rbe result objects and store them in results
            while ((line = bufferedReader.readLine()) != null) {
                if (line.contains("#")) {
                    continue;
                }
                String currentPartName;
                switch (resultType) {
                    case CBAR:
                        // for cbar 6th location of spring is partName. ! partName must be unique
                        currentPartName = line.split(",")[5];
                        if (!results.keySet().contains(currentPartName)) {
                            results.put(currentPartName, new CbarResults(line));
                        } else {
                            results.get(currentPartName).addResults(line);
                        }
                        break;
                    case RBE2:
                        // for rbe 6th location of spring is partName. ! partName must be unique
                        currentPartName = line.split(",")[5];
                        if (!results.keySet().contains(currentPartName)) {
                            results.put(currentPartName, new Rbe2Results(line));
                        } else {
                            results.get(currentPartName).addResults(line);
                        }
                        break;
                    case RBE2NOMAG:
                        // for rbe 6th location of spring is partName. ! partName must be unique
                        currentPartName = line.split(",")[5];
                        if (!results.keySet().contains(currentPartName)) {
                            results.put(currentPartName, new Rbe2NoMagResults(line));
                        } else {
                            results.get(currentPartName).addResults(line);
                        }
                        break;
                }
            }
        } catch (IOException exc) {
            System.out.println("\t[!!] Error: problem with I/O operation on file: " + this.csvFilePath + ". Ask MR.");
            return false;
        }
//        // set max min F>0min for results //FIXME this must be moved to BoltTables, because Fsheartotal is calc. there
//        for (String partName : results.keySet()) {
//            results.get(partName).setMaxMinAbove0Min();
//        }
        return true;
    }

    public Map<String, FemResults> getResults() {
        return results;
    }

    public ResultType getResultType() {
        return resultType;
    }

    public void clearResults() {
        csvFilePath = "";
        results = new HashMap<>();
        resultType = null;
    }

    public String getFileName() {
        return csvFilePath.replaceAll(".*/", "");
    }

    private String updateBackSlashes2Slashes(String filePath) {
        return filePath.replaceAll("\\\\", "/");
    }

    public void sortResults() {
        List<String> sortedKeys = new ArrayList<>(results.keySet());
        sortedKeys.sort(String::compareTo);

        Map<String, FemResults> sortedResults = new LinkedHashMap<>();

        for(String key: sortedKeys) {
            sortedResults.put(key, results.get(key));
        }
        results = new LinkedHashMap<>(sortedResults);
    }

    // FIXME redundant methods - delete them later

    public int getSize() {
        return results.size();
    }
}
