package com.michalrys.boltab.csvparser;

public enum ResultType {
    CBAR("Results contains cbar shear 1, shear 2, axial forces."),
    RBE2("Results contains rbe2 MAG, X, Y, Z"),
    RBE2NOMAG("Results contains rbe2 X, Y, Z without MAG"),
    WRONG_CSV_DATA("Some other results - program will stop");

    String description;

    ResultType(String description) {
        this.description = description;
    }
}
