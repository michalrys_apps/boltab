package com.michalrys.boltab.bolttables;

import com.michalrys.boltab.boltabsetup.BolTabSetup;
import com.michalrys.boltab.boltabsetup.LoadCaseType;
import com.michalrys.boltab.csvparser.CsvParserResultExtractor;
import com.michalrys.boltab.csvparser.ResultType;
import com.michalrys.boltab.results.CbarResults;
import com.michalrys.boltab.results.Rbe2NoMagResults;
import com.michalrys.boltab.results.Rbe2Results;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class BoltTables {
    private String csvFilePath;
    private String csvFilePathWithoutFileName;
    private Boolean shouldSetPathToSave = true;

    // workbook pools
    private BolTabSetup bolTabSetup = new BolTabSetup();
    private boolean isUserCustonConfigurationRead;
    private Workbook workbook;
    private Sheet sheet;
    private int columnCurrentStartId;
    private int columnCurrentEndId;
    private final int COLUMN_FIRST_START_ID = 0;
    private final int COLUMN_FIRST_END_ID = 11;  // old value 9
    private final int COLUMN_OFFSET = 12; // old value 10
    private ResultType resultType;

    private CellStyle cellStyleBoltName;
    private CellStyle cellStyleBoltNameExtraTables;
    private CellStyle cellStyleBoltNameWarning;
    private CellStyle cellStyleDescriptionWhite;
    private CellStyle cellStyleDescriptionBlank;
    private CellStyle cellStyleHeaderTopFontCenter;
    private CellStyle cellStyleHeaderTopFontLeft;
    private CellStyle cellStyleHeaderLeftCRI;
    private CellStyle cellStyleHeaderLeftMAX;
    private CellStyle cellStyleHeaderLeftMIN;
    private CellStyle cellStyleHeaderLeftAbsMIN;
    private CellStyle cellStyleHeaderLeftCRILoadcase;
    private CellStyle cellStyleHeaderLeftMAXLoadcase;
    private CellStyle cellStyleHeaderLeftMINLoadcase;
    private CellStyle cellStyleHeaderLeftAbsMINLoadcase;
    private CellStyle cellStyleBolt0;
    private CellStyle cellStyleBolt1;
    private CellStyle cellStyleBolt2;
    private CellStyle cellStyleLC1;
    private CellStyle cellStyleLC2;
    private CellStyle cellStyleResults0;
    private CellStyle cellStyleResults1;
    private CellStyle cellStyleResults2;
    private CellStyle cellStyleResultsCriNormal;
    private CellStyle cellStyleResultsCriNormalColored;
    private CellStyle cellStyleResultsCriBold;
    private CellStyle cellStyleResultsCriCriterion;
    private CellStyle cellStyleResultsMaxNormal;
    private CellStyle cellStyleResultsMaxBold;
    private CellStyle cellStyleResultsMaxCriterion;
    private CellStyle cellStyleResultsMinNormal;
    private CellStyle cellStyleResultsMinBold;
    private CellStyle cellStyleResultsMinCriterion;
    private CellStyle cellStyleResultsAbsMinNormal;
    private CellStyle cellStyleResultsAbsMinBold;
    private CellStyle cellStyleResultsAbsMinCriterion;
    private CellStyle cellStyleGraySeparator;
    // workbook

    public BoltTables() {
        // this is initial state for empty excel file
        columnCurrentStartId = COLUMN_FIRST_START_ID;
        columnCurrentEndId = COLUMN_FIRST_END_ID;
    }

    public void setUpWorkbook() {
        String sheetTitle = "Bolts";

        this.workbook = new XSSFWorkbook();

        // sheet with bolt forces tables - all in one sheet
        sheet = workbook.createSheet(sheetTitle);

        // set up cell fonts
        XSSFFont font = ((XSSFWorkbook) this.workbook).createFont();
        font.setFontName("Calibri");
        font.setFontHeightInPoints((short) 11);

        XSSFFont fontBold = ((XSSFWorkbook) this.workbook).createFont();
        fontBold.setFontName("Calibri");
        fontBold.setFontHeightInPoints((short) 11);
        fontBold.setBold(true);

        // set up cell styles
        this.cellStyleBoltName = this.workbook.createCellStyle();            // row 0 : bolt name
        this.cellStyleBoltNameExtraTables = this.workbook.createCellStyle();            // row 0 : bolt name extra tables
        this.cellStyleBoltNameWarning = this.workbook.createCellStyle();     // row 0 : bolt name warning
        this.cellStyleDescriptionWhite = this.workbook.createCellStyle();    // row 1, 2 : short description, statistics
        this.cellStyleDescriptionBlank = this.workbook.createCellStyle();    // row 1, 2 : short description, statistics
        this.cellStyleHeaderTopFontCenter = this.workbook.createCellStyle(); // always first rows in tables
        this.cellStyleHeaderTopFontLeft = this.workbook.createCellStyle();   // always first rows in tables
        this.cellStyleHeaderLeftCRI = this.workbook.createCellStyle();       // always left columns in tables
        this.cellStyleHeaderLeftMAX = this.workbook.createCellStyle();       // always left columns in tables
        this.cellStyleHeaderLeftMIN = this.workbook.createCellStyle();       // always left columns in tables
        this.cellStyleHeaderLeftAbsMIN = this.workbook.createCellStyle();    // always left columns in tables
        this.cellStyleHeaderLeftCRILoadcase = this.workbook.createCellStyle();       // always left columns in tables
        this.cellStyleHeaderLeftMAXLoadcase = this.workbook.createCellStyle();       // always left columns in tables
        this.cellStyleHeaderLeftMINLoadcase = this.workbook.createCellStyle();       // always left columns in tables
        this.cellStyleHeaderLeftAbsMINLoadcase = this.workbook.createCellStyle();    // always left columns in tables
        this.cellStyleBolt0 = this.workbook.createCellStyle();               // result tables - bolt id with bg color 1
        this.cellStyleBolt1 = this.workbook.createCellStyle();               // result tables - bolt id with bg color 1
        this.cellStyleBolt2 = this.workbook.createCellStyle();               // result tables - bolt id with bg color 2
        this.cellStyleLC1 = this.workbook.createCellStyle();                 // result tables - bolt id with bg color 1
        this.cellStyleLC2 = this.workbook.createCellStyle();                 // result tables - bolt id with bg color 2
        this.cellStyleResults0 = this.workbook.createCellStyle();            // result tables - results without bg
        this.cellStyleResults1 = this.workbook.createCellStyle();            // result tables - results with bg color 1
        this.cellStyleResults2 = this.workbook.createCellStyle();            // result tables - results with bg color 2
        this.cellStyleResultsCriNormal = this.workbook.createCellStyle();            // result tables - results for cir
        this.cellStyleResultsCriNormalColored = this.workbook.createCellStyle();            // result tables - results for cir
        this.cellStyleResultsCriBold = this.workbook.createCellStyle();            // result tables - results for cir
        this.cellStyleResultsCriCriterion = this.workbook.createCellStyle();
        this.cellStyleResultsMaxNormal = this.workbook.createCellStyle();
        this.cellStyleResultsMaxBold = this.workbook.createCellStyle();
        this.cellStyleResultsMaxCriterion = this.workbook.createCellStyle();
        this.cellStyleResultsMinNormal = this.workbook.createCellStyle();
        this.cellStyleResultsMinBold = this.workbook.createCellStyle();
        this.cellStyleResultsMinCriterion = this.workbook.createCellStyle();
        this.cellStyleResultsAbsMinNormal = this.workbook.createCellStyle();
        this.cellStyleResultsAbsMinBold = this.workbook.createCellStyle();
        this.cellStyleResultsAbsMinCriterion = this.workbook.createCellStyle();
        this.cellStyleGraySeparator = this.workbook.createCellStyle();       // column small grey separator

        // assign fonts to styles
        cellStyleBoltName.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
        cellStyleBoltName.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleBoltName.setFont(fontBold);
        cellStyleBoltName.setAlignment(HorizontalAlignment.LEFT);

        // bolt name extra tables
        cellStyleBoltNameExtraTables.setFillForegroundColor(IndexedColors.LAVENDER.getIndex());
        cellStyleBoltNameExtraTables.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleBoltNameExtraTables.setFont(fontBold);
        cellStyleBoltNameExtraTables.setAlignment(HorizontalAlignment.LEFT);

        // assign warning when sth went wrong
        cellStyleBoltNameWarning.setFillForegroundColor(IndexedColors.RED.getIndex());
        cellStyleBoltNameWarning.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleBoltNameWarning.setFont(fontBold);
        cellStyleBoltNameWarning.setAlignment(HorizontalAlignment.LEFT);

        cellStyleDescriptionWhite.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        cellStyleDescriptionWhite.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleDescriptionWhite.setFont(font);
        cellStyleDescriptionWhite.setAlignment(HorizontalAlignment.LEFT);

        cellStyleDescriptionBlank.setFont(font);
        cellStyleDescriptionBlank.setAlignment(HorizontalAlignment.LEFT);

        cellStyleGraySeparator.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
        cellStyleGraySeparator.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleGraySeparator.setFont(font);

        cellStyleHeaderTopFontCenter.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        cellStyleHeaderTopFontCenter.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleHeaderTopFontCenter.setAlignment(HorizontalAlignment.CENTER);
        cellStyleHeaderTopFontCenter.setBorderBottom(BorderStyle.THIN);
        cellStyleHeaderTopFontCenter.setBorderTop(BorderStyle.THIN);
        cellStyleHeaderTopFontCenter.setBorderRight(BorderStyle.THIN);
        cellStyleHeaderTopFontCenter.setBorderLeft(BorderStyle.THIN);
        cellStyleHeaderTopFontCenter.setFont(fontBold);

        cellStyleHeaderTopFontLeft.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        cellStyleHeaderTopFontLeft.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleHeaderTopFontLeft.setAlignment(HorizontalAlignment.LEFT);
        cellStyleHeaderTopFontLeft.setBorderBottom(BorderStyle.THIN);
        cellStyleHeaderTopFontLeft.setBorderTop(BorderStyle.THIN);
        cellStyleHeaderTopFontLeft.setBorderRight(BorderStyle.THIN);
        cellStyleHeaderTopFontLeft.setBorderLeft(BorderStyle.THIN);
        cellStyleHeaderTopFontLeft.setFont(fontBold);

        cellStyleHeaderLeftCRI.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
        cellStyleHeaderLeftCRI.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleHeaderLeftCRI.setAlignment(HorizontalAlignment.CENTER);
        cellStyleHeaderLeftCRI.setBorderBottom(BorderStyle.THIN);
        cellStyleHeaderLeftCRI.setBorderTop(BorderStyle.THIN);
        cellStyleHeaderLeftCRI.setBorderRight(BorderStyle.THIN);
        cellStyleHeaderLeftCRI.setBorderLeft(BorderStyle.THIN);
        cellStyleHeaderLeftCRI.setFont(font);

        cellStyleHeaderLeftMAX.setFillForegroundColor(IndexedColors.CORAL.getIndex());
        cellStyleHeaderLeftMAX.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleHeaderLeftMAX.setAlignment(HorizontalAlignment.CENTER);
        cellStyleHeaderLeftMAX.setBorderBottom(BorderStyle.THIN);
        cellStyleHeaderLeftMAX.setBorderTop(BorderStyle.THIN);
        cellStyleHeaderLeftMAX.setBorderRight(BorderStyle.THIN);
        cellStyleHeaderLeftMAX.setBorderLeft(BorderStyle.THIN);
        cellStyleHeaderLeftMAX.setFont(font);

        cellStyleHeaderLeftMIN.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        cellStyleHeaderLeftMIN.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleHeaderLeftMIN.setAlignment(HorizontalAlignment.CENTER);
        cellStyleHeaderLeftMIN.setBorderBottom(BorderStyle.THIN);
        cellStyleHeaderLeftMIN.setBorderTop(BorderStyle.THIN);
        cellStyleHeaderLeftMIN.setBorderRight(BorderStyle.THIN);
        cellStyleHeaderLeftMIN.setBorderLeft(BorderStyle.THIN);
        cellStyleHeaderLeftMIN.setFont(font);

        cellStyleHeaderLeftCRILoadcase.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
        cellStyleHeaderLeftCRILoadcase.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleHeaderLeftCRILoadcase.setAlignment(HorizontalAlignment.CENTER);
        cellStyleHeaderLeftCRILoadcase.setBorderBottom(BorderStyle.THIN);
        cellStyleHeaderLeftCRILoadcase.setBorderTop(BorderStyle.THIN);
        cellStyleHeaderLeftCRILoadcase.setBorderRight(BorderStyle.THIN);
        cellStyleHeaderLeftCRILoadcase.setBorderLeft(BorderStyle.THIN);
        cellStyleHeaderLeftCRILoadcase.setFont(font);
        cellStyleHeaderLeftCRILoadcase.setVerticalAlignment(VerticalAlignment.CENTER);

        cellStyleHeaderLeftMAXLoadcase.setFillForegroundColor(IndexedColors.CORAL.getIndex());
        cellStyleHeaderLeftMAXLoadcase.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleHeaderLeftMAXLoadcase.setAlignment(HorizontalAlignment.CENTER);
        cellStyleHeaderLeftMAXLoadcase.setBorderBottom(BorderStyle.THIN);
        cellStyleHeaderLeftMAXLoadcase.setBorderTop(BorderStyle.THIN);
        cellStyleHeaderLeftMAXLoadcase.setBorderRight(BorderStyle.THIN);
        cellStyleHeaderLeftMAXLoadcase.setBorderLeft(BorderStyle.THIN);
        cellStyleHeaderLeftMAXLoadcase.setFont(font);
        cellStyleHeaderLeftMAXLoadcase.setVerticalAlignment(VerticalAlignment.CENTER);

        cellStyleHeaderLeftMINLoadcase.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        cellStyleHeaderLeftMINLoadcase.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleHeaderLeftMINLoadcase.setAlignment(HorizontalAlignment.CENTER);
        cellStyleHeaderLeftMINLoadcase.setBorderBottom(BorderStyle.THIN);
        cellStyleHeaderLeftMINLoadcase.setBorderTop(BorderStyle.THIN);
        cellStyleHeaderLeftMINLoadcase.setBorderRight(BorderStyle.THIN);
        cellStyleHeaderLeftMINLoadcase.setBorderLeft(BorderStyle.THIN);
        cellStyleHeaderLeftMINLoadcase.setFont(font);
        cellStyleHeaderLeftMINLoadcase.setVerticalAlignment(VerticalAlignment.CENTER);

        cellStyleHeaderLeftAbsMIN.setFillForegroundColor(IndexedColors.AQUA.getIndex());
        cellStyleHeaderLeftAbsMIN.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleHeaderLeftAbsMIN.setAlignment(HorizontalAlignment.CENTER);
        cellStyleHeaderLeftAbsMIN.setBorderBottom(BorderStyle.THIN);
        cellStyleHeaderLeftAbsMIN.setBorderTop(BorderStyle.THIN);
        cellStyleHeaderLeftAbsMIN.setBorderRight(BorderStyle.THIN);
        cellStyleHeaderLeftAbsMIN.setBorderLeft(BorderStyle.THIN);
        cellStyleHeaderLeftAbsMIN.setFont(font);

        cellStyleHeaderLeftAbsMINLoadcase.setFillForegroundColor(IndexedColors.AQUA.getIndex());
        cellStyleHeaderLeftAbsMINLoadcase.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleHeaderLeftAbsMINLoadcase.setAlignment(HorizontalAlignment.CENTER);
        cellStyleHeaderLeftAbsMINLoadcase.setBorderBottom(BorderStyle.THIN);
        cellStyleHeaderLeftAbsMINLoadcase.setBorderTop(BorderStyle.THIN);
        cellStyleHeaderLeftAbsMINLoadcase.setBorderRight(BorderStyle.THIN);
        cellStyleHeaderLeftAbsMINLoadcase.setBorderLeft(BorderStyle.THIN);
        cellStyleHeaderLeftAbsMINLoadcase.setFont(font);
        cellStyleHeaderLeftAbsMINLoadcase.setVerticalAlignment(VerticalAlignment.CENTER);

        cellStyleResults0.setAlignment(HorizontalAlignment.RIGHT);
        cellStyleResults0.setBorderBottom(BorderStyle.THIN);
        cellStyleResults0.setBorderTop(BorderStyle.THIN);
        cellStyleResults0.setBorderRight(BorderStyle.THIN);
        cellStyleResults0.setBorderLeft(BorderStyle.THIN);
        cellStyleResults0.setFont(font);
        cellStyleResults0.setDataFormat(workbook.createDataFormat().getFormat("0"));

        cellStyleBolt0.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        cellStyleBolt0.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleBolt0.setAlignment(HorizontalAlignment.LEFT);
        cellStyleBolt0.setBorderBottom(BorderStyle.THIN);
        cellStyleBolt0.setBorderTop(BorderStyle.THIN);
        cellStyleBolt0.setBorderRight(BorderStyle.THIN);
        cellStyleBolt0.setBorderLeft(BorderStyle.THIN);
        cellStyleBolt0.setFont(fontBold);

        cellStyleBolt1.setFillForegroundColor(IndexedColors.WHITE1.getIndex());
        cellStyleBolt1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleBolt1.setAlignment(HorizontalAlignment.LEFT);
        cellStyleBolt1.setBorderBottom(BorderStyle.THIN);
        cellStyleBolt1.setBorderTop(BorderStyle.THIN);
        cellStyleBolt1.setBorderRight(BorderStyle.THIN);
        cellStyleBolt1.setBorderLeft(BorderStyle.THIN);
        cellStyleBolt1.setFont(fontBold);

        cellStyleBolt2.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
        cellStyleBolt2.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleBolt2.setAlignment(HorizontalAlignment.LEFT);
        cellStyleBolt2.setBorderBottom(BorderStyle.THIN);
        cellStyleBolt2.setBorderTop(BorderStyle.THIN);
        cellStyleBolt2.setBorderRight(BorderStyle.THIN);
        cellStyleBolt2.setBorderLeft(BorderStyle.THIN);
        cellStyleBolt2.setFont(fontBold);

        cellStyleLC1.setFillForegroundColor(IndexedColors.WHITE1.getIndex());
        cellStyleLC1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleLC1.setAlignment(HorizontalAlignment.LEFT);
        cellStyleLC1.setBorderBottom(BorderStyle.THIN);
        cellStyleLC1.setBorderTop(BorderStyle.THIN);
        cellStyleLC1.setBorderRight(BorderStyle.THIN);
        cellStyleLC1.setBorderLeft(BorderStyle.THIN);
        cellStyleLC1.setFont(fontBold);

        cellStyleLC2.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
        cellStyleLC2.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleLC2.setAlignment(HorizontalAlignment.LEFT);
        cellStyleLC2.setBorderBottom(BorderStyle.THIN);
        cellStyleLC2.setBorderTop(BorderStyle.THIN);
        cellStyleLC2.setBorderRight(BorderStyle.THIN);
        cellStyleLC2.setBorderLeft(BorderStyle.THIN);
        cellStyleLC2.setFont(fontBold);

        cellStyleResults1.setFillForegroundColor(IndexedColors.WHITE1.getIndex());
        cellStyleResults1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleResults1.setAlignment(HorizontalAlignment.RIGHT);
        cellStyleResults1.setBorderBottom(BorderStyle.THIN);
        cellStyleResults1.setBorderTop(BorderStyle.THIN);
        cellStyleResults1.setBorderRight(BorderStyle.THIN);
        cellStyleResults1.setBorderLeft(BorderStyle.THIN);
        cellStyleResults1.setFont(font);
        cellStyleResults1.setDataFormat(workbook.createDataFormat().getFormat("0"));

        cellStyleResults2.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());
        cellStyleResults2.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleResults2.setAlignment(HorizontalAlignment.RIGHT);
        cellStyleResults2.setBorderBottom(BorderStyle.THIN);
        cellStyleResults2.setBorderTop(BorderStyle.THIN);
        cellStyleResults2.setBorderRight(BorderStyle.THIN);
        cellStyleResults2.setBorderLeft(BorderStyle.THIN);
        cellStyleResults2.setFont(font);
        cellStyleResults2.setDataFormat(workbook.createDataFormat().getFormat("0"));

        cellStyleResultsCriNormal.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        cellStyleResultsCriNormal.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleResultsCriNormal.setAlignment(HorizontalAlignment.RIGHT);
        cellStyleResultsCriNormal.setBorderBottom(BorderStyle.THIN);
        cellStyleResultsCriNormal.setBorderTop(BorderStyle.THIN);
        cellStyleResultsCriNormal.setBorderRight(BorderStyle.THIN);
        cellStyleResultsCriNormal.setBorderLeft(BorderStyle.THIN);
        cellStyleResultsCriNormal.setFont(font);
        cellStyleResultsCriNormal.setDataFormat(workbook.createDataFormat().getFormat("0"));

        cellStyleResultsCriNormalColored.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
        cellStyleResultsCriNormalColored.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleResultsCriNormalColored.setAlignment(HorizontalAlignment.RIGHT);
        cellStyleResultsCriNormalColored.setBorderBottom(BorderStyle.THIN);
        cellStyleResultsCriNormalColored.setBorderTop(BorderStyle.THIN);
        cellStyleResultsCriNormalColored.setBorderRight(BorderStyle.THIN);
        cellStyleResultsCriNormalColored.setBorderLeft(BorderStyle.THIN);
        cellStyleResultsCriNormalColored.setFont(font);
        cellStyleResultsCriNormalColored.setDataFormat(workbook.createDataFormat().getFormat("0"));

        cellStyleResultsCriBold.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
        cellStyleResultsCriBold.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleResultsCriBold.setAlignment(HorizontalAlignment.RIGHT);
        cellStyleResultsCriBold.setBorderBottom(BorderStyle.THIN);
        cellStyleResultsCriBold.setBorderTop(BorderStyle.THIN);
        cellStyleResultsCriBold.setBorderRight(BorderStyle.THIN);
        cellStyleResultsCriBold.setBorderLeft(BorderStyle.THIN);
        cellStyleResultsCriBold.setFont(fontBold);
        cellStyleResultsCriBold.setDataFormat(workbook.createDataFormat().getFormat("0"));

        cellStyleResultsCriCriterion.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        cellStyleResultsCriCriterion.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleResultsCriCriterion.setAlignment(HorizontalAlignment.CENTER);
        cellStyleResultsCriCriterion.setVerticalAlignment(VerticalAlignment.CENTER);
        cellStyleResultsCriCriterion.setBorderBottom(BorderStyle.THIN);
        cellStyleResultsCriCriterion.setBorderTop(BorderStyle.THIN);
        cellStyleResultsCriCriterion.setBorderRight(BorderStyle.THIN);
        cellStyleResultsCriCriterion.setBorderLeft(BorderStyle.THIN);
        cellStyleResultsCriCriterion.setFont(font);
        cellStyleResultsCriCriterion.setDataFormat(workbook.createDataFormat().getFormat("0"));

        cellStyleResultsMaxNormal.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        cellStyleResultsMaxNormal.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleResultsMaxNormal.setAlignment(HorizontalAlignment.RIGHT);
        cellStyleResultsMaxNormal.setBorderBottom(BorderStyle.THIN);
        cellStyleResultsMaxNormal.setBorderTop(BorderStyle.THIN);
        cellStyleResultsMaxNormal.setBorderRight(BorderStyle.THIN);
        cellStyleResultsMaxNormal.setBorderLeft(BorderStyle.THIN);
        cellStyleResultsMaxNormal.setFont(font);
        cellStyleResultsMaxNormal.setDataFormat(workbook.createDataFormat().getFormat("0"));

        cellStyleResultsMaxBold.setFillForegroundColor(IndexedColors.CORAL.getIndex());
        cellStyleResultsMaxBold.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleResultsMaxBold.setAlignment(HorizontalAlignment.RIGHT);
        cellStyleResultsMaxBold.setBorderBottom(BorderStyle.THIN);
        cellStyleResultsMaxBold.setBorderTop(BorderStyle.THIN);
        cellStyleResultsMaxBold.setBorderRight(BorderStyle.THIN);
        cellStyleResultsMaxBold.setBorderLeft(BorderStyle.THIN);
        cellStyleResultsMaxBold.setFont(fontBold);
        cellStyleResultsMaxBold.setDataFormat(workbook.createDataFormat().getFormat("0"));

        cellStyleResultsMaxCriterion.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        cellStyleResultsMaxCriterion.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleResultsMaxCriterion.setAlignment(HorizontalAlignment.CENTER);
        cellStyleResultsMaxCriterion.setVerticalAlignment(VerticalAlignment.CENTER);
        cellStyleResultsMaxCriterion.setBorderBottom(BorderStyle.THIN);
        cellStyleResultsMaxCriterion.setBorderTop(BorderStyle.THIN);
        cellStyleResultsMaxCriterion.setBorderRight(BorderStyle.THIN);
        cellStyleResultsMaxCriterion.setBorderLeft(BorderStyle.THIN);
        cellStyleResultsMaxCriterion.setFont(font);
        cellStyleResultsMaxCriterion.setDataFormat(workbook.createDataFormat().getFormat("0"));

        cellStyleResultsMinNormal.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        cellStyleResultsMinNormal.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleResultsMinNormal.setAlignment(HorizontalAlignment.RIGHT);
        cellStyleResultsMinNormal.setBorderBottom(BorderStyle.THIN);
        cellStyleResultsMinNormal.setBorderTop(BorderStyle.THIN);
        cellStyleResultsMinNormal.setBorderRight(BorderStyle.THIN);
        cellStyleResultsMinNormal.setBorderLeft(BorderStyle.THIN);
        cellStyleResultsMinNormal.setFont(font);
        cellStyleResultsMinNormal.setDataFormat(workbook.createDataFormat().getFormat("0"));

        cellStyleResultsMinBold.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        cellStyleResultsMinBold.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleResultsMinBold.setAlignment(HorizontalAlignment.RIGHT);
        cellStyleResultsMinBold.setBorderBottom(BorderStyle.THIN);
        cellStyleResultsMinBold.setBorderTop(BorderStyle.THIN);
        cellStyleResultsMinBold.setBorderRight(BorderStyle.THIN);
        cellStyleResultsMinBold.setBorderLeft(BorderStyle.THIN);
        cellStyleResultsMinBold.setFont(fontBold);
        cellStyleResultsMinBold.setDataFormat(workbook.createDataFormat().getFormat("0"));

        cellStyleResultsMinCriterion.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        cellStyleResultsMinCriterion.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleResultsMinCriterion.setAlignment(HorizontalAlignment.CENTER);
        cellStyleResultsMinCriterion.setVerticalAlignment(VerticalAlignment.CENTER);
        cellStyleResultsMinCriterion.setBorderBottom(BorderStyle.THIN);
        cellStyleResultsMinCriterion.setBorderTop(BorderStyle.THIN);
        cellStyleResultsMinCriterion.setBorderRight(BorderStyle.THIN);
        cellStyleResultsMinCriterion.setBorderLeft(BorderStyle.THIN);
        cellStyleResultsMinCriterion.setFont(font);
        cellStyleResultsMinCriterion.setDataFormat(workbook.createDataFormat().getFormat("0"));

        cellStyleResultsAbsMinNormal.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        cellStyleResultsAbsMinNormal.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleResultsAbsMinNormal.setAlignment(HorizontalAlignment.RIGHT);
        cellStyleResultsAbsMinNormal.setBorderBottom(BorderStyle.THIN);
        cellStyleResultsAbsMinNormal.setBorderTop(BorderStyle.THIN);
        cellStyleResultsAbsMinNormal.setBorderRight(BorderStyle.THIN);
        cellStyleResultsAbsMinNormal.setBorderLeft(BorderStyle.THIN);
        cellStyleResultsAbsMinNormal.setFont(font);
        cellStyleResultsAbsMinNormal.setDataFormat(workbook.createDataFormat().getFormat("0"));

        cellStyleResultsAbsMinBold.setFillForegroundColor(IndexedColors.AQUA.getIndex());
        cellStyleResultsAbsMinBold.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleResultsAbsMinBold.setAlignment(HorizontalAlignment.RIGHT);
        cellStyleResultsAbsMinBold.setBorderBottom(BorderStyle.THIN);
        cellStyleResultsAbsMinBold.setBorderTop(BorderStyle.THIN);
        cellStyleResultsAbsMinBold.setBorderRight(BorderStyle.THIN);
        cellStyleResultsAbsMinBold.setBorderLeft(BorderStyle.THIN);
        cellStyleResultsAbsMinBold.setFont(fontBold);
        cellStyleResultsAbsMinBold.setDataFormat(workbook.createDataFormat().getFormat("0"));

        cellStyleResultsAbsMinCriterion.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        cellStyleResultsAbsMinCriterion.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyleResultsAbsMinCriterion.setAlignment(HorizontalAlignment.CENTER);
        cellStyleResultsAbsMinCriterion.setVerticalAlignment(VerticalAlignment.CENTER);
        cellStyleResultsAbsMinCriterion.setBorderBottom(BorderStyle.THIN);
        cellStyleResultsAbsMinCriterion.setBorderTop(BorderStyle.THIN);
        cellStyleResultsAbsMinCriterion.setBorderRight(BorderStyle.THIN);
        cellStyleResultsAbsMinCriterion.setBorderLeft(BorderStyle.THIN);
        cellStyleResultsAbsMinCriterion.setFont(font);
        cellStyleResultsAbsMinCriterion.setDataFormat(workbook.createDataFormat().getFormat("0"));
    }

    public int fillWorkBookWithData(int currentOffset, String csvFilePath, CsvParserResultExtractor csvParser) {
        this.csvFilePath = updateBackSlashes2Slashes(csvFilePath); //FIXME this is redundant - delete it later
        resultType = csvParser.getResultType();

        // if results are ok, do not set path to save file again
        if (!resultType.equals(ResultType.WRONG_CSV_DATA) && shouldSetPathToSave) {
            csvFilePathWithoutFileName = getFilePathWithoutFileName(this.csvFilePath);
            shouldSetPathToSave = false;
        }

        if (!isUserCustonConfigurationRead) {
            bolTabSetup.read(this.csvFilePath);
            isUserCustonConfigurationRead = true;
        }

        // generate bolt tables for given parsed data (several, different kind, f.e.: CBAR, RBE2, ...)
        for (String partName : csvParser.getResults().keySet()) {
            // set friction coefficient
            if (bolTabSetup.getPartNamesForOtherFrictionCoefficient() != null &&
                    bolTabSetup.getPartNamesForOtherFrictionCoefficient().contains(partName) &&
                    bolTabSetup.getIsOtherFrictionCoefficientUsed()) {
                csvParser.getResults().get(partName).setFrictionCoefficient(bolTabSetup.getOtherFrictionCoefficient());
            } else {
                csvParser.getResults().get(partName).setFrictionCoefficient(bolTabSetup.getFrictionCoefficient());
            }

            generateColumnsSize(currentOffset);
            generateStatistics(partName, currentOffset, csvParser);

            // additional calculations on data according to user defined / default settings
            csvParser.getResults().get(partName).calculateCriFactor();
            csvParser.getResults().get(partName).setMaxMinAbove0Min();

            switch (resultType) {
                case CBAR:
                    generateSummaryTablesCBAR(partName, currentOffset, csvParser);
                    generateResultsTableCBAR(partName, currentOffset, csvParser);
                    break;
                case RBE2:
                    generateSummaryTablesRBE2(partName, currentOffset, csvParser);
                    generateResultsTableRBE2(partName, currentOffset, csvParser);
                    break;
                case RBE2NOMAG:
                    generateSummaryTablesRBE2NoMag(partName, currentOffset, csvParser);
                    generateResultsTableRBE2NoMag(partName, currentOffset, csvParser);
                    break;
            }
            currentOffset++;

            // additional tables if they were defined in settings
            if (bolTabSetup.getAreExtraTablesGenerated() && bolTabSetup.getPartNamesForExtraTables().contains(partName)) {
                // generate extra tables
                generateColumnsSize(currentOffset);
                generateStatisticsExtraTables(partName, currentOffset, csvParser);

                switch (resultType) {
                    case CBAR: //TODO here is problem
                        int currentRow = generateExtraSummaryCBAR(partName, currentOffset, csvParser);
                        currentRow = generateExtraSummariesCriMaxMinAbsCBAR(partName, currentOffset, csvParser);
                        currentRow = generateExtraResultsTableCBAR(LoadCaseType.STATIC, currentRow, partName, currentOffset, csvParser);
                        currentRow = generateExtraResultsTableCBAR(LoadCaseType.CRASH, currentRow, partName, currentOffset, csvParser);
                        generateExtraResultsTableCBAR(LoadCaseType.FATIGUE, currentRow, partName, currentOffset, csvParser);
                        break;
                    case RBE2:
                        //TODO later
                        break;
                    case RBE2NOMAG:
                        //TODO later
                        break;
                }
                currentOffset++;
            }
        }
        return currentOffset;
    }

    private void generateColumnsSize(int currentOffset) {
        int columnId = columnCurrentStartId + (COLUMN_OFFSET * currentOffset);

        // set up columns dimensions and small column as separator
        sheet.setColumnWidth(columnId, 2425);   // column 0 : blank additional
        sheet.setColumnWidth(++columnId, 2425); // column 1 : blank additional
        sheet.setColumnWidth(++columnId, 2425); // column 2 : blank - BOLT
        sheet.setColumnWidth(++columnId, 2425); // column 3 : CRI - MAX - MIN - LC
        sheet.setColumnWidth(++columnId, 2425); // column 4 : FX
        sheet.setColumnWidth(++columnId, 2425); // column 5 : FY
        sheet.setColumnWidth(++columnId, 2425); // column 6 : FZ
        sheet.setColumnWidth(++columnId, 2425); // column 7 : FYZ
        sheet.setColumnWidth(++columnId, 2425); // column 8 : blank - description
        sheet.setColumnWidth(++columnId, 2425); // column 9 : blank
        sheet.setColumnWidth(++columnId, 2425); // column 10 : blank additional
        sheet.setColumnWidth(++columnId, 770);  // column 11 : blank small grey separator
        columnCurrentEndId = columnId;
        sheet.setDefaultColumnStyle(columnCurrentEndId, cellStyleGraySeparator);
    }

    private void generateStatistics(String partName, int currentOffset, CsvParserResultExtractor csvParser) {
        int cellFirst = columnCurrentStartId + (COLUMN_OFFSET * currentOffset);
        int cellLast = cellFirst + COLUMN_OFFSET - 1;

        String boltName = partName;
        String fileName = csvParser.getFileName();
        String timeStamp = LocalDate.now().toString() + " " + LocalTime.now().toString();

        // current row and cell in row to write data
        Cell cell;
        Row row;
        CellRangeAddress mergedCells;

        // --- row 0: BASICS bolt name
        if (sheet.getRow(0) == null) {
            row = sheet.createRow(0);
        } else {
            row = sheet.getRow(0);
        }
        cell = row.createCell(cellFirst);
        cell.setCellValue(boltName);
        cell.setCellStyle(cellStyleBoltName);
        // if there is wrong results - N/A -> give warning - red bgcolor
        if (csvParser.getResults().get(partName).isNAasValue()) {
            cell.setCellStyle(cellStyleBoltNameWarning);
        }

        mergedCells = new CellRangeAddress(0, 0, cellFirst, cellLast - 1);
        sheet.addMergedRegion(mergedCells);

        // --- row 1: statistics -> file name
        if (sheet.getRow(1) == null) {
            row = sheet.createRow(1);
        } else {
            row = sheet.getRow(1);
        }
        cell = row.createCell(cellFirst);
        cell.setCellValue("File: " + fileName);
        cell.setCellStyle(cellStyleDescriptionWhite);
        for (int i = cellFirst + 1; i < cellLast; i++) {
            cell = row.createCell(i);
            cell.setCellStyle(cellStyleDescriptionWhite);
        }

        // --- row 2: statistics -> time stamp
        if (sheet.getRow(2) == null) {
            row = sheet.createRow(2);
        } else {
            row = sheet.getRow(2);
        }
        cell = row.createCell(cellFirst);
        cell.setCellValue("Generated: " + timeStamp);
        cell.setCellStyle(cellStyleDescriptionWhite);
        for (int i = cellFirst + 1; i < cellLast; i++) {
            cell = row.createCell(i);
            cell.setCellStyle(cellStyleDescriptionWhite);
        }

        // --- row 3: statistics -> friction coefficient
        if (sheet.getRow(3) == null) {
            row = sheet.createRow(3);
        } else {
            row = sheet.getRow(3);
        }
        cell = row.createCell(cellFirst);
        cell.setCellValue("Friction coefficient for cri factor is: "
                + csvParser.getResults().get(partName).getFrictionCoefficient());
        cell.setCellStyle(cellStyleDescriptionWhite);
        for (int i = cellFirst + 1; i < cellLast; i++) {
            cell = row.createCell(i);
            cell.setCellStyle(cellStyleDescriptionWhite);
        }

        // --- row 4: statistics -> empty so far, free for user comments
        if (sheet.getRow(4) == null) {
            row = sheet.createRow(4);
        } else {
            row = sheet.getRow(4);
        }
        for (int i = cellFirst; i < cellLast; i++) {
            cell = row.createCell(i);
            cell.setCellStyle(cellStyleDescriptionWhite);
        }
        // if there is wrong results - N/A -> give warning
        if (csvParser.getResults().get(partName).isNAasValue()) {
            cell = row.createCell(cellFirst);
            cell.setCellValue("Wrong results in csv - there was N/A. Please export again. Nothing was done.");
            cell.setCellStyle(cellStyleBoltNameWarning);

            for (int i = cellFirst + 1; i < cellLast; i++) {
                cell = row.createCell(i);
                cell.setCellStyle(cellStyleBoltNameWarning);
            }
        }
    }

    private void generateStatisticsExtraTables(String partName, int currentOffset, CsvParserResultExtractor csvParser) {
        int cellFirst = columnCurrentStartId + (COLUMN_OFFSET * currentOffset);
        int cellLast = cellFirst + COLUMN_OFFSET - 1;

        String boltName = partName;
        String fileName = csvParser.getFileName();
        String timeStamp = LocalDate.now().toString() + " " + LocalTime.now().toString();

        // current row and cell in row to write data
        Cell cell;
        Row row;
        CellRangeAddress mergedCells;

        // --- row 0: BASICS bolt name
        if (sheet.getRow(0) == null) {
            row = sheet.createRow(0);
        } else {
            row = sheet.getRow(0);
        }
        cell = row.createCell(cellFirst);
        cell.setCellValue(boltName + " - EXTRA TABLES");
        cell.setCellStyle(cellStyleBoltNameExtraTables);
        // if there is wrong results - N/A -> give warning - red bgcolor
        if (csvParser.getResults().get(partName).isNAasValue()) {
            cell.setCellStyle(cellStyleBoltNameWarning);
        }

        mergedCells = new CellRangeAddress(0, 0, cellFirst, cellLast - 1);
        sheet.addMergedRegion(mergedCells);

        // --- row 1: statistics -> file name
        if (sheet.getRow(1) == null) {
            row = sheet.createRow(1);
        } else {
            row = sheet.getRow(1);
        }
        cell = row.createCell(cellFirst);
        cell.setCellValue("File: " + fileName);
        cell.setCellStyle(cellStyleDescriptionWhite);
        for (int i = cellFirst + 1; i < cellLast; i++) {
            cell = row.createCell(i);
            cell.setCellStyle(cellStyleDescriptionWhite);
        }

        // --- row 2: statistics -> time stamp
        if (sheet.getRow(2) == null) {
            row = sheet.createRow(2);
        } else {
            row = sheet.getRow(2);
        }
        cell = row.createCell(cellFirst);
        cell.setCellValue("Generated: " + timeStamp);
        cell.setCellStyle(cellStyleDescriptionWhite);
        for (int i = cellFirst + 1; i < cellLast; i++) {
            cell = row.createCell(i);
            cell.setCellStyle(cellStyleDescriptionWhite);
        }

        // --- row 3: statistics -> friction coefficient
        if (sheet.getRow(3) == null) {
            row = sheet.createRow(3);
        } else {
            row = sheet.getRow(3);
        }
        cell = row.createCell(cellFirst);
        cell.setCellValue("Friction coefficient for cri factor is: "
                + csvParser.getResults().get(partName).getFrictionCoefficient());
        cell.setCellStyle(cellStyleDescriptionWhite);
        for (int i = cellFirst + 1; i < cellLast; i++) {
            cell = row.createCell(i);
            cell.setCellStyle(cellStyleDescriptionWhite);
        }

        // --- row 4: statistics -> empty so far, free for user comments
        if (sheet.getRow(4) == null) {
            row = sheet.createRow(4);
        } else {
            row = sheet.getRow(4);
        }
        for (int i = cellFirst; i < cellLast; i++) {
            cell = row.createCell(i);
            cell.setCellStyle(cellStyleDescriptionWhite);
        }
        // if there is wrong results - N/A -> give warning
        if (csvParser.getResults().get(partName).isNAasValue()) {
            cell = row.createCell(cellFirst);
            cell.setCellValue("Wrong results in csv - there was N/A. Please export again. Nothing was done.");
            cell.setCellStyle(cellStyleBoltNameWarning);

            for (int i = cellFirst + 1; i < cellLast; i++) {
                cell = row.createCell(i);
                cell.setCellStyle(cellStyleBoltNameWarning);
            }
        }
    }

    private void generateSummaryTablesCBAR(String partName, int currentOffset, CsvParserResultExtractor csvParser) {
        int cellFirst = columnCurrentStartId + (COLUMN_OFFSET * currentOffset);
        int cellLast = cellFirst + COLUMN_OFFSET - 1; // currently not in use, delete later

        // --- row 5-10: CRI table - headers, values
        String force1Name = "Fax [N]";
        String force2Name = "Fsh1 [N]";
        String force3Name = "Fsh2 [N]";
        String force4Name = "Fsht [N]";

        Double firstCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getCriFaxial();
        Double firstCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getCriFshear1();
        Double firstCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getCriFshear2();
        Double firstCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getCriFshearTotal();

        Integer firstCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getCriLCId();
        Integer firstCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getCriBoltId();
        Double firstCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getCriFactor();

        String firstCriterionName = "CRI";

        int rowStart = 5;
        createTableCriticalBolt(
                sheet, rowStart, cellFirst,
                cellStyleHeaderTopFontCenter,
                cellStyleResultsCriNormal, cellStyleResultsCriBold, cellStyleResultsCriCriterion,
                force1Name, force2Name, force3Name, force4Name,
                firstCriterionBoltId, firstCriterionLCid,
                firstCriterionForce1, firstCriterionForce2, firstCriterionForce3, firstCriterionForce4,
                firstCriterionCriFactor, firstCriterionName
        );

        // --- row 9-13: MAX table - headers, values
        firstCriterionName = "MAX";
        firstCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFaxial();
        firstCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFaxial_shear1();
        firstCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFaxial_shear2();
        firstCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFaxial_shearTotal();
        firstCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMaxFaxialLcId();
        firstCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMaxFaxialBoltId();
        firstCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMaxFaxialCriFactor();

        String secondCriterionName = "MAX";
        Double secondCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshear1_axial();
        Double secondCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshear1();
        Double secondCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshear1_shear2();
        Double secondCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshear1_shearTotal();
        Integer secondCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshear1LcId();
        Integer secondCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshear1BoltId();
        Double secondCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshear1CriFactor();

        String thirdCriterionName = "MAX";
        Double thirdCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshear2_axial();
        Double thirdCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshear2_shear1();
        Double thirdCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshear2();
        Double thirdCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshear2_shearTotal();
        Integer thirdCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshear2LcId();
        Integer thirdCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshear2BoltId();
        Double thirdCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshear2CriFactor();

        String fourthCriterionName = "MAX";
        Double fourthCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshearTotal_axial();
        Double fourthCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshearTotal_shear1();
        Double fourthCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshearTotal_shear2();
        Double fourthCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshearTotal();
        Integer fourthCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshear2LcId();
        Integer fourthCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshear2BoltId();
        Double fourthCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshear2CriFactor();

        rowStart = 9;
        createTableByCriteriumBolt(
                sheet, rowStart, cellFirst,
                cellStyleHeaderTopFontCenter,

                cellStyleResultsMaxNormal,
                cellStyleResultsMaxBold, cellStyleResultsMaxCriterion,
                force1Name, force2Name, force3Name, force4Name,
                firstCriterionBoltId, firstCriterionLCid,
                firstCriterionForce1, firstCriterionForce2, firstCriterionForce3, firstCriterionForce4,
                firstCriterionCriFactor, firstCriterionName,


                cellStyleResultsMaxNormal,
                cellStyleResultsMaxBold, cellStyleResultsMaxCriterion,
                secondCriterionBoltId, secondCriterionLCid,
                secondCriterionForce1, secondCriterionForce2, secondCriterionForce3, secondCriterionForce4,
                secondCriterionCriFactor, secondCriterionName,

                cellStyleResultsMaxNormal,
                cellStyleResultsMaxBold, cellStyleResultsMaxCriterion,
                thirdCriterionBoltId, thirdCriterionLCid,
                thirdCriterionForce1, thirdCriterionForce2, thirdCriterionForce3, thirdCriterionForce4,
                thirdCriterionCriFactor, thirdCriterionName,

                cellStyleResultsMaxNormal,
                cellStyleResultsMaxBold, cellStyleResultsMaxCriterion,
                fourthCriterionBoltId, fourthCriterionLCid,
                fourthCriterionForce1, fourthCriterionForce2, fourthCriterionForce3, fourthCriterionForce4,
                fourthCriterionCriFactor, fourthCriterionName
        );

        // --- row 16-21: MIN table - headers, values
        firstCriterionName = "MIN";
        firstCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMinFaxial();
        firstCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMinFaxial_shear1();
        firstCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMinFaxial_shear2();
        firstCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMinFaxial_shearTotal();
        firstCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMinFaxialLcId();
        firstCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMinFaxialBoltId();
        firstCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMinFaxialCriFactor();

        secondCriterionName = "MIN";
        secondCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMinFshear1_axial();
        secondCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMinFshear1();
        secondCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMinFshear1_shear2();
        secondCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMinFshear1_shearTotal();
        secondCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMinFshear1LcId();
        secondCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMinFshear1BoltId();
        secondCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMinFshear1CriFactor();

        thirdCriterionName = "MIN";
        thirdCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMinFshear2_axial();
        thirdCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMinFshear2_shear1();
        thirdCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMinFshear2();
        thirdCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMinFshear2_shearTotal();
        thirdCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMinFshear2LcId();
        thirdCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMinFshear2BoltId();
        thirdCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMinFshear2CriFactor();

        fourthCriterionName = "MIN";
        fourthCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMinFshearTotal_axial();
        fourthCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMinFshearTotal_shear1();
        fourthCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMinFshearTotal_shear2();
        fourthCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMinFshearTotal();
        fourthCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMinFshear2LcId();
        fourthCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMinFshear2BoltId();
        fourthCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMinFshear2CriFactor();

        rowStart = 16;
        createTableByCriteriumBolt(
                sheet, rowStart, cellFirst,
                cellStyleHeaderTopFontCenter,

                cellStyleResultsMinNormal,
                cellStyleResultsMinBold, cellStyleResultsMinCriterion,
                force1Name, force2Name, force3Name, force4Name,
                firstCriterionBoltId, firstCriterionLCid,
                firstCriterionForce1, firstCriterionForce2, firstCriterionForce3, firstCriterionForce4,
                firstCriterionCriFactor, firstCriterionName,


                cellStyleResultsMinNormal,
                cellStyleResultsMinBold, cellStyleResultsMinCriterion,
                secondCriterionBoltId, secondCriterionLCid,
                secondCriterionForce1, secondCriterionForce2, secondCriterionForce3, secondCriterionForce4,
                secondCriterionCriFactor, secondCriterionName,

                cellStyleResultsMinNormal,
                cellStyleResultsMinBold, cellStyleResultsMinCriterion,
                thirdCriterionBoltId, thirdCriterionLCid,
                thirdCriterionForce1, thirdCriterionForce2, thirdCriterionForce3, thirdCriterionForce4,
                thirdCriterionCriFactor, thirdCriterionName,

                cellStyleResultsMinNormal,
                cellStyleResultsMinBold, cellStyleResultsMinCriterion,
                fourthCriterionBoltId, fourthCriterionLCid,
                fourthCriterionForce1, fourthCriterionForce2, fourthCriterionForce3, fourthCriterionForce4,
                fourthCriterionCriFactor, fourthCriterionName
        );

        // --- row 23-27: ABS MIN table - headers, values
        firstCriterionName = "A MIN";
        firstCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFaxial();
        firstCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFaxial_shear1();
        firstCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFaxial_shear2();
        firstCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFaxial_shearTotal();
        firstCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFaxialLcId();
        firstCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFaxialBoltId();
        firstCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFaxialCriFactor();

        secondCriterionName = "A MIN";
        secondCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshear1_axial();
        secondCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshear1();
        secondCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshear1_shear2();
        secondCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshear1_shearTotal();
        secondCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshear1LcId();
        secondCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshear1BoltId();
        secondCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshear1CriFactor();

        thirdCriterionName = "A MIN";
        thirdCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshear2_axial();
        thirdCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshear2_shear1();
        thirdCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshear2();
        thirdCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshear2_shearTotal();
        thirdCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshear2LcId();
        thirdCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshear2BoltId();
        thirdCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshear2CriFactor();

        fourthCriterionName = "A MIN";
        fourthCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshearTotal_axial();
        fourthCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshearTotal_shear1();
        fourthCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshearTotal_shear2();
        fourthCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshearTotal();
        fourthCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshear2LcId();
        fourthCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshear2BoltId();
        fourthCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshear2CriFactor();

        rowStart = 23;
        createTableByCriteriumBolt(
                sheet, rowStart, cellFirst,
                cellStyleHeaderTopFontCenter,

                cellStyleResultsAbsMinNormal,
                cellStyleResultsAbsMinBold, cellStyleResultsAbsMinCriterion,
                force1Name, force2Name, force3Name, force4Name,
                firstCriterionBoltId, firstCriterionLCid,
                firstCriterionForce1, firstCriterionForce2, firstCriterionForce3, firstCriterionForce4,
                firstCriterionCriFactor, firstCriterionName,


                cellStyleResultsAbsMinNormal,
                cellStyleResultsAbsMinBold, cellStyleResultsAbsMinCriterion,
                secondCriterionBoltId, secondCriterionLCid,
                secondCriterionForce1, secondCriterionForce2, secondCriterionForce3, secondCriterionForce4,
                secondCriterionCriFactor, secondCriterionName,

                cellStyleResultsAbsMinNormal,
                cellStyleResultsAbsMinBold, cellStyleResultsAbsMinCriterion,
                thirdCriterionBoltId, thirdCriterionLCid,
                thirdCriterionForce1, thirdCriterionForce2, thirdCriterionForce3, thirdCriterionForce4,
                thirdCriterionCriFactor, thirdCriterionName,

                cellStyleResultsAbsMinNormal,
                cellStyleResultsAbsMinBold, cellStyleResultsAbsMinCriterion,
                fourthCriterionBoltId, fourthCriterionLCid,
                fourthCriterionForce1, fourthCriterionForce2, fourthCriterionForce3, fourthCriterionForce4,
                fourthCriterionCriFactor, fourthCriterionName
        );

    }

    private void generateSummaryTablesCBARold(String partName, int currentOffset, CsvParserResultExtractor csvParser) {
        int cellFirst = columnCurrentStartId + (COLUMN_OFFSET * currentOffset);
        int cellLast = cellFirst + COLUMN_OFFSET - 1; // currently not in use, delete later

        // --- row 5-10: CRI table - headers, values
        String criForce1Name = "Fax [N]";
        String criForce2Name = "Fsh1 [N]";
        String criForce3Name = "Fsh2 [N]";
        String criForce4Name = "Fsht [N]";

        Double criValue1Force = ((CbarResults) csvParser.getResults().get(partName)).getCriFaxial();
        Double criValue2Force = ((CbarResults) csvParser.getResults().get(partName)).getCriFshear1();
        Double criValue3Force = ((CbarResults) csvParser.getResults().get(partName)).getCriFshear2();
        Double criValue4Force = ((CbarResults) csvParser.getResults().get(partName)).getCriFshearTotal();

        Integer criValue1Lc = ((CbarResults) csvParser.getResults().get(partName)).getCriLCId();
        Integer criValue2Lc = null;
        Integer criValue3Lc = null;
        Integer criValue4Lc = null;

        Integer criValue1Bolt = ((CbarResults) csvParser.getResults().get(partName)).getCriBoltId();
        Integer criValue2Bolt = null;
        Integer criValue3Bolt = null;
        Integer criValue4Bolt = null;

        Double criValue1CriFact = ((CbarResults) csvParser.getResults().get(partName)).getCriFactor();
        Double criValue2CriFact = null;
        Double criValue3CriFact = null;
        Double criValue4CriFact = null;

        Integer criValue1Row = null;
        Integer criValue2Row = null;
        Integer criValue3Row = null;
        Integer criValue4Row = null;

        createTableSummary("CRI", 5,
                criForce1Name, criForce2Name, criForce3Name, criForce4Name,
                cellFirst, sheet, cellStyleHeaderTopFontCenter, cellStyleHeaderLeftCRI, cellStyleResults0,
                criValue1Force, criValue2Force, criValue3Force, criValue4Force,
                criValue1Lc, criValue2Lc, criValue3Lc, criValue4Lc,
                criValue1Bolt, criValue2Bolt, criValue3Bolt, criValue4Bolt,
                criValue1CriFact, criValue2CriFact, criValue3CriFact, criValue4CriFact,
                criValue1Row, criValue2Row, criValue3Row, criValue4Row);

        // --- row 12-17: MAX table - headers, values
        String maxForce1Name = "Fax [N]";
        String maxForce2Name = "Fsh1 [N]";
        String maxForce3Name = "Fsh2 [N]";
        String maxForce4Name = "Fsht [N]";

        Double maxValue1Force = ((CbarResults) csvParser.getResults().get(partName)).getMaxFaxial();
        Double maxValue2Force = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshear1();
        Double maxValue3Force = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshear2();
        Double maxValue4Force = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshearTotal();

        Integer maxValue1Lc = ((CbarResults) csvParser.getResults().get(partName)).getMaxFaxialLcId();
        Integer maxValue2Lc = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshear1LcId();
        Integer maxValue3Lc = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshear2LcId();
        Integer maxValue4Lc = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshearTotalLcId();

        Integer maxValue1Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMaxFaxialBoltId();
        Integer maxValue2Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshear1BoltId();
        Integer maxValue3Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshear2BoltId();
        Integer maxValue4Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshearTotalBoltId();

        Double maxValue1criFact = ((CbarResults) csvParser.getResults().get(partName)).getMaxFaxialCriFactor();
        Double maxValue2criFact = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshear1CriFactor();
        Double maxValue3criFact = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshear2CriFactor();
        Double maxValue4criFact = ((CbarResults) csvParser.getResults().get(partName)).getMaxFshearTotalCriFactor();

        Integer maxValue1Row = null;
        Integer maxValue2Row = null;
        Integer maxValue3Row = null;
        Integer maxValue4Row = null;

        createTableSummary("MAX", 12,
                maxForce1Name, maxForce2Name, maxForce3Name, maxForce4Name,
                cellFirst, sheet, cellStyleHeaderTopFontCenter, cellStyleHeaderLeftMAX, cellStyleResults0,
                maxValue1Force, maxValue2Force, maxValue3Force, maxValue4Force,
                maxValue1Lc, maxValue2Lc, maxValue3Lc, maxValue4Lc,
                maxValue1Bolt, maxValue2Bolt, maxValue3Bolt, maxValue4Bolt,
                maxValue1criFact, maxValue2criFact, maxValue3criFact, maxValue4criFact,
                maxValue1Row, maxValue2Row, maxValue3Row, maxValue4Row);

        // --- row 19-24: MIN table - headers, values
        String minForce1Name = "Fax [N]";
        String minForce2Name = "Fsh1 [N]";
        String minForce3Name = "Fsh2 [N]";
        String minForce4Name = "Fsht [N]";

        Double minValue1Force = ((CbarResults) csvParser.getResults().get(partName)).getMinFaxial();
        Double minValue2Force = ((CbarResults) csvParser.getResults().get(partName)).getMinFshear1();
        Double minValue3Force = ((CbarResults) csvParser.getResults().get(partName)).getMinFshear2();
        Double minValue4Force = ((CbarResults) csvParser.getResults().get(partName)).getMinFshearTotal();

        Integer minValue1Lc = ((CbarResults) csvParser.getResults().get(partName)).getMinFaxialLcId();
        Integer minValue2Lc = ((CbarResults) csvParser.getResults().get(partName)).getMinFshear1LcId();
        Integer minValue3Lc = ((CbarResults) csvParser.getResults().get(partName)).getMinFshear2LcId();
        Integer minValue4Lc = ((CbarResults) csvParser.getResults().get(partName)).getMinFshearTotalLcId();

        Integer minValue1Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMinFaxialBoltId();
        Integer minValue2Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMinFshear1BoltId();
        Integer minValue3Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMinFshear2BoltId();
        Integer minValue4Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMinFshearTotalBoltId();

        Double minValue1criFact = ((CbarResults) csvParser.getResults().get(partName)).getMinFaxialCriFactor();
        Double minValue2criFact = ((CbarResults) csvParser.getResults().get(partName)).getMinFshear1CriFactor();
        Double minValue3criFact = ((CbarResults) csvParser.getResults().get(partName)).getMinFshear2CriFactor();
        Double minValue4criFact = ((CbarResults) csvParser.getResults().get(partName)).getMinFshearTotalCriFactor();

        Integer minValue1Row = null;
        Integer minValue2Row = null;
        Integer minValue3Row = null;
        Integer minValue4Row = null;

        createTableSummary("MIN", 19,
                minForce1Name, minForce2Name, minForce3Name, minForce4Name,
                cellFirst, sheet, cellStyleHeaderTopFontCenter, cellStyleHeaderLeftMIN, cellStyleResults0,
                minValue1Force, minValue2Force, minValue3Force, minValue4Force,
                minValue1Lc, minValue2Lc, minValue3Lc, minValue4Lc,
                minValue1Bolt, minValue2Bolt, minValue3Bolt, minValue4Bolt,
                minValue1criFact, minValue2criFact, minValue3criFact, minValue4criFact,
                minValue1Row, minValue2Row, minValue3Row, minValue4Row);

        // --- row 26-31: F>0 MIN table - headers, values
        String absMinForce1Name = "Fax [N]";
        String absMinForce2Name = "Fsh1 [N]";
        String absMinForce3Name = "Fsh2 [N]";
        String absMinForce4Name = "Fsht [N]";

        Double absMinValue1Force = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFaxial();
        Double absMinValue2Force = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshear1();
        Double absMinValue3Force = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshear2();
        Double absMinValue4Force = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshearTotal();

        Integer absMinValue1Lc = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFaxialLcId();
        Integer absMinValue2Lc = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshear1LcId();
        Integer absMinValue3Lc = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshear2LcId();
        Integer absMinValue4Lc = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshearTotalLcId();

        Integer absMinValue1Bolt = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFaxialBoltId();
        Integer absMinValue2Bolt = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshear1BoltId();
        Integer absMinValue3Bolt = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshear2BoltId();
        Integer absMinValue4Bolt = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshearTotalBoltId();

        Double absMinValue1absCriFact = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFaxialCriFactor();
        Double absMinValue2absCriFact = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshear1CriFactor();
        Double absMinValue3absCriFact = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshear2CriFactor();
        Double absMinValue4absCriFact = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFshearTotalCriFactor();

        Integer absMinValue1Row = null;
        Integer absMinValue2Row = null;
        Integer absMinValue3Row = null;
        Integer absMinValue4Row = null;

        // F>0 MIN
        createTableSummary("F>0 MIN", 26,
                absMinForce1Name, absMinForce2Name, absMinForce3Name, absMinForce4Name,
                cellFirst, sheet, cellStyleHeaderTopFontCenter, cellStyleHeaderLeftAbsMIN, cellStyleResults0,
                absMinValue1Force, absMinValue2Force, absMinValue3Force, absMinValue4Force,
                absMinValue1Lc, absMinValue2Lc, absMinValue3Lc, absMinValue4Lc,
                absMinValue1Bolt, absMinValue2Bolt, absMinValue3Bolt, absMinValue4Bolt,
                absMinValue1absCriFact, absMinValue2absCriFact, absMinValue3absCriFact, absMinValue4absCriFact,
                absMinValue1Row, absMinValue2Row, absMinValue3Row, absMinValue4Row);
    }

    private void generateResultsTableCBAR(String partName, int currentOffset, CsvParserResultExtractor csvParser) {
        int cellFirst = columnCurrentStartId + (COLUMN_OFFSET * currentOffset);
        int cellLast = cellFirst + COLUMN_OFFSET - 1; // currently not in use, delete later

        // current row and cell in row to write data
        Cell cell;
        Row row;

        // --- row 34: RESULTS header
        String headerBolt = "BOLT";
        String headerLC = "LC";
        String criFactor = "cri factor";
        String description = "extra info";
        String resultsForce1Name = "Fax [N]";
        String resultsForce2Name = "Fsh1 [N]";
        String resultsForce3Name = "Fsh2 [N]";
        String resultsForce4Name = "Fsht [N]";

        createHeaderForTableResult(35, cellFirst, 0, sheet,
                cellStyleHeaderTopFontLeft, cellStyleHeaderTopFontCenter, cellStyleDescriptionBlank,
                headerBolt, headerLC,
                resultsForce1Name, resultsForce2Name, resultsForce3Name, resultsForce4Name,
                criFactor, description);

        // --- row 36 - all RESULTS
        int currentResultRowId = 36;

        List<Integer> boltIds = new ArrayList<>();
        int backgroundId = 1;

        // get table boltId - lcId: location 0 = boltId, location 1 = lcId
        for (Integer[] pairBoltIdLcId : ((CbarResults) csvParser.getResults().get(partName)).getPairBoltIdLcIdForFaxial().keySet()) {

            // mechanism to change background - change colour after passing all load cases per bolt
            if (boltIds.contains(pairBoltIdLcId[0])) {
                boltIds = new ArrayList<>();
                backgroundId++;
                boltIds.add(pairBoltIdLcId[0]);
            } else {
                boltIds.add(pairBoltIdLcId[0]);
            }

            // bolt id
            if (sheet.getRow(currentResultRowId) == null) {
                row = sheet.createRow(currentResultRowId++);
            } else {
                row = sheet.getRow(currentResultRowId++);
            }
            cell = row.createCell(cellFirst + 2);
            cell.setCellValue(pairBoltIdLcId[0]);
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleBolt1);
            } else {
                cell.setCellStyle(cellStyleBolt2);
            }

            // lc id
            cell = row.createCell(cellFirst + 3);
            cell.setCellValue(pairBoltIdLcId[1]);
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleLC1);
            } else {
                cell.setCellStyle(cellStyleLC2);
            }

            // VALUE Faxial
            cell = row.createCell(cellFirst + 4);
            cell.setCellValue(((CbarResults) csvParser.getResults().get(partName)).getPairBoltIdLcIdForFaxial().get(pairBoltIdLcId));
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleResults1);
            } else {
                cell.setCellStyle(cellStyleResults2);
            }

            // VALUE Fshear1
            cell = row.createCell(cellFirst + 5);
            cell.setCellValue(((CbarResults) csvParser.getResults().get(partName)).getPairBoltIdLcIdForFshear1().get(pairBoltIdLcId));
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleResults1);
            } else {
                cell.setCellStyle(cellStyleResults2);
            }

            // VALUE Fshear2
            cell = row.createCell(cellFirst + 6);
            cell.setCellValue(((CbarResults) csvParser.getResults().get(partName)).getPairBoltIdLcIdForFshear2().get(pairBoltIdLcId));
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleResults1);
            } else {
                cell.setCellStyle(cellStyleResults2);
            }

            // VALUE Fshear total
            cell = row.createCell(cellFirst + 7);
            cell.setCellValue(((CbarResults) csvParser.getResults().get(partName)).getPairBoltIdLcIdForFshearTotal().get(pairBoltIdLcId));
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleResults1);
            } else {
                cell.setCellStyle(cellStyleResults2);
            }

            // VALUE CRI FACTOR
            cell = row.createCell(cellFirst + 8);
            cell.setCellValue(((CbarResults) csvParser.getResults().get(partName)).getPairBoltIdLcIdForCriFactor().get(pairBoltIdLcId));
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleResults1);
            } else {
                cell.setCellStyle(cellStyleResults2);
            }
        }
    }

    private void generateSummaryTablesRBE2(String partName, int currentOffset, CsvParserResultExtractor csvParser) {
        int cellFirst = columnCurrentStartId + (COLUMN_OFFSET * currentOffset);
        int cellLast = cellFirst + COLUMN_OFFSET - 1;

        // --- row 5-10: CRI table - headers, values
        String criForce1Name = "Fmag [N]";
        String criForce2Name = "Fx [N]";
        String criForce3Name = "Fy [N]";
        String criForce4Name = "Fz [N]";

        Double criValue1Force = null; // TODO later user must decide how to combine forces
        Double criValue2Force = null; //
        Double criValue3Force = null; //
        Double criValue4Force = null; //

        Integer criValue1Lc = null; // TODO later user must decide how to combine forces
        Integer criValue2Lc = null;
        Integer criValue3Lc = null;
        Integer criValue4Lc = null;

        Integer criValue1Bolt = null; // TODO later user must decide how to combine forces
        Integer criValue2Bolt = null;
        Integer criValue3Bolt = null;
        Integer criValue4Bolt = null;

        Double criValue1CriFact = null; // TODO later user must decide how to combine forces
        Double criValue2CriFact = null;
        Double criValue3CriFact = null;
        Double criValue4CriFact = null;

        Integer criValue1Row = null; // TODO later user must decide how to combine forces
        Integer criValue2Row = null;
        Integer criValue3Row = null;
        Integer criValue4Row = null;

        createTableSummary("CRI", 5,
                criForce1Name, criForce2Name, criForce3Name, criForce4Name,
                cellFirst, sheet, cellStyleHeaderTopFontCenter, cellStyleHeaderLeftCRI, cellStyleResults0,
                criValue1Force, criValue2Force, criValue3Force, criValue4Force,
                criValue1Lc, criValue2Lc, criValue3Lc, criValue4Lc,
                criValue1Bolt, criValue2Bolt, criValue3Bolt, criValue4Bolt,
                criValue1CriFact, criValue2CriFact, criValue3CriFact, criValue4CriFact,
                criValue1Row, criValue2Row, criValue3Row, criValue4Row);

        // --- row 12-17: MAX table - headers, values
        String maxForce1Name = "Fmag [N]";
        String maxForce2Name = "Fx [N]";
        String maxForce3Name = "Fy [N]";
        String maxForce4Name = "Fz [N]";

        Double maxValue1Force = ((Rbe2Results) csvParser.getResults().get(partName)).getMaxFmag();
        Double maxValue2Force = ((Rbe2Results) csvParser.getResults().get(partName)).getMaxFx();
        Double maxValue3Force = ((Rbe2Results) csvParser.getResults().get(partName)).getMaxFy();
        Double maxValue4Force = ((Rbe2Results) csvParser.getResults().get(partName)).getMaxFz();

        Integer maxValue1Lc = ((Rbe2Results) csvParser.getResults().get(partName)).getMaxFmagLcId();
        Integer maxValue2Lc = ((Rbe2Results) csvParser.getResults().get(partName)).getMaxFxLcId();
        Integer maxValue3Lc = ((Rbe2Results) csvParser.getResults().get(partName)).getMaxFyLcId();
        Integer maxValue4Lc = ((Rbe2Results) csvParser.getResults().get(partName)).getMaxFzLcId();

        Integer maxValue1Bolt = ((Rbe2Results) csvParser.getResults().get(partName)).getMaxFmagBoltId();
        Integer maxValue2Bolt = ((Rbe2Results) csvParser.getResults().get(partName)).getMaxFxBoltId();
        Integer maxValue3Bolt = ((Rbe2Results) csvParser.getResults().get(partName)).getMaxFyBoltId();
        Integer maxValue4Bolt = ((Rbe2Results) csvParser.getResults().get(partName)).getMaxFzBoltId();

        Double maxValue1criFact = null; // TODO later user must decide how to combine forces
        Double maxValue2criFact = null;
        Double maxValue3criFact = null;
        Double maxValue4criFact = null;

        Integer maxValue1Row = 0;
        Integer maxValue2Row = 0;
        Integer maxValue3Row = 0;
        Integer maxValue4Row = 0;

        createTableSummary("MAX", 12,
                maxForce1Name, maxForce2Name, maxForce3Name, maxForce4Name,
                cellFirst, sheet, cellStyleHeaderTopFontCenter, cellStyleHeaderLeftMAX, cellStyleResults0,
                maxValue1Force, maxValue2Force, maxValue3Force, maxValue4Force,
                maxValue1Lc, maxValue2Lc, maxValue3Lc, maxValue4Lc,
                maxValue1Bolt, maxValue2Bolt, maxValue3Bolt, maxValue4Bolt,
                maxValue1criFact, maxValue2criFact, maxValue3criFact, maxValue4criFact,
                maxValue1Row, maxValue2Row, maxValue3Row, maxValue4Row);

        // --- row 19-24: MIN table - headers, values
        String minForce1Name = "Fmag [N]";
        String minForce2Name = "Fx [N]";
        String minForce3Name = "Fy [N]";
        String minForce4Name = "Fz [N]";

        Double minValue1Force = ((Rbe2Results) csvParser.getResults().get(partName)).getMinFmag();
        Double minValue2Force = ((Rbe2Results) csvParser.getResults().get(partName)).getMinFx();
        Double minValue3Force = ((Rbe2Results) csvParser.getResults().get(partName)).getMinFy();
        Double minValue4Force = ((Rbe2Results) csvParser.getResults().get(partName)).getMinFz();

        Integer minValue1Lc = ((Rbe2Results) csvParser.getResults().get(partName)).getMinFmagLcId();
        Integer minValue2Lc = ((Rbe2Results) csvParser.getResults().get(partName)).getMinFxLcId();
        Integer minValue3Lc = ((Rbe2Results) csvParser.getResults().get(partName)).getMinFyLcId();
        Integer minValue4Lc = ((Rbe2Results) csvParser.getResults().get(partName)).getMinFzLcId();

        Integer minValue1Bolt = ((Rbe2Results) csvParser.getResults().get(partName)).getMinFmagBoltId();
        Integer minValue2Bolt = ((Rbe2Results) csvParser.getResults().get(partName)).getMinFxBoltId();
        Integer minValue3Bolt = ((Rbe2Results) csvParser.getResults().get(partName)).getMinFyBoltId();
        Integer minValue4Bolt = ((Rbe2Results) csvParser.getResults().get(partName)).getMinFzBoltId();

        Double minValue1criFact = null; // TODO later user must decide how to combine forces
        Double minValue2criFact = null;
        Double minValue3criFact = null;
        Double minValue4criFact = null;

        Integer minValue1Row = 0;
        Integer minValue2Row = 0;
        Integer minValue3Row = 0;
        Integer minValue4Row = 0;

        createTableSummary("MIN", 19,
                minForce1Name, minForce2Name, minForce3Name, minForce4Name,
                cellFirst, sheet, cellStyleHeaderTopFontCenter, cellStyleHeaderLeftMIN, cellStyleResults0,
                minValue1Force, minValue2Force, minValue3Force, minValue4Force,
                minValue1Lc, minValue2Lc, minValue3Lc, minValue4Lc,
                minValue1Bolt, minValue2Bolt, minValue3Bolt, minValue4Bolt,
                minValue1criFact, minValue2criFact, minValue3criFact, minValue4criFact,
                minValue1Row, minValue2Row, minValue3Row, minValue4Row);

        // --- row 26-31: F>0 MIN table - headers, values
        String absMinForce1Name = "Fmag [N]";
        String absMinForce2Name = "Fx [N]";
        String absMinForce3Name = "Fy [N]";
        String absMinForce4Name = "Fz [N]";

        Double absMinValue1Force = ((Rbe2Results) csvParser.getResults().get(partName)).getAbsMinFmag();
        Double absMinValue2Force = ((Rbe2Results) csvParser.getResults().get(partName)).getAbsMinFx();
        Double absMinValue3Force = ((Rbe2Results) csvParser.getResults().get(partName)).getAbsMinFy();
        Double absMinValue4Force = ((Rbe2Results) csvParser.getResults().get(partName)).getAbsMinFz();

        Integer absMinValue1Lc = ((Rbe2Results) csvParser.getResults().get(partName)).getAbsMinFmagLcId();
        Integer absMinValue2Lc = ((Rbe2Results) csvParser.getResults().get(partName)).getAbsMinFxLcId();
        Integer absMinValue3Lc = ((Rbe2Results) csvParser.getResults().get(partName)).getAbsMinFyLcId();
        Integer absMinValue4Lc = ((Rbe2Results) csvParser.getResults().get(partName)).getAbsMinFzLcId();

        Integer absMinValue1Bolt = ((Rbe2Results) csvParser.getResults().get(partName)).getAbsMinFmagBoltId();
        Integer absMinValue2Bolt = ((Rbe2Results) csvParser.getResults().get(partName)).getAbsMinFxBoltId();
        Integer absMinValue3Bolt = ((Rbe2Results) csvParser.getResults().get(partName)).getAbsMinFyBoltId();
        Integer absMinValue4Bolt = ((Rbe2Results) csvParser.getResults().get(partName)).getAbsMinFzBoltId();

        Double absMinValue1absCriFact = null; // TODO later user must decide how to combine forces
        Double absMinValue2absCriFact = null;
        Double absMinValue3absCriFact = null;
        Double absMinValue4absCriFact = null;

        Integer absMinValue1Row = 0;
        Integer absMinValue2Row = 0;
        Integer absMinValue3Row = 0;
        Integer absMinValue4Row = 0;

        // F>0 MIN
        createTableSummary("F>0 MIN", 26,
                absMinForce1Name, absMinForce2Name, absMinForce3Name, absMinForce4Name,
                cellFirst, sheet, cellStyleHeaderTopFontCenter, cellStyleHeaderLeftAbsMIN, cellStyleResults0,
                absMinValue1Force, absMinValue2Force, absMinValue3Force, absMinValue4Force,
                absMinValue1Lc, absMinValue2Lc, absMinValue3Lc, absMinValue4Lc,
                absMinValue1Bolt, absMinValue2Bolt, absMinValue3Bolt, absMinValue4Bolt,
                absMinValue1absCriFact, absMinValue2absCriFact, absMinValue3absCriFact, absMinValue4absCriFact,
                absMinValue1Row, absMinValue2Row, absMinValue3Row, absMinValue4Row);
    }

    private void generateResultsTableRBE2(String partName, int currentOffset, CsvParserResultExtractor csvParser) {
        int cellFirst = columnCurrentStartId + (COLUMN_OFFSET * currentOffset);
        int cellLast = cellFirst + COLUMN_OFFSET - 1; //todo delete it later

        // current row and cell in row to write data
        Cell cell;
        Row row;

        // --- row 34: RESULTS header
        String headerBolt = "BOLT";
        String headerLC = "LC";
        String criFactor = "cri factor";
        String description = "extra info";
        String resultsForce1Name = "Fmag [N]";
        String resultsForce2Name = "Fx [N]";
        String resultsForce3Name = "Fy [N]";
        String resultsForce4Name = "Fz [N]";

        createHeaderForTableResult(34, cellFirst, 0, sheet,
                cellStyleHeaderTopFontLeft, cellStyleHeaderTopFontCenter, cellStyleDescriptionBlank,
                headerBolt, headerLC,
                resultsForce1Name, resultsForce2Name, resultsForce3Name, resultsForce4Name,
                criFactor, description);

        // --- row 35 - RESULTS
        int currentResultRowId = 35;

        List<Integer> boltIds = new ArrayList<>();
        int backgroundId = 1;

        // get table boltId - lcId: location 0 = boltId, location 1 = lcId
        for (Integer[] pairBoltIdLcId : ((Rbe2Results) csvParser.getResults().get(partName)).getPairBoltIdLcIdForFy().keySet()) {

            // mechanism to change background - change colour after passing all load cases per bolt
            if (boltIds.contains(pairBoltIdLcId[0])) {
                boltIds = new ArrayList<>();
                backgroundId++;
                boltIds.add(pairBoltIdLcId[0]);
            } else {
                boltIds.add(pairBoltIdLcId[0]);
            }

            // bolt id
            if (sheet.getRow(currentResultRowId) == null) {
                row = sheet.createRow(currentResultRowId++);
            } else {
                row = sheet.getRow(currentResultRowId++);
            }
            cell = row.createCell(cellFirst + 1);
            cell.setCellValue(pairBoltIdLcId[0]);
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleBolt1);
            } else {
                cell.setCellStyle(cellStyleBolt2);
            }

            // lc id
            cell = row.createCell(cellFirst + 2);
            cell.setCellValue(pairBoltIdLcId[1]);
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleLC1);
            } else {
                cell.setCellStyle(cellStyleLC2);
            }

            // VALUE Fmag
            cell = row.createCell(cellFirst + 3);
            cell.setCellValue(((Rbe2Results) csvParser.getResults().get(partName)).getPairBoltIdLcIdForFmag().get(pairBoltIdLcId));
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleResults1);
            } else {
                cell.setCellStyle(cellStyleResults2);
            }

            // VALUE Fx
            cell = row.createCell(cellFirst + 4);
            cell.setCellValue(((Rbe2Results) csvParser.getResults().get(partName)).getPairBoltIdLcIdForFx().get(pairBoltIdLcId));
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleResults1);
            } else {
                cell.setCellStyle(cellStyleResults2);
            }

            // VALUE Fy
            cell = row.createCell(cellFirst + 5);
            cell.setCellValue(((Rbe2Results) csvParser.getResults().get(partName)).getPairBoltIdLcIdForFy().get(pairBoltIdLcId));
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleResults1);
            } else {
                cell.setCellStyle(cellStyleResults2);
            }

            // VALUE Fz
            cell = row.createCell(cellFirst + 6);
            cell.setCellValue(((Rbe2Results) csvParser.getResults().get(partName)).getPairBoltIdLcIdForFz().get(pairBoltIdLcId));
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleResults1);
            } else {
                cell.setCellStyle(cellStyleResults2);
            }

            // VALUE CRI FACTOR
            cell = row.createCell(cellFirst + 7);
            // for rbe2 this will not be calculated
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleResults1);
            } else {
                cell.setCellStyle(cellStyleResults2);
            }

        }
    }

    private void generateSummaryTablesRBE2NoMag(String partName, int currentOffset, CsvParserResultExtractor csvParser) {
        int cellFirst = columnCurrentStartId + (COLUMN_OFFSET * currentOffset);
        int cellLast = cellFirst + COLUMN_OFFSET - 1;

        // --- row 5-10: CRI table - headers, values
        String criForce1Name = "Fx [N]";
        String criForce2Name = "Fy [N]";
        String criForce3Name = "Fz [N]";
        String criForce4Name = "F... [N]";

        Double criValue1Force = null; // TODO later user must decide how to combine forces
        Double criValue2Force = null; //
        Double criValue3Force = null; //
        Double criValue4Force = null; //

        Integer criValue1Lc = null; // TODO later user must decide how to combine forces
        Integer criValue2Lc = null;
        Integer criValue3Lc = null;
        Integer criValue4Lc = null;

        Integer criValue1Bolt = null; // TODO later user must decide how to combine forces
        Integer criValue2Bolt = null;
        Integer criValue3Bolt = null;
        Integer criValue4Bolt = null;

        Double criValue1CriFact = null; // TODO later user must decide how to combine forces
        Double criValue2CriFact = null;
        Double criValue3CriFact = null;
        Double criValue4CriFact = null;

        Integer criValue1Row = null; // TODO later user must decide how to combine forces
        Integer criValue2Row = null;
        Integer criValue3Row = null;
        Integer criValue4Row = null;

        createTableSummary("CRI", 5,
                criForce1Name, criForce2Name, criForce3Name, criForce4Name,
                cellFirst, sheet, cellStyleHeaderTopFontCenter, cellStyleHeaderLeftCRI, cellStyleResults0,
                criValue1Force, criValue2Force, criValue3Force, criValue4Force,
                criValue1Lc, criValue2Lc, criValue3Lc, criValue4Lc,
                criValue1Bolt, criValue2Bolt, criValue3Bolt, criValue4Bolt,
                criValue1CriFact, criValue2CriFact, criValue3CriFact, criValue4CriFact,
                criValue1Row, criValue2Row, criValue3Row, criValue4Row);

        // --- row 12-17: MAX table - headers, values
        String maxForce1Name = "Fx [N]";
        String maxForce2Name = "Fy [N]";
        String maxForce3Name = "Fz [N]";
        String maxForce4Name = "F... [N]";

        Double maxValue1Force = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getMaxFx();
        Double maxValue2Force = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getMaxFy();
        Double maxValue3Force = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getMaxFz();
        Double maxValue4Force = null;

        Integer maxValue1Lc = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getMaxFxLcId();
        Integer maxValue2Lc = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getMaxFyLcId();
        Integer maxValue3Lc = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getMaxFzLcId();
        Integer maxValue4Lc = null;

        Integer maxValue1Bolt = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getMaxFxBoltId();
        Integer maxValue2Bolt = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getMaxFyBoltId();
        Integer maxValue3Bolt = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getMaxFzBoltId();
        Integer maxValue4Bolt = null;

        Double maxValue1criFact = null; // TODO later user must decide how to combine forces
        Double maxValue2criFact = null;
        Double maxValue3criFact = null;
        Double maxValue4criFact = null;

        Integer maxValue1Row = 0;
        Integer maxValue2Row = 0;
        Integer maxValue3Row = 0;
        Integer maxValue4Row = 0;

        createTableSummary("MAX", 12,
                maxForce1Name, maxForce2Name, maxForce3Name, maxForce4Name,
                cellFirst, sheet, cellStyleHeaderTopFontCenter, cellStyleHeaderLeftMAX, cellStyleResults0,
                maxValue1Force, maxValue2Force, maxValue3Force, maxValue4Force,
                maxValue1Lc, maxValue2Lc, maxValue3Lc, maxValue4Lc,
                maxValue1Bolt, maxValue2Bolt, maxValue3Bolt, maxValue4Bolt,
                maxValue1criFact, maxValue2criFact, maxValue3criFact, maxValue4criFact,
                maxValue1Row, maxValue2Row, maxValue3Row, maxValue4Row);

        // --- row 19-24: MIN table - headers, values
        String minForce1Name = "Fx [N]";
        String minForce2Name = "Fy [N]";
        String minForce3Name = "Fz [N]";
        String minForce4Name = "F... [N]";

        Double minValue1Force = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getMinFx();
        Double minValue2Force = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getMinFy();
        Double minValue3Force = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getMinFz();
        Double minValue4Force = null;

        Integer minValue1Lc = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getMinFxLcId();
        Integer minValue2Lc = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getMinFyLcId();
        Integer minValue3Lc = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getMinFzLcId();
        Integer minValue4Lc = null;

        Integer minValue1Bolt = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getMinFxBoltId();
        Integer minValue2Bolt = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getMinFyBoltId();
        Integer minValue3Bolt = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getMinFzBoltId();
        Integer minValue4Bolt = null;

        Double minValue1criFact = null; // TODO later user must decide how to combine forces
        Double minValue2criFact = null;
        Double minValue3criFact = null;
        Double minValue4criFact = null;

        Integer minValue1Row = 0;
        Integer minValue2Row = 0;
        Integer minValue3Row = 0;
        Integer minValue4Row = 0;

        createTableSummary("MIN", 19,
                minForce1Name, minForce2Name, minForce3Name, minForce4Name,
                cellFirst, sheet, cellStyleHeaderTopFontCenter, cellStyleHeaderLeftMIN, cellStyleResults0,
                minValue1Force, minValue2Force, minValue3Force, minValue4Force,
                minValue1Lc, minValue2Lc, minValue3Lc, minValue4Lc,
                minValue1Bolt, minValue2Bolt, minValue3Bolt, minValue4Bolt,
                minValue1criFact, minValue2criFact, minValue3criFact, minValue4criFact,
                minValue1Row, minValue2Row, minValue3Row, minValue4Row);

        // --- row 26-31: ABSOLUT MIN table - headers, values
        String absMinForce1Name = "Fx [N]";
        String absMinForce2Name = "Fy [N]";
        String absMinForce3Name = "Fz [N]";
        String absMinForce4Name = "F... [N]";

        Double absMinValue1Force = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getAbsMinFx();
        Double absMinValue2Force = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getAbsMinFy();
        Double absMinValue3Force = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getAbsMinFz();
        Double absMinValue4Force = null;

        Integer absMinValue1Lc = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getAbsMinFxLcId();
        Integer absMinValue2Lc = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getAbsMinFyLcId();
        Integer absMinValue3Lc = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getAbsMinFzLcId();
        Integer absMinValue4Lc = null;

        Integer absMinValue1Bolt = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getAbsMinFxBoltId();
        Integer absMinValue2Bolt = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getAbsMinFyBoltId();
        Integer absMinValue3Bolt = ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getAbsMinFzBoltId();
        Integer absMinValue4Bolt = null;

        Double absMinValue1absCriFact = null; // TODO later user must decide how to combine forces
        Double absMinValue2absCriFact = null;
        Double absMinValue3absCriFact = null;
        Double absMinValue4absCriFact = null;

        Integer absMinValue1Row = 0;
        Integer absMinValue2Row = 0;
        Integer absMinValue3Row = 0;
        Integer absMinValue4Row = 0;

        // ABS MIN
        createTableSummary("F>0 MIN", 26,
                absMinForce1Name, absMinForce2Name, absMinForce3Name, absMinForce4Name,
                cellFirst, sheet, cellStyleHeaderTopFontCenter, cellStyleHeaderLeftAbsMIN, cellStyleResults0,
                absMinValue1Force, absMinValue2Force, absMinValue3Force, absMinValue4Force,
                absMinValue1Lc, absMinValue2Lc, absMinValue3Lc, absMinValue4Lc,
                absMinValue1Bolt, absMinValue2Bolt, absMinValue3Bolt, absMinValue4Bolt,
                absMinValue1absCriFact, absMinValue2absCriFact, absMinValue3absCriFact, absMinValue4absCriFact,
                absMinValue1Row, absMinValue2Row, absMinValue3Row, absMinValue4Row);
    }

    private void generateResultsTableRBE2NoMag(String partName, int currentOffset, CsvParserResultExtractor csvParser) {
        int cellFirst = columnCurrentStartId + (COLUMN_OFFSET * currentOffset);
        int cellLast = cellFirst + COLUMN_OFFSET - 1;

        // current row and cell in row to write data
        Cell cell;
        Row row;

        // --- row 34: RESULTS header
        String headerBolt = "BOLT";
        String headerLC = "LC";
        String criFactor = "cri factor";
        String description = "extra info";
        String resultsForce1Name = "Fx [N]";
        String resultsForce2Name = "Fy [N]";
        String resultsForce3Name = "Fz [N]";
        String resultsForce4Name = "F... [N]";

        createHeaderForTableResult(34, cellFirst, 0, sheet,
                cellStyleHeaderTopFontLeft, cellStyleHeaderTopFontCenter, cellStyleDescriptionBlank,
                headerBolt, headerLC,
                resultsForce1Name, resultsForce2Name, resultsForce3Name, resultsForce4Name,
                criFactor, description);

        // --- row 35 - RESULTS ...
        int currentResultRowId = 35;

        List<Integer> boltIds = new ArrayList<>();
        int backgroundId = 1;

        // get table boltId - lcId: location 0 = boltId, location 1 = lcId
        for (Integer[] pairBoltIdLcId : ((Rbe2NoMagResults) csvParser.getResults().get(partName)).getPairBoltIdLcIdForFy().keySet()) {

            // mechanism to change background - change colour after passing all load cases per bolt
            if (boltIds.contains(pairBoltIdLcId[0])) {
                boltIds = new ArrayList<>();
                backgroundId++;
                boltIds.add(pairBoltIdLcId[0]);
            } else {
                boltIds.add(pairBoltIdLcId[0]);
            }

            // bolt id
            if (sheet.getRow(currentResultRowId) == null) {
                row = sheet.createRow(currentResultRowId++);
            } else {
                row = sheet.getRow(currentResultRowId++);
            }
            cell = row.createCell(cellFirst + 1);
            cell.setCellValue(pairBoltIdLcId[0]);
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleBolt1);
            } else {
                cell.setCellStyle(cellStyleBolt2);
            }

            // lc id
            cell = row.createCell(cellFirst + 2);
            cell.setCellValue(pairBoltIdLcId[1]);
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleLC1);
            } else {
                cell.setCellStyle(cellStyleLC2);
            }

            // VALUE Fx
            cell = row.createCell(cellFirst + 3);
            cell.setCellValue(((Rbe2NoMagResults) csvParser.getResults().get(partName)).getPairBoltIdLcIdForFx().get(pairBoltIdLcId));
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleResults1);
            } else {
                cell.setCellStyle(cellStyleResults2);
            }

            // VALUE Fy
            cell = row.createCell(cellFirst + 4);
            cell.setCellValue(((Rbe2NoMagResults) csvParser.getResults().get(partName)).getPairBoltIdLcIdForFy().get(pairBoltIdLcId));
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleResults1);
            } else {
                cell.setCellStyle(cellStyleResults2);
            }

            // VALUE Fz
            cell = row.createCell(cellFirst + 5);
            cell.setCellValue(((Rbe2NoMagResults) csvParser.getResults().get(partName)).getPairBoltIdLcIdForFz().get(pairBoltIdLcId));
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleResults1);
            } else {
                cell.setCellStyle(cellStyleResults2);
            }

            // VALUE no other force - do nothing
            cell = row.createCell(cellFirst + 6);
            // empty cell - no Fmag
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleResults1);
            } else {
                cell.setCellStyle(cellStyleResults2);
            }

            // VALUE CRI FACTOR - do nothing
            cell = row.createCell(cellFirst + 7);
            // for rbe2 this will not be calculated
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleResults1);
            } else {
                cell.setCellStyle(cellStyleResults2);
            }
        }
    }

    private void createTableCriticalBolt(
            Sheet sheet, int rowStart, int cellFirst,
            CellStyle cellStyleHeaderTopFontCenter, CellStyle cellStyleFirstCriteriumResultsStyle,
            CellStyle cellStyleFirstCriteriumResultsBoldStyle, CellStyle cellStyleFirstCriteriumCriterStyle,
            String force1Name, String force2Name, String force3Name, String force4Name,
            Integer firstCriterionBoltId, Integer firstCriterionLCid,
            Double firstCriterionForce1, Double firstCriterionForce2, Double firstCriterionForce3, Double firstCriterionForce4,
            Double firstCriterionCriFactor, String firstCriterionName
    ) {
        Row row;
        Cell cell;
        // --- row rowStart: SummaryTitle table - headers
        if (sheet.getRow(rowStart) == null) {
            row = sheet.createRow(rowStart);
        } else {
            row = sheet.getRow(rowStart);
        }
        cell = row.createCell(cellFirst + 2);
        cell.setCellValue("BOLT");   // SummaryTitle
        cell.setCellStyle(cellStyleHeaderTopFontLeft);

        cell = row.createCell(cellFirst + 3);
        cell.setCellValue("LC");
        cell.setCellStyle(cellStyleHeaderTopFontLeft);

        cell = row.createCell(cellFirst + 4);
        cell.setCellValue(force1Name);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(cellFirst + 5);
        cell.setCellValue(force2Name);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(cellFirst + 6);
        cell.setCellValue(force3Name);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(cellFirst + 7);
        cell.setCellValue(force4Name);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(cellFirst + 8);
        cell.setCellValue("cri factor");
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(cellFirst + 9);
        cell.setCellValue("CRITER.");
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        // --- rowStart + 1: Values for first criterion
        if (sheet.getRow(rowStart + 1) == null) {
            row = sheet.createRow(rowStart + 1);
        } else {
            row = sheet.getRow(rowStart + 1);
        }
        cell = row.createCell(cellFirst + 2);
        if (firstCriterionBoltId != null) cell.setCellValue(firstCriterionBoltId);
        cell.setCellStyle(cellStyleBolt0);

        cell = row.createCell(cellFirst + 3);
        if (firstCriterionLCid != null) cell.setCellValue(firstCriterionLCid);
        cell.setCellStyle(cellStyleBolt0);

        cell = row.createCell(cellFirst + 4);
        if (firstCriterionForce1 != null) cell.setCellValue(firstCriterionForce1);
        cell.setCellStyle(cellStyleFirstCriteriumResultsStyle);

        cell = row.createCell(cellFirst + 5);
        if (firstCriterionForce2 != null) cell.setCellValue(firstCriterionForce2);
        cell.setCellStyle(cellStyleFirstCriteriumResultsStyle);

        cell = row.createCell(cellFirst + 6);
        if (firstCriterionForce3 != null) cell.setCellValue(firstCriterionForce3);
        cell.setCellStyle(cellStyleFirstCriteriumResultsStyle);

        cell = row.createCell(cellFirst + 7);
        if (firstCriterionForce4 != null) cell.setCellValue(firstCriterionForce4);
        cell.setCellStyle(cellStyleFirstCriteriumResultsStyle);

        cell = row.createCell(cellFirst + 8);
        if (firstCriterionCriFactor != null) cell.setCellValue(firstCriterionCriFactor);
        cell.setCellStyle(cellStyleFirstCriteriumResultsBoldStyle);

        cell = row.createCell(cellFirst + 9);
        if (firstCriterionName != null) cell.setCellValue(firstCriterionName);
        cell.setCellStyle(cellStyleFirstCriteriumCriterStyle);
    }

    private void createTableByCriteriumBolt(
            Sheet sheet, int rowStart, int cellFirst,
            CellStyle cellStyleHeaderTopFontCenter,

            CellStyle cellStyleFirstCriteriumResultsStyle,
            CellStyle cellStyleFirstCriteriumResultsBoldStyle, CellStyle cellStyleFirstCriteriumCriterStyle,
            String force1Name, String force2Name, String force3Name, String force4Name,
            Integer firstCriterionBoltId, Integer firstCriterionLCid,
            Double firstCriterionForce1, Double firstCriterionForce2, Double firstCriterionForce3, Double firstCriterionForce4,
            Double firstCriterionCriFactor, String firstCriterionName,

            CellStyle cellStyleSecondCriteriumResultsStyle,
            CellStyle cellStyleSecondCriteriumResultsBoldStyle, CellStyle cellStyleSecondCriteriumCriterStyle,
            Integer secondCriterionBoltId, Integer secondCriterionLCid,
            Double secondCriterionForce1, Double secondCriterionForce2, Double secondCriterionForce3, Double secondCriterionForce4,
            Double secondCriterionCriFactor, String secondCriterionName,

            CellStyle cellStyleThirdCriteriumResultsStyle,
            CellStyle cellStyleThirdCriteriumResultsBoldStyle, CellStyle cellStyleThirdCriteriumCriterStyle,
            Integer thirdCriterionBoltId, Integer thirdCriterionLCid,
            Double thirdCriterionForce1, Double thirdCriterionForce2, Double thirdCriterionForce3, Double thirdCriterionForce4,
            Double thirdCriterionCriFactor, String thirdCriterionName,

            CellStyle cellStyleFourthCriteriumResultsStyle,
            CellStyle cellStyleFourthCriteriumResultsBoldStyle, CellStyle cellStyleFourthCriteriumCriterStyle,
            Integer fourthCriterionBoltId, Integer fourthCriterionLCid,
            Double fourthCriterionForce1, Double fourthCriterionForce2, Double fourthCriterionForce3, Double fourthCriterionForce4,
            Double fourthCriterionCriFactor, String fourthCriterionName
    ) {
        Row row;
        Cell cell;
        // --- row rowStart: SummaryTitle table - headers
        if (sheet.getRow(rowStart) == null) {
            row = sheet.createRow(rowStart);
        } else {
            row = sheet.getRow(rowStart);
        }
        cell = row.createCell(cellFirst + 2);
        cell.setCellValue("BOLT");   // SummaryTitle
        cell.setCellStyle(cellStyleHeaderTopFontLeft);

        cell = row.createCell(cellFirst + 3);
        cell.setCellValue("LC");
        cell.setCellStyle(cellStyleHeaderTopFontLeft);

        cell = row.createCell(cellFirst + 4);
        cell.setCellValue(force1Name);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(cellFirst + 5);
        cell.setCellValue(force2Name);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(cellFirst + 6);
        cell.setCellValue(force3Name);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(cellFirst + 7);
        cell.setCellValue(force4Name);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(cellFirst + 8);
        cell.setCellValue("cri factor");
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(cellFirst + 9);
        cell.setCellValue("CRITER.");
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        // --- rowStart + 1: Values for first criterion
        if (sheet.getRow(rowStart + 1) == null) {
            row = sheet.createRow(rowStart + 1);
        } else {
            row = sheet.getRow(rowStart + 1);
        }
        cell = row.createCell(cellFirst + 2);
        if (firstCriterionBoltId != null) cell.setCellValue(firstCriterionBoltId);
        cell.setCellStyle(cellStyleBolt0);

        cell = row.createCell(cellFirst + 3);
        if (firstCriterionLCid != null) cell.setCellValue(firstCriterionLCid);
        cell.setCellStyle(cellStyleBolt0);

        cell = row.createCell(cellFirst + 4);
        if (firstCriterionForce1 != null) cell.setCellValue(firstCriterionForce1);
        cell.setCellStyle(cellStyleFirstCriteriumResultsBoldStyle);

        cell = row.createCell(cellFirst + 5);
        if (firstCriterionForce2 != null) cell.setCellValue(firstCriterionForce2);
        cell.setCellStyle(cellStyleFirstCriteriumResultsStyle);

        cell = row.createCell(cellFirst + 6);
        if (firstCriterionForce3 != null) cell.setCellValue(firstCriterionForce3);
        cell.setCellStyle(cellStyleFirstCriteriumResultsStyle);

        cell = row.createCell(cellFirst + 7);
        if (firstCriterionForce4 != null) cell.setCellValue(firstCriterionForce4);
        cell.setCellStyle(cellStyleFirstCriteriumResultsStyle);

        cell = row.createCell(cellFirst + 8);
        if (firstCriterionCriFactor != null) cell.setCellValue(firstCriterionCriFactor);
        cell.setCellStyle(cellStyleFirstCriteriumResultsStyle);

        cell = row.createCell(cellFirst + 9);
        if (firstCriterionName != null) cell.setCellValue(firstCriterionName);
        cell.setCellStyle(cellStyleFirstCriteriumCriterStyle);

        // --- rowStart + 2: Values for second criterion ---------------------------------------------------------------
        if (sheet.getRow(rowStart + 2) == null) {
            row = sheet.createRow(rowStart + 2);
        } else {
            row = sheet.getRow(rowStart + 2);
        }
        cell = row.createCell(cellFirst + 2);
        if (secondCriterionBoltId != null) cell.setCellValue(secondCriterionBoltId);
        cell.setCellStyle(cellStyleBolt0);

        cell = row.createCell(cellFirst + 3);
        if (secondCriterionLCid != null) cell.setCellValue(secondCriterionLCid);
        cell.setCellStyle(cellStyleBolt0);

        cell = row.createCell(cellFirst + 4);
        if (secondCriterionForce1 != null) cell.setCellValue(secondCriterionForce1);
        cell.setCellStyle(cellStyleSecondCriteriumResultsStyle);

        cell = row.createCell(cellFirst + 5);
        if (secondCriterionForce2 != null) cell.setCellValue(secondCriterionForce2);
        cell.setCellStyle(cellStyleSecondCriteriumResultsBoldStyle);

        cell = row.createCell(cellFirst + 6);
        if (secondCriterionForce3 != null) cell.setCellValue(secondCriterionForce3);
        cell.setCellStyle(cellStyleSecondCriteriumResultsStyle);

        cell = row.createCell(cellFirst + 7);
        if (secondCriterionForce4 != null) cell.setCellValue(secondCriterionForce4);
        cell.setCellStyle(cellStyleSecondCriteriumResultsStyle);

        cell = row.createCell(cellFirst + 8);
        if (secondCriterionCriFactor != null) cell.setCellValue(secondCriterionCriFactor);
        cell.setCellStyle(cellStyleSecondCriteriumResultsStyle);

        cell = row.createCell(cellFirst + 9);
        if (secondCriterionName != null) cell.setCellValue(secondCriterionName);
        cell.setCellStyle(cellStyleSecondCriteriumCriterStyle);

        // --- rowStart + 3: Values for third criterion ---------------------------------------------------------------
        if (sheet.getRow(rowStart + 3) == null) {
            row = sheet.createRow(rowStart + 3);
        } else {
            row = sheet.getRow(rowStart + 3);
        }
        cell = row.createCell(cellFirst + 2);
        if (thirdCriterionBoltId != null) cell.setCellValue(thirdCriterionBoltId);
        cell.setCellStyle(cellStyleBolt0);

        cell = row.createCell(cellFirst + 3);
        if (thirdCriterionLCid != null) cell.setCellValue(thirdCriterionLCid);
        cell.setCellStyle(cellStyleBolt0);

        cell = row.createCell(cellFirst + 4);
        if (thirdCriterionForce1 != null) cell.setCellValue(thirdCriterionForce1);
        cell.setCellStyle(cellStyleThirdCriteriumResultsStyle);

        cell = row.createCell(cellFirst + 5);
        if (thirdCriterionForce2 != null) cell.setCellValue(thirdCriterionForce2);
        cell.setCellStyle(cellStyleThirdCriteriumResultsStyle);

        cell = row.createCell(cellFirst + 6);
        if (thirdCriterionForce3 != null) cell.setCellValue(thirdCriterionForce3);
        cell.setCellStyle(cellStyleThirdCriteriumResultsBoldStyle);

        cell = row.createCell(cellFirst + 7);
        if (thirdCriterionForce4 != null) cell.setCellValue(thirdCriterionForce4);
        cell.setCellStyle(cellStyleThirdCriteriumResultsStyle);

        cell = row.createCell(cellFirst + 8);
        if (thirdCriterionCriFactor != null) cell.setCellValue(thirdCriterionCriFactor);
        cell.setCellStyle(cellStyleThirdCriteriumResultsStyle);

        cell = row.createCell(cellFirst + 9);
        if (thirdCriterionName != null) cell.setCellValue(thirdCriterionName);
        cell.setCellStyle(cellStyleThirdCriteriumCriterStyle);

        // --- rowStart + 4: Values for fouth criterion ---------------------------------------------------------------
        if (sheet.getRow(rowStart + 4) == null) {
            row = sheet.createRow(rowStart + 4);
        } else {
            row = sheet.getRow(rowStart + 4);
        }
        cell = row.createCell(cellFirst + 2);
        if (fourthCriterionBoltId != null) cell.setCellValue(fourthCriterionBoltId);
        cell.setCellStyle(cellStyleBolt0);

        cell = row.createCell(cellFirst + 3);
        if (fourthCriterionLCid != null) cell.setCellValue(fourthCriterionLCid);
        cell.setCellStyle(cellStyleBolt0);

        cell = row.createCell(cellFirst + 4);
        if (fourthCriterionForce1 != null) cell.setCellValue(fourthCriterionForce1);
        cell.setCellStyle(cellStyleFourthCriteriumResultsStyle);

        cell = row.createCell(cellFirst + 5);
        if (fourthCriterionForce2 != null) cell.setCellValue(fourthCriterionForce2);
        cell.setCellStyle(cellStyleFourthCriteriumResultsStyle);

        cell = row.createCell(cellFirst + 6);
        if (fourthCriterionForce3 != null) cell.setCellValue(fourthCriterionForce3);
        cell.setCellStyle(cellStyleFourthCriteriumResultsStyle);

        cell = row.createCell(cellFirst + 7);
        if (fourthCriterionForce4 != null) cell.setCellValue(fourthCriterionForce4);
        cell.setCellStyle(cellStyleFourthCriteriumResultsBoldStyle);

        cell = row.createCell(cellFirst + 8);
        if (fourthCriterionCriFactor != null) cell.setCellValue(fourthCriterionCriFactor);
        cell.setCellStyle(cellStyleFourthCriteriumResultsStyle);

        cell = row.createCell(cellFirst + 9);
        if (fourthCriterionName != null) cell.setCellValue(fourthCriterionName);
        cell.setCellStyle(cellStyleFourthCriteriumCriterStyle);

        //FIXME do this later
        //sheet.addMergedRegion(new CellRangeAddress(row.getRowNum() - 4, row.getRowNum(), cellFirst + 9, cellFirst + 9));
    }


    private void createTableSummary(String SummaryTitle, int rowStart,
                                    String Force1Name, String Force2Name, String Force3Name, String Force4Name,
                                    int cellFirst, Sheet sheet, CellStyle cellStyleHeaderTopFontCenter,
                                    CellStyle cellStyleHeaderLeftCRI, CellStyle cellStyleResults0,
                                    Double value1Force, Double value2Force, Double value3Force, Double value4Force,
                                    Integer value1Lc, Integer value2Lc, Integer value3Lc, Integer value4Lc,
                                    Integer value1Bolt, Integer value2Bolt, Integer value3Bolt, Integer value4Bolt,
                                    Double value1CriFact, Double value2CriFact, Double value3CriFact, Double value4CriFact,
                                    Integer value1Row, Integer value2Row, Integer value3Row, Integer value4Row) {
        Row row;
        Cell cell;
        // --- row rowStart: SummaryTitle table - headers
        if (sheet.getRow(rowStart) == null) {
            row = sheet.createRow(rowStart);
        } else {
            row = sheet.getRow(rowStart);
        }
        cell = row.createCell(cellFirst + 2);
        cell.setCellValue(SummaryTitle);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(cellFirst + 3);
        cell.setCellValue(Force1Name);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(cellFirst + 4);
        cell.setCellValue(Force2Name);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(cellFirst + 5);
        cell.setCellValue(Force3Name);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(cellFirst + 6);
        cell.setCellValue(Force4Name);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        // --- rowStart + 1: Value
        if (sheet.getRow(rowStart + 1) == null) {
            row = sheet.createRow(rowStart + 1);
        } else {
            row = sheet.getRow(rowStart + 1);
        }
        cell = row.createCell(cellFirst + 2);
        cell.setCellValue("Value");
        cell.setCellStyle(cellStyleHeaderLeftCRI);

        cell = row.createCell(cellFirst + 3);
        if (value1Force != null) cell.setCellValue(value1Force);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 4);
        if (value2Force != null) cell.setCellValue(value2Force);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 5);
        if (value3Force != null) cell.setCellValue(value3Force);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 6);
        if (value4Force != null) cell.setCellValue(value4Force);
        cell.setCellStyle(cellStyleResults0);

        // --- rowStart + 2: LC id
        if (sheet.getRow(rowStart + 2) == null) {
            row = sheet.createRow(rowStart + 2);
        } else {
            row = sheet.getRow(rowStart + 2);
        }
        cell = row.createCell(cellFirst + 2);
        cell.setCellValue("LC id");
        cell.setCellStyle(cellStyleHeaderLeftCRI);

        cell = row.createCell(cellFirst + 3);
        if (value1Lc != null) cell.setCellValue(value1Lc);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 4);
        if (value2Lc != null) cell.setCellValue(value2Lc);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 5);
        if (value3Lc != null) cell.setCellValue(value3Lc);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 6);
        if (value4Lc != null) cell.setCellValue(value4Lc);
        cell.setCellStyle(cellStyleResults0);

        // --- rowStart + 3: BOLT id
        if (sheet.getRow(rowStart + 3) == null) {
            row = sheet.createRow(rowStart + 3);
        } else {
            row = sheet.getRow(rowStart + 3);
        }
        cell = row.createCell(cellFirst + 2);
        cell.setCellValue("Bolt id");
        cell.setCellStyle(cellStyleHeaderLeftCRI);

        cell = row.createCell(cellFirst + 3);
        if (value1Bolt != null) cell.setCellValue(value1Bolt);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 4);
        if (value2Bolt != null) cell.setCellValue(value2Bolt);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 5);
        if (value3Bolt != null) cell.setCellValue(value3Bolt);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 6);
        if (value4Bolt != null) cell.setCellValue(value4Bolt);
        cell.setCellStyle(cellStyleResults0);

        // --- rowStart + 4: cri factor
        if (sheet.getRow(rowStart + 4) == null) {
            row = sheet.createRow(rowStart + 4);
        } else {
            row = sheet.getRow(rowStart + 4);
        }
        cell = row.createCell(cellFirst + 2);
        cell.setCellValue("cri factor");
        cell.setCellStyle(cellStyleHeaderLeftCRI);

        cell = row.createCell(cellFirst + 3);
        if (value1CriFact != null) cell.setCellValue(value1CriFact);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 4);
        if (value2CriFact != null) cell.setCellValue(value2CriFact);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 5);
        if (value3CriFact != null) cell.setCellValue(value3CriFact);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 6);
        if (value4CriFact != null) cell.setCellValue(value4CriFact);
        cell.setCellStyle(cellStyleResults0);

        // --- rowStart + 5: extra info
        if (sheet.getRow(rowStart + 5) == null) {
            row = sheet.createRow(rowStart + 5);
        } else {
            row = sheet.getRow(rowStart + 5);
        }
        cell = row.createCell(cellFirst + 2);
        cell.setCellValue("extra info");
        cell.setCellStyle(cellStyleHeaderLeftCRI);

        cell = row.createCell(cellFirst + 3);
        if (value1Row != null) cell.setCellValue(value1Row);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 4);
        if (value2Row != null) cell.setCellValue(value2Row);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 5);
        if (value3Row != null) cell.setCellValue(value3Row);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 6);
        if (value4Row != null) cell.setCellValue(value4Row);
        cell.setCellStyle(cellStyleResults0);
    }

    private void createHeaderForTableResult(int rowStart, int columnStartId, int columnStartOffset, Sheet sheet,
                                            CellStyle cellStyleHeaderTopFontLeft, CellStyle cellStyleHeaderTopFontCenter,
                                            CellStyle cellStyleDescriptionBlank,
                                            String headerBolt, String headerLC,
                                            String Force1Name, String Force2Name, String Force3Name, String Force4Name,
                                            String criFactor, String description) {
        Row row;
        Cell cell;

        if (sheet.getRow(rowStart) == null) {
            row = sheet.createRow(rowStart);
        } else {
            row = sheet.getRow(rowStart);
        }
        cell = row.createCell(columnStartId + 2 + columnStartOffset);
        cell.setCellValue(headerBolt);
        cell.setCellStyle(cellStyleHeaderTopFontLeft);

        cell = row.createCell(columnStartId + 3 + columnStartOffset);
        cell.setCellValue(headerLC);
        cell.setCellStyle(cellStyleHeaderTopFontLeft);

        cell = row.createCell(columnStartId + 4 + columnStartOffset);
        cell.setCellValue(Force1Name);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(columnStartId + 5 + columnStartOffset);
        cell.setCellValue(Force2Name);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(columnStartId + 6 + columnStartOffset);
        cell.setCellValue(Force3Name);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(columnStartId + 7 + columnStartOffset);
        cell.setCellValue(Force4Name);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(columnStartId + 8 + columnStartOffset);
        cell.setCellValue(criFactor);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(columnStartId + 9 + columnStartOffset);
        if (description != null) cell.setCellValue(description);
        cell.setCellStyle(cellStyleDescriptionBlank);
    }

    private int generateExtraSummaryCBAR(String partName, int currentOffset, CsvParserResultExtractor csvParser) {
        int cellFirst = columnCurrentStartId + (COLUMN_OFFSET * currentOffset);
        int cellLast = cellFirst + COLUMN_OFFSET - 1; // currently not in use, delete later

        // calculate critical bolts for specified load cases
        ((CbarResults) csvParser.getResults().get(partName))
                .setCriLoadCaseBolt(LoadCaseType.STATIC, bolTabSetup.getLcIdsForExtraTableStaticLC());

        ((CbarResults) csvParser.getResults().get(partName))
                .setCriLoadCaseBolt(LoadCaseType.FATIGUE, bolTabSetup.getLcIdsForExtraTableFatigueLC());

        ((CbarResults) csvParser.getResults().get(partName))
                .setCriLoadCaseBolt(LoadCaseType.CRASH, bolTabSetup.getLcIdsForExtraTableCrashLC());

        ((CbarResults) csvParser.getResults().get(partName))
                .setMaxLoadCaseBolt(LoadCaseType.STATIC, bolTabSetup.getLcIdsForExtraTableStaticLC());

        ((CbarResults) csvParser.getResults().get(partName))
                .setMaxLoadCaseBolt(LoadCaseType.FATIGUE, bolTabSetup.getLcIdsForExtraTableFatigueLC());

        ((CbarResults) csvParser.getResults().get(partName))
                .setMaxLoadCaseBolt(LoadCaseType.CRASH, bolTabSetup.getLcIdsForExtraTableCrashLC());

        // 1 STATIC
        // --- row 5-10: CRI table - headers, values
        String force1NameFirst = "Fax [N]";
        String force2NameFirst = "Fsh1 [N]";
        String force3NameFirst = "Fsh2 [N]";
        String force4NameFirst = "Fsht [N]";

        // --------- 1 ------------------
        String firstCriterionName = "CRI";
        Double firstCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getCriStaticFaxial();
        Double firstCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getCriStaticFshear1();
        Double firstCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getCriStaticFshear2();
        Double firstCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getCriStaticFshearTotal();

        Integer firstCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getCriStaticLCId();
        Integer firstCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getCriStaticBoltId();
        Double firstCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getCriStaticFactor();

        // --------- 2 ------------------
        String secondCriterionName = "MAX";
        Double secondCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFaxial();
        Double secondCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFaxial_shear1();
        Double secondCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFaxial_shear2();
        Double secondCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFaxial_shearTotal();

        Integer secondCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFaxialLcId();
        Integer secondCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFaxialBoltId();
        Double secondCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFaxialCriFactor();

        // --------- 3 ------------------
        String thirdCriterionName = "MAX";
        Double thirdCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshearTotal_axial();
        Double thirdCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshearTotal_shear1();
        Double thirdCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshearTotal_shear2();
        Double thirdCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshearTotal();

        Integer thirdCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshearTotalLcId();
        Integer thirdCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshearTotalBoltId();
        Double thirdCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshearTotalCriFactor();

        String firstLCname = "STATIC";
        String secondLCname = "STATIC";
        String thirdLCname = "STATIC";

        boolean printHeaders = true;
        int rowStart = 5;
        createExtraTableSummaryFirstCBAR(
                sheet, rowStart, cellFirst,

                printHeaders,

                cellStyleHeaderTopFontCenter,
                cellStyleResultsCriCriterion, firstLCname,
                cellStyleResultsMaxCriterion, secondLCname,
                cellStyleResultsMaxCriterion, thirdLCname,

                cellStyleHeaderTopFontCenter,

                force1NameFirst, force2NameFirst, force3NameFirst, force4NameFirst,

                cellStyleResultsCriNormal, cellStyleResultsCriBold,
                cellStyleResultsCriCriterion,
                firstCriterionBoltId, firstCriterionLCid,
                firstCriterionForce1, firstCriterionForce2, firstCriterionForce3, firstCriterionForce4,
                firstCriterionCriFactor, firstCriterionName,

                cellStyleResultsMaxNormal, cellStyleResultsMaxBold,
                cellStyleResultsMaxCriterion,
                secondCriterionBoltId, secondCriterionLCid,
                secondCriterionForce1, secondCriterionForce2, secondCriterionForce3, secondCriterionForce4,
                secondCriterionCriFactor, secondCriterionName,

                cellStyleResultsMaxNormal, cellStyleResultsMaxBold,
                cellStyleResultsMaxCriterion,
                thirdCriterionBoltId, thirdCriterionLCid,
                thirdCriterionForce1, thirdCriterionForce2, thirdCriterionForce3, thirdCriterionForce4,
                thirdCriterionCriFactor, thirdCriterionName
        );

        // 2 CRASH rows: 8-9-11
        // --------- 1 ------------------
        firstCriterionName = "CRI";
        firstCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getCriCrashFaxial();
        firstCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getCriCrashFshear1();
        firstCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getCriCrashFshear2();
        firstCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getCriCrashFshearTotal();

        firstCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getCriCrashLCId();
        firstCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getCriCrashBoltId();
        firstCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getCriCrashFactor();

        // --------- 2 ------------------
        secondCriterionName = "MAX";
        secondCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFaxial();
        secondCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFaxial_shear1();
        secondCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFaxial_shear2();
        secondCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFaxial_shearTotal();

        secondCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFaxialLcId();
        secondCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFaxialBoltId();
        secondCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFaxialCriFactor();

        // --------- 3 ------------------
        thirdCriterionName = "MAX";
        thirdCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshearTotal_axial();
        thirdCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshearTotal_shear1();
        thirdCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshearTotal_shear2();
        thirdCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshearTotal();

        thirdCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshearTotalLcId();
        thirdCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshearTotalBoltId();
        thirdCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshearTotalCriFactor();

        firstLCname = "CRASH";
        secondLCname = "CRASH";
        thirdLCname = "CRASH";

        printHeaders = true;
        rowStart = 10;
        createExtraTableSummaryFirstCBAR(
                sheet, rowStart, cellFirst,

                printHeaders,

                cellStyleHeaderTopFontCenter,
                cellStyleResultsCriCriterion, firstLCname,
                cellStyleResultsMaxCriterion, secondLCname,
                cellStyleResultsMaxCriterion, thirdLCname,

                cellStyleHeaderTopFontCenter,

                force1NameFirst, force2NameFirst, force3NameFirst, force4NameFirst,

                cellStyleResultsCriNormal, cellStyleResultsCriBold,
                cellStyleResultsCriCriterion,
                firstCriterionBoltId, firstCriterionLCid,
                firstCriterionForce1, firstCriterionForce2, firstCriterionForce3, firstCriterionForce4,
                firstCriterionCriFactor, firstCriterionName,

                cellStyleResultsMaxNormal, cellStyleResultsMaxBold,
                cellStyleResultsMaxCriterion,
                secondCriterionBoltId, secondCriterionLCid,
                secondCriterionForce1, secondCriterionForce2, secondCriterionForce3, secondCriterionForce4,
                secondCriterionCriFactor, secondCriterionName,

                cellStyleResultsMaxNormal, cellStyleResultsMaxBold,
                cellStyleResultsMaxCriterion,
                thirdCriterionBoltId, thirdCriterionLCid,
                thirdCriterionForce1, thirdCriterionForce2, thirdCriterionForce3, thirdCriterionForce4,
                thirdCriterionCriFactor, thirdCriterionName
        );

        // 3 FATIGUE rows: 11-12-14
        // --------- 1 ------------------
        firstCriterionName = "CRI";
        firstCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getCriFatigueFaxial();
        firstCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getCriFatigueFshear1();
        firstCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getCriFatigueFshear2();
        firstCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getCriFatigueFshearTotal();

        firstCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getCriFatigueLCId();
        firstCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getCriFatigueBoltId();
        firstCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getCriFatigueFactor();

        // --------- 2 ------------------
        secondCriterionName = "MAX";
        secondCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFaxial();
        secondCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFaxial_shear1();
        secondCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFaxial_shear2();
        secondCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFaxial_shearTotal();

        secondCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFaxialLcId();
        secondCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFaxialBoltId();
        secondCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFaxialCriFactor();

        // --------- 3 ------------------
        thirdCriterionName = "MAX";
        thirdCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshearTotal_axial();
        thirdCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshearTotal_shear1();
        thirdCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshearTotal_shear2();
        thirdCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshearTotal();

        thirdCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshearTotalLcId();
        thirdCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshearTotalBoltId();
        thirdCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshearTotalCriFactor();

        firstLCname = "FATIGUE";
        secondLCname = "FATIGUE";
        thirdLCname = "FATIGUE";

        printHeaders = true;
        rowStart = 15;
        createExtraTableSummaryFirstCBAR(
                sheet, rowStart, cellFirst,

                printHeaders,

                cellStyleHeaderTopFontCenter,
                cellStyleResultsCriCriterion, firstLCname,
                cellStyleResultsMaxCriterion, secondLCname,
                cellStyleResultsMaxCriterion, thirdLCname,

                cellStyleHeaderTopFontCenter,

                force1NameFirst, force2NameFirst, force3NameFirst, force4NameFirst,

                cellStyleResultsCriNormal, cellStyleResultsCriBold,
                cellStyleResultsCriCriterion,
                firstCriterionBoltId, firstCriterionLCid,
                firstCriterionForce1, firstCriterionForce2, firstCriterionForce3, firstCriterionForce4,
                firstCriterionCriFactor, firstCriterionName,

                cellStyleResultsMaxNormal, cellStyleResultsMaxBold,
                cellStyleResultsMaxCriterion,
                secondCriterionBoltId, secondCriterionLCid,
                secondCriterionForce1, secondCriterionForce2, secondCriterionForce3, secondCriterionForce4,
                secondCriterionCriFactor, secondCriterionName,

                cellStyleResultsMaxNormal, cellStyleResultsMaxBold,
                cellStyleResultsMaxCriterion,
                thirdCriterionBoltId, thirdCriterionLCid,
                thirdCriterionForce1, thirdCriterionForce2, thirdCriterionForce3, thirdCriterionForce4,
                thirdCriterionCriFactor, thirdCriterionName
        );

        int currentRowLocation = 50;
        return currentRowLocation;
    }

    private void createExtraTableSummaryFirstCBAR(
            Sheet sheet, int rowStart, int cellFirst,

            boolean printHeaders,

            CellStyle cellStyleHeaderLCgroup,
            CellStyle cellStyleFirstLCNameStyle, String firstLCname,
            CellStyle cellStyleSecondLCNameStyle, String secondLCname,
            CellStyle cellStyleThirdLCNameStyle, String thirdLCname,

            CellStyle cellStyleHeaderTopFontCenter,

            String force1NameFirst, String force2NameFirst, String force3NameFirst, String force4NameFirst,

            CellStyle cellStyleFirstCriteriumResultsNormalStyle, CellStyle cellStyleFirstCriteriumResultsBoldStyle,
            CellStyle cellStyleFirstCriteriumCriterStyle,
            Integer firstCriterionBoltId, Integer firstCriterionLCid,
            Double firstCriterionForce1, Double firstCriterionForce2, Double firstCriterionForce3, Double firstCriterionForce4,
            Double firstCriterionCriFactor, String firstCriterionName,

            CellStyle cellStyleSecondCriteriumResultsNormalStyle, CellStyle cellStyleSecondCriteriumResultsBoldStyle,
            CellStyle cellStyleSecondCriteriumCriterStyle,
            Integer secondCriterionBoltId, Integer secondCriterionLCid,
            Double secondCriterionForce1, Double secondCriterionForce2, Double secondCriterionForce3, Double secondCriterionForce4,
            Double secondCriterionCriFactor, String secondCriterionName,

            CellStyle cellStyleThirdCriteriumResultsNormalStyle, CellStyle cellStyleThirdCriteriumResultsBoldStyle,
            CellStyle cellStyleThirdCriteriumCriterStyle,
            Integer thirdCriterionBoltId, Integer thirdCriterionLCid,
            Double thirdCriterionForce1, Double thirdCriterionForce2, Double thirdCriterionForce3, Double thirdCriterionForce4,
            Double thirdCriterionCriFactor, String thirdCriterionName
    ) {
        Row row;
        Cell cell;

        if (printHeaders) {
            // --- 1) rowStart: SummaryTitle table - headers ---------------------------------------------------------------
            if (sheet.getRow(rowStart) == null) {
                row = sheet.createRow(rowStart);
            } else {
                row = sheet.getRow(rowStart);
            }

            cell = row.createCell(cellFirst + 1);
            cell.setCellValue("LC group");   // Loadcase group name
            cell.setCellStyle(cellStyleHeaderTopFontCenter);

            cell = row.createCell(cellFirst + 2);
            cell.setCellValue("BOLT");   // SummaryTitle
            cell.setCellStyle(cellStyleHeaderTopFontLeft);

            cell = row.createCell(cellFirst + 3);
            cell.setCellValue("LC");
            cell.setCellStyle(cellStyleHeaderTopFontLeft);

            cell = row.createCell(cellFirst + 4);
            cell.setCellValue(force1NameFirst);
            cell.setCellStyle(cellStyleHeaderTopFontCenter);

            cell = row.createCell(cellFirst + 5);
            cell.setCellValue(force2NameFirst);
            cell.setCellStyle(cellStyleHeaderTopFontCenter);

            cell = row.createCell(cellFirst + 6);
            cell.setCellValue(force3NameFirst);
            cell.setCellStyle(cellStyleHeaderTopFontCenter);

            cell = row.createCell(cellFirst + 7);
            cell.setCellValue(force4NameFirst);
            cell.setCellStyle(cellStyleHeaderTopFontCenter);

            cell = row.createCell(cellFirst + 8);
            cell.setCellValue("cri factor");
            cell.setCellStyle(cellStyleHeaderTopFontCenter);

            cell = row.createCell(cellFirst + 9);
            cell.setCellValue("CRITER.");
            cell.setCellStyle(cellStyleHeaderTopFontCenter);
        }

        // --- 2) rowStart + 1: Values for Bolt critical criterion -----------------------------------------------------
        if (sheet.getRow(rowStart + 1) == null) {
            row = sheet.createRow(rowStart + 1);
        } else {
            row = sheet.getRow(rowStart + 1);
        }
        cell = row.createCell(cellFirst + 1);
        if (firstLCname != null) cell.setCellValue(firstLCname);
        cell.setCellStyle(cellStyleFirstLCNameStyle);

        cell = row.createCell(cellFirst + 2);
        if (firstCriterionBoltId != null) cell.setCellValue(firstCriterionBoltId);
        cell.setCellStyle(cellStyleBolt0);

        cell = row.createCell(cellFirst + 3);
        if (firstCriterionLCid != null) cell.setCellValue(firstCriterionLCid);
        cell.setCellStyle(cellStyleBolt0);

        cell = row.createCell(cellFirst + 4);
        if (firstCriterionForce1 != null) cell.setCellValue(firstCriterionForce1);
        cell.setCellStyle(cellStyleFirstCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 5);
        if (firstCriterionForce2 != null) cell.setCellValue(firstCriterionForce2);
        cell.setCellStyle(cellStyleFirstCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 6);
        if (firstCriterionForce3 != null) cell.setCellValue(firstCriterionForce3);
        cell.setCellStyle(cellStyleFirstCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 7);
        if (firstCriterionForce4 != null) cell.setCellValue(firstCriterionForce4);
        cell.setCellStyle(cellStyleFirstCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 8);
        if (firstCriterionCriFactor != null) cell.setCellValue(firstCriterionCriFactor);
        cell.setCellStyle(cellStyleFirstCriteriumResultsBoldStyle);

        cell = row.createCell(cellFirst + 9);
        if (firstCriterionName != null) cell.setCellValue(firstCriterionName);
        cell.setCellStyle(cellStyleFirstCriteriumCriterStyle);

        // --- 3) rowStart + 2: Values for Fax MAX criterion -----------------------------------------------------------
        if (sheet.getRow(rowStart + 2) == null) {
            row = sheet.createRow(rowStart + 2);
        } else {
            row = sheet.getRow(rowStart + 2);
        }
        cell = row.createCell(cellFirst + 1);
        if (secondLCname != null) cell.setCellValue(secondLCname);
        cell.setCellStyle(cellStyleSecondLCNameStyle);

        cell = row.createCell(cellFirst + 2);
        if (secondCriterionBoltId != null) cell.setCellValue(secondCriterionBoltId);
        cell.setCellStyle(cellStyleBolt0);

        cell = row.createCell(cellFirst + 3);
        if (secondCriterionLCid != null) cell.setCellValue(secondCriterionLCid);
        cell.setCellStyle(cellStyleBolt0);

        cell = row.createCell(cellFirst + 4);
        if (secondCriterionForce1 != null) cell.setCellValue(secondCriterionForce1);
        cell.setCellStyle(cellStyleSecondCriteriumResultsBoldStyle);

        cell = row.createCell(cellFirst + 5);
        if (secondCriterionForce2 != null) cell.setCellValue(secondCriterionForce2);
        cell.setCellStyle(cellStyleSecondCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 6);
        if (secondCriterionForce3 != null) cell.setCellValue(secondCriterionForce3);
        cell.setCellStyle(cellStyleSecondCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 7);
        if (secondCriterionForce4 != null) cell.setCellValue(secondCriterionForce4);
        cell.setCellStyle(cellStyleSecondCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 8);
        if (secondCriterionCriFactor != null) cell.setCellValue(secondCriterionCriFactor);
        cell.setCellStyle(cellStyleSecondCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 9);
        if (secondCriterionName != null) cell.setCellValue(secondCriterionName);
        cell.setCellStyle(cellStyleSecondCriteriumCriterStyle);

        // --- 4) rowStart + 3: Values for Fsh total MAX criterion -----------------------------------------------------
        if (sheet.getRow(rowStart + 3) == null) {
            row = sheet.createRow(rowStart + 3);
        } else {
            row = sheet.getRow(rowStart + 3);
        }
        cell = row.createCell(cellFirst + 1);
        if (thirdLCname != null) cell.setCellValue(thirdLCname);
        cell.setCellStyle(cellStyleThirdLCNameStyle);

        cell = row.createCell(cellFirst + 2);
        if (thirdCriterionBoltId != null) cell.setCellValue(thirdCriterionBoltId);
        cell.setCellStyle(cellStyleBolt0);

        cell = row.createCell(cellFirst + 3);
        if (thirdCriterionLCid != null) cell.setCellValue(thirdCriterionLCid);
        cell.setCellStyle(cellStyleBolt0);

        cell = row.createCell(cellFirst + 4);
        if (thirdCriterionForce1 != null) cell.setCellValue(thirdCriterionForce1);
        cell.setCellStyle(cellStyleThirdCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 5);
        if (thirdCriterionForce2 != null) cell.setCellValue(thirdCriterionForce2);
        cell.setCellStyle(cellStyleThirdCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 6);
        if (thirdCriterionForce3 != null) cell.setCellValue(thirdCriterionForce3);
        cell.setCellStyle(cellStyleThirdCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 7);
        if (thirdCriterionForce4 != null) cell.setCellValue(thirdCriterionForce4);
        cell.setCellStyle(cellStyleThirdCriteriumResultsBoldStyle);

        cell = row.createCell(cellFirst + 8);
        if (thirdCriterionCriFactor != null) cell.setCellValue(thirdCriterionCriFactor);
        cell.setCellStyle(cellStyleThirdCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 9);
        if (thirdCriterionName != null) cell.setCellValue(thirdCriterionName);
        cell.setCellStyle(cellStyleThirdCriteriumCriterStyle);


    }

    private void createExtraTableSummarySecondCBAR(
            Sheet sheet, int rowStart, int cellFirst,

            boolean printHeaders,

            CellStyle cellStyleHeaderLCgroup,
            CellStyle cellStyleFirstLCNameStyle, String firstLCname,
            CellStyle cellStyleSecondLCNameStyle, String secondLCname,
            CellStyle cellStyleThirdLCNameStyle, String thirdLCname,
            CellStyle cellStyleFourthLCNameStyle, String fourthLCname,

            CellStyle cellStyleHeaderTopFontCenter,

            String force1NameFirst, String force2NameFirst, String force3NameFirst, String force4NameFirst,

            CellStyle cellStyleFirstCriteriumResultsNormalStyle, CellStyle cellStyleFirstCriteriumResultsBoldStyle,
            CellStyle cellStyleFirstCriteriumCriterStyle,
            Integer firstCriterionBoltId, Integer firstCriterionLCid,
            Double firstCriterionForce1, Double firstCriterionForce2, Double firstCriterionForce3, Double firstCriterionForce4,
            Double firstCriterionCriFactor, String firstCriterionName,

            CellStyle cellStyleSecondCriteriumResultsNormalStyle, CellStyle cellStyleSecondCriteriumResultsBoldStyle,
            CellStyle cellStyleSecondCriteriumCriterStyle,
            Integer secondCriterionBoltId, Integer secondCriterionLCid,
            Double secondCriterionForce1, Double secondCriterionForce2, Double secondCriterionForce3, Double secondCriterionForce4,
            Double secondCriterionCriFactor, String secondCriterionName,

            CellStyle cellStyleThirdCriteriumResultsNormalStyle, CellStyle cellStyleThirdCriteriumResultsBoldStyle,
            CellStyle cellStyleThirdCriteriumCriterStyle,
            Integer thirdCriterionBoltId, Integer thirdCriterionLCid,
            Double thirdCriterionForce1, Double thirdCriterionForce2, Double thirdCriterionForce3, Double thirdCriterionForce4,
            Double thirdCriterionCriFactor, String thirdCriterionName,

            CellStyle cellStyleFourthCriteriumResultsNormalStyle, CellStyle cellStyleFourthCriteriumResultsBoldStyle,
            CellStyle cellStyleFourthCriteriumCriterStyle,
            Integer fourthCriterionBoltId, Integer fourthCriterionLCid,
            Double fourthCriterionForce1, Double fourthCriterionForce2, Double fourthCriterionForce3, Double fourthCriterionForce4,
            Double fourthCriterionCriFactor, String fourthCriterionName
    ) {
        Row row;
        Cell cell;

        if (printHeaders) {
            // --- 1) rowStart: SummaryTitle table - headers ---------------------------------------------------------------
            if (sheet.getRow(rowStart) == null) {
                row = sheet.createRow(rowStart);
            } else {
                row = sheet.getRow(rowStart);
            }

            cell = row.createCell(cellFirst + 1);
            cell.setCellValue("LC group");   // Loadcase group name
            cell.setCellStyle(cellStyleHeaderTopFontCenter);

            cell = row.createCell(cellFirst + 2);
            cell.setCellValue("BOLT");   // SummaryTitle
            cell.setCellStyle(cellStyleHeaderTopFontLeft);

            cell = row.createCell(cellFirst + 3);
            cell.setCellValue("LC");
            cell.setCellStyle(cellStyleHeaderTopFontLeft);

            cell = row.createCell(cellFirst + 4);
            cell.setCellValue(force1NameFirst);
            cell.setCellStyle(cellStyleHeaderTopFontCenter);

            cell = row.createCell(cellFirst + 5);
            cell.setCellValue(force2NameFirst);
            cell.setCellStyle(cellStyleHeaderTopFontCenter);

            cell = row.createCell(cellFirst + 6);
            cell.setCellValue(force3NameFirst);
            cell.setCellStyle(cellStyleHeaderTopFontCenter);

            cell = row.createCell(cellFirst + 7);
            cell.setCellValue(force4NameFirst);
            cell.setCellStyle(cellStyleHeaderTopFontCenter);

            cell = row.createCell(cellFirst + 8);
            cell.setCellValue("cri factor");
            cell.setCellStyle(cellStyleHeaderTopFontCenter);

            cell = row.createCell(cellFirst + 9);
            cell.setCellValue("CRITER.");
            cell.setCellStyle(cellStyleHeaderTopFontCenter);
        }

        // --- 2) rowStart + 1: Fax  -----------------------------------------------------
        if (sheet.getRow(rowStart + 1) == null) {
            row = sheet.createRow(rowStart + 1);
        } else {
            row = sheet.getRow(rowStart + 1);
        }
        cell = row.createCell(cellFirst + 1);
        if (firstLCname != null) cell.setCellValue(firstLCname);
        cell.setCellStyle(cellStyleFirstLCNameStyle);

        cell = row.createCell(cellFirst + 2);
        if (firstCriterionBoltId != null) cell.setCellValue(firstCriterionBoltId);
        cell.setCellStyle(cellStyleBolt0);

        cell = row.createCell(cellFirst + 3);
        if (firstCriterionLCid != null) cell.setCellValue(firstCriterionLCid);
        cell.setCellStyle(cellStyleBolt0);

        cell = row.createCell(cellFirst + 4);
        if (firstCriterionForce1 != null) cell.setCellValue(firstCriterionForce1);
        cell.setCellStyle(cellStyleFirstCriteriumResultsBoldStyle);

        cell = row.createCell(cellFirst + 5);
        if (firstCriterionForce2 != null) cell.setCellValue(firstCriterionForce2);
        cell.setCellStyle(cellStyleFirstCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 6);
        if (firstCriterionForce3 != null) cell.setCellValue(firstCriterionForce3);
        cell.setCellStyle(cellStyleFirstCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 7);
        if (firstCriterionForce4 != null) cell.setCellValue(firstCriterionForce4);
        cell.setCellStyle(cellStyleFirstCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 8);
        if (firstCriterionCriFactor != null) cell.setCellValue(firstCriterionCriFactor);
        cell.setCellStyle(cellStyleFirstCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 9);
        if (firstCriterionName != null) cell.setCellValue(firstCriterionName);
        cell.setCellStyle(cellStyleFirstCriteriumCriterStyle);

        // --- 3) rowStart + 2: Fshear1 -----------------------------------------------------------
        if (sheet.getRow(rowStart + 2) == null) {
            row = sheet.createRow(rowStart + 2);
        } else {
            row = sheet.getRow(rowStart + 2);
        }
        cell = row.createCell(cellFirst + 1);
        if (secondLCname != null) cell.setCellValue(secondLCname);
        cell.setCellStyle(cellStyleSecondLCNameStyle);

        cell = row.createCell(cellFirst + 2);
        if (secondCriterionBoltId != null) cell.setCellValue(secondCriterionBoltId);
        cell.setCellStyle(cellStyleBolt0);

        cell = row.createCell(cellFirst + 3);
        if (secondCriterionLCid != null) cell.setCellValue(secondCriterionLCid);
        cell.setCellStyle(cellStyleBolt0);

        cell = row.createCell(cellFirst + 4);
        if (secondCriterionForce1 != null) cell.setCellValue(secondCriterionForce1);
        cell.setCellStyle(cellStyleSecondCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 5);
        if (secondCriterionForce2 != null) cell.setCellValue(secondCriterionForce2);
        cell.setCellStyle(cellStyleSecondCriteriumResultsBoldStyle);

        cell = row.createCell(cellFirst + 6);
        if (secondCriterionForce3 != null) cell.setCellValue(secondCriterionForce3);
        cell.setCellStyle(cellStyleSecondCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 7);
        if (secondCriterionForce4 != null) cell.setCellValue(secondCriterionForce4);
        cell.setCellStyle(cellStyleSecondCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 8);
        if (secondCriterionCriFactor != null) cell.setCellValue(secondCriterionCriFactor);
        cell.setCellStyle(cellStyleSecondCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 9);
        if (secondCriterionName != null) cell.setCellValue(secondCriterionName);
        cell.setCellStyle(cellStyleSecondCriteriumCriterStyle);

        // --- 4) rowStart + 3: Fshear2 -----------------------------------------------------
        if (sheet.getRow(rowStart + 3) == null) {
            row = sheet.createRow(rowStart + 3);
        } else {
            row = sheet.getRow(rowStart + 3);
        }
        cell = row.createCell(cellFirst + 1);
        if (thirdLCname != null) cell.setCellValue(thirdLCname);
        cell.setCellStyle(cellStyleThirdLCNameStyle);

        cell = row.createCell(cellFirst + 2);
        if (thirdCriterionBoltId != null) cell.setCellValue(thirdCriterionBoltId);
        cell.setCellStyle(cellStyleBolt0);

        cell = row.createCell(cellFirst + 3);
        if (thirdCriterionLCid != null) cell.setCellValue(thirdCriterionLCid);
        cell.setCellStyle(cellStyleBolt0);

        cell = row.createCell(cellFirst + 4);
        if (thirdCriterionForce1 != null) cell.setCellValue(thirdCriterionForce1);
        cell.setCellStyle(cellStyleThirdCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 5);
        if (thirdCriterionForce2 != null) cell.setCellValue(thirdCriterionForce2);
        cell.setCellStyle(cellStyleThirdCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 6);
        if (thirdCriterionForce3 != null) cell.setCellValue(thirdCriterionForce3);
        cell.setCellStyle(cellStyleThirdCriteriumResultsBoldStyle);

        cell = row.createCell(cellFirst + 7);
        if (thirdCriterionForce4 != null) cell.setCellValue(thirdCriterionForce4);
        cell.setCellStyle(cellStyleThirdCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 8);
        if (thirdCriterionCriFactor != null) cell.setCellValue(thirdCriterionCriFactor);
        cell.setCellStyle(cellStyleThirdCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 9);
        if (thirdCriterionName != null) cell.setCellValue(thirdCriterionName);
        cell.setCellStyle(cellStyleThirdCriteriumCriterStyle);

        // --- 5) rowStart + 4: Fshear total -----------------------------------------------------
        if (sheet.getRow(rowStart + 4) == null) {
            row = sheet.createRow(rowStart + 4);
        } else {
            row = sheet.getRow(rowStart + 4);
        }
        cell = row.createCell(cellFirst + 1);
        if (fourthLCname != null) cell.setCellValue(fourthLCname);
        cell.setCellStyle(cellStyleFourthLCNameStyle);

        cell = row.createCell(cellFirst + 2);
        if (fourthCriterionBoltId != null) cell.setCellValue(fourthCriterionBoltId);
        cell.setCellStyle(cellStyleBolt0);

        cell = row.createCell(cellFirst + 3);
        if (fourthCriterionLCid != null) cell.setCellValue(fourthCriterionLCid);
        cell.setCellStyle(cellStyleBolt0);

        cell = row.createCell(cellFirst + 4);
        if (fourthCriterionForce1 != null) cell.setCellValue(fourthCriterionForce1);
        cell.setCellStyle(cellStyleFourthCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 5);
        if (fourthCriterionForce2 != null) cell.setCellValue(fourthCriterionForce2);
        cell.setCellStyle(cellStyleFourthCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 6);
        if (fourthCriterionForce3 != null) cell.setCellValue(fourthCriterionForce3);
        cell.setCellStyle(cellStyleFourthCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 7);
        if (fourthCriterionForce4 != null) cell.setCellValue(fourthCriterionForce4);
        cell.setCellStyle(cellStyleFourthCriteriumResultsBoldStyle);

        cell = row.createCell(cellFirst + 8);
        if (fourthCriterionCriFactor != null) cell.setCellValue(fourthCriterionCriFactor);
        cell.setCellStyle(cellStyleFourthCriteriumResultsNormalStyle);

        cell = row.createCell(cellFirst + 9);
        if (fourthCriterionName != null) cell.setCellValue(fourthCriterionName);
        cell.setCellStyle(cellStyleFourthCriteriumCriterStyle);
    }

    private int createExtraTableSummaryCBAR____old(String partName, int rowStart, int cellFirst,
                                                   Sheet sheet, CellStyle cellStyleHeaderTopFontCenter,
                                                   CellStyle cellStyleHeaderLeftCRI, CellStyle cellStyleResults0,
                                                   String Row1Header, String Row2Header, String Row3Header, String Row4Header,
                                                   String ColumnHeader0, String ColumnHeader1, String ColumnHeader2,
                                                   String ColumnHeader3, String ColumnHeader4, String ColumnHeader5,
                                                   String ColumnHeader6, String ColumnHeader7,
                                                   Integer loadCaseId1, Integer boltId1,
                                                   Double force11, Double force12, Double force13, Double force14, Double criFactor1,
                                                   Integer loadCaseId2, Integer boltId2,
                                                   Double force21, Double force22, Double force23, Double force24, Double criFactor2,
                                                   Integer loadCaseId3, Integer boltId3,
                                                   Double force31, Double force32, Double force33, Double force34, Double criFactor3,
                                                   Integer loadCaseId4, Integer boltId4,
                                                   Double force41, Double force42, Double force43, Double force44, Double criFactor4) {
        Row row;
        Cell cell;
        int currentRow = rowStart;
        // --- row rowStart: SummaryTitle table - headers
        if (sheet.getRow(currentRow) == null) {
            row = sheet.createRow(currentRow);
        } else {
            row = sheet.getRow(currentRow);
        }
        cell = row.createCell(cellFirst);
        cell.setCellValue(ColumnHeader0);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(cellFirst + 1);
        cell.setCellValue(ColumnHeader1);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(cellFirst + 2);
        cell.setCellValue(ColumnHeader2);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(cellFirst + 3);
        cell.setCellValue(ColumnHeader3);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(cellFirst + 4);
        cell.setCellValue(ColumnHeader4);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(cellFirst + 5);
        cell.setCellValue(ColumnHeader5);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(cellFirst + 6);
        cell.setCellValue(ColumnHeader6);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        cell = row.createCell(cellFirst + 7);
        cell.setCellValue(ColumnHeader7);
        cell.setCellStyle(cellStyleHeaderTopFontCenter);

        // --- ++currentRow : STATIC
        if (sheet.getRow(currentRow) == null) {
            row = sheet.createRow(currentRow);
        } else {
            row = sheet.getRow(++currentRow);
        }
        cell = row.createCell(cellFirst);
        cell.setCellValue(Row1Header);
        cell.setCellStyle(cellStyleHeaderLeftCRI);

        cell = row.createCell(cellFirst + 1);
        if (loadCaseId1 != null) cell.setCellValue(loadCaseId1);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 2);
        if (boltId1 != null) cell.setCellValue(boltId1);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 3);
        if (force11 != null) cell.setCellValue(force11);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 4);
        if (force12 != null) cell.setCellValue(force12);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 5);
        if (force13 != null) cell.setCellValue(force13);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 6);
        if (force14 != null) cell.setCellValue(force14);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 7);
        if (criFactor1 != null) cell.setCellValue(criFactor1);
        cell.setCellStyle(cellStyleResults0);

        // --- ++currentRow : CRASH
        if (sheet.getRow(currentRow) == null) {
            row = sheet.createRow(++currentRow);
        } else {
            row = sheet.getRow(++currentRow);
        }
        cell = row.createCell(cellFirst);
        cell.setCellValue(Row2Header);
        cell.setCellStyle(cellStyleHeaderLeftCRI);

        cell = row.createCell(cellFirst + 1);
        if (loadCaseId2 != null) cell.setCellValue(loadCaseId2);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 2);
        if (boltId2 != null) cell.setCellValue(boltId2);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 3);
        if (force21 != null) cell.setCellValue(force21);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 4);
        if (force22 != null) cell.setCellValue(force22);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 5);
        if (force23 != null) cell.setCellValue(force23);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 6);
        if (force24 != null) cell.setCellValue(force24);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 7);
        if (criFactor2 != null) cell.setCellValue(criFactor2);
        cell.setCellStyle(cellStyleResults0);

        // --- ++currentRow : FATIGUE
        if (sheet.getRow(currentRow) == null) {
            row = sheet.createRow(++currentRow);
        } else {
            row = sheet.getRow(++currentRow);
        }
        cell = row.createCell(cellFirst);
        cell.setCellValue(Row3Header);
        cell.setCellStyle(cellStyleHeaderLeftCRI);

        cell = row.createCell(cellFirst + 1);
        if (loadCaseId3 != null) cell.setCellValue(loadCaseId3);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 2);
        if (boltId3 != null) cell.setCellValue(boltId3);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 3);
        if (force31 != null) cell.setCellValue(force31);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 4);
        if (force32 != null) cell.setCellValue(force32);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 5);
        if (force33 != null) cell.setCellValue(force33);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 6);
        if (force34 != null) cell.setCellValue(force34);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 7);
        if (criFactor3 != null) cell.setCellValue(criFactor3);
        cell.setCellStyle(cellStyleResults0);

        // --- ++currentRow : extra info
        if (sheet.getRow(currentRow) == null) {
            row = sheet.createRow(++currentRow);
        } else {
            row = sheet.getRow(++currentRow);
        }
        cell = row.createCell(cellFirst);
        cell.setCellValue(Row4Header);
        cell.setCellStyle(cellStyleHeaderLeftCRI);

        cell = row.createCell(cellFirst + 1);
        if (loadCaseId4 != null) cell.setCellValue(loadCaseId4);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 2);
        if (boltId4 != null) cell.setCellValue(boltId4);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 3);
        if (force41 != null) cell.setCellValue(force41);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 4);
        if (force42 != null) cell.setCellValue(force42);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 5);
        if (force43 != null) cell.setCellValue(force43);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 6);
        if (force44 != null) cell.setCellValue(force44);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 7);
        if (criFactor4 != null) cell.setCellValue(criFactor4);
        cell.setCellStyle(cellStyleResults0);

        currentRow += 3;
        return currentRow;
    }


    private int generateExtraSummariesCriMaxMinAbsCBAR(String partName, int currentOffset, CsvParserResultExtractor csvParser) {
        int cellFirst = columnCurrentStartId + (COLUMN_OFFSET * currentOffset);
        int cellLast = cellFirst + COLUMN_OFFSET - 1; // currently not in use, delete later

        String force1NameFirst = "Fax [N]";
        String force2NameFirst = "Fsh1 [N]";
        String force3NameFirst = "Fsh2 [N]";
        String force4NameFirst = "Fsht [N]";

        // 1.1 MAX STATIC ----------------------------------------------------------------------------------------------
        // --------- 1 ------------------ Fax
        String firstCriterionName = "MAX";
        Double firstCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFaxial();
        Double firstCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFaxial_shear1();
        Double firstCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFaxial_shear2();
        Double firstCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFaxial_shearTotal();

        Integer firstCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFaxialLcId();
        Integer firstCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFaxialBoltId();
        Double firstCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFaxialCriFactor();

        // --------- 2 ------------------ Fshear1
        String secondCriterionName = "MAX";
        Double secondCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshear1_axial();
        Double secondCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshear1();
        Double secondCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshear1_shear2();
        Double secondCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshear1_shearTotal();

        Integer secondCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshear1LcId();
        Integer secondCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshear1BoltId();
        Double secondCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshear1CriFactor();

        // --------- 3 ------------------ Fshear2
        String thirdCriterionName = "MAX";
        Double thirdCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshear2_axial();
        Double thirdCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshear2_shear1();
        Double thirdCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshear2();
        Double thirdCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshear2_shearTotal();

        Integer thirdCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshear2LcId();
        Integer thirdCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshear2BoltId();
        Double thirdCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshear2CriFactor();

        // --------- 4 ------------------ FshearTotal
        String fourthCriterionName = "MAX";
        Double fourthCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshearTotal_axial();
        Double fourthCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshearTotal_shear1();
        Double fourthCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshearTotal_shear2();
        Double fourthCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshearTotal();

        Integer fourthCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshearTotalLcId();
        Integer fourthCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshearTotalBoltId();
        Double fourthCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshearTotalCriFactor();

        String firstLCname = "STATIC";
        String secondLCname = "STATIC";
        String thirdLCname = "STATIC";
        String fourthLCname = "STATIC";

        boolean printHeaders = true;
        int rowStart = 23;
        createExtraTableSummarySecondCBAR(
                sheet, rowStart, cellFirst,

                printHeaders,

                cellStyleHeaderTopFontCenter,
                cellStyleResultsMaxCriterion, firstLCname,
                cellStyleResultsMaxCriterion, secondLCname,
                cellStyleResultsMaxCriterion, thirdLCname,
                cellStyleResultsMaxCriterion, fourthLCname,

                cellStyleHeaderTopFontCenter,

                force1NameFirst, force2NameFirst, force3NameFirst, force4NameFirst,

                cellStyleResultsMaxNormal, cellStyleResultsMaxBold,
                cellStyleResultsMaxCriterion,
                firstCriterionBoltId, firstCriterionLCid,
                firstCriterionForce1, firstCriterionForce2, firstCriterionForce3, firstCriterionForce4,
                firstCriterionCriFactor, firstCriterionName,

                cellStyleResultsMaxNormal, cellStyleResultsMaxBold,
                cellStyleResultsMaxCriterion,
                secondCriterionBoltId, secondCriterionLCid,
                secondCriterionForce1, secondCriterionForce2, secondCriterionForce3, secondCriterionForce4,
                secondCriterionCriFactor, secondCriterionName,

                cellStyleResultsMaxNormal, cellStyleResultsMaxBold,
                cellStyleResultsMaxCriterion,
                thirdCriterionBoltId, thirdCriterionLCid,
                thirdCriterionForce1, thirdCriterionForce2, thirdCriterionForce3, thirdCriterionForce4,
                thirdCriterionCriFactor, thirdCriterionName,

                cellStyleResultsMaxNormal, cellStyleResultsMaxBold,
                cellStyleResultsMaxCriterion,
                fourthCriterionBoltId, fourthCriterionLCid,
                fourthCriterionForce1, fourthCriterionForce2, fourthCriterionForce3, fourthCriterionForce4,
                fourthCriterionCriFactor, fourthCriterionName
        );

        // 1.2 MAX CRASH -----------------------------------------------------------------------------------------------
        // --------- 1 ------------------ Fax
        firstCriterionName = "MAX";
        firstCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFaxial();
        firstCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFaxial_shear1();
        firstCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFaxial_shear2();
        firstCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFaxial_shearTotal();

        firstCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFaxialLcId();
        firstCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFaxialBoltId();
        firstCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFaxialCriFactor();

        // --------- 2 ------------------ Fshear1
        secondCriterionName = "MAX";
        secondCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshear1_axial();
        secondCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshear1();
        secondCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshear1_shear2();
        secondCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshear1_shearTotal();

        secondCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshear1LcId();
        secondCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshear1BoltId();
        secondCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshear1CriFactor();

        // --------- 3 ------------------ Fshear2
        thirdCriterionName = "MAX";
        thirdCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshear2_axial();
        thirdCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshear2_shear1();
        thirdCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshear2();
        thirdCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshear2_shearTotal();

        thirdCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshear2LcId();
        thirdCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshear2BoltId();
        thirdCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshear2CriFactor();

        // --------- 4 ------------------ FshearTotal
        fourthCriterionName = "MAX";
        fourthCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshearTotal_axial();
        fourthCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshearTotal_shear1();
        fourthCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshearTotal_shear2();
        fourthCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshearTotal();

        fourthCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshearTotalLcId();
        fourthCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshearTotalBoltId();
        fourthCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshearTotalCriFactor();

        firstLCname = "CRASH";
        secondLCname = "CRASH";
        thirdLCname = "CRASH";
        fourthLCname = "CRASH";

        printHeaders = true;
        rowStart = 28;
        createExtraTableSummarySecondCBAR(
                sheet, rowStart, cellFirst,

                printHeaders,

                cellStyleHeaderTopFontCenter,
                cellStyleResultsMaxCriterion, firstLCname,
                cellStyleResultsMaxCriterion, secondLCname,
                cellStyleResultsMaxCriterion, thirdLCname,
                cellStyleResultsMaxCriterion, fourthLCname,

                cellStyleHeaderTopFontCenter,

                force1NameFirst, force2NameFirst, force3NameFirst, force4NameFirst,

                cellStyleResultsMaxNormal, cellStyleResultsMaxBold,
                cellStyleResultsMaxCriterion,
                firstCriterionBoltId, firstCriterionLCid,
                firstCriterionForce1, firstCriterionForce2, firstCriterionForce3, firstCriterionForce4,
                firstCriterionCriFactor, firstCriterionName,

                cellStyleResultsMaxNormal, cellStyleResultsMaxBold,
                cellStyleResultsMaxCriterion,
                secondCriterionBoltId, secondCriterionLCid,
                secondCriterionForce1, secondCriterionForce2, secondCriterionForce3, secondCriterionForce4,
                secondCriterionCriFactor, secondCriterionName,

                cellStyleResultsMaxNormal, cellStyleResultsMaxBold,
                cellStyleResultsMaxCriterion,
                thirdCriterionBoltId, thirdCriterionLCid,
                thirdCriterionForce1, thirdCriterionForce2, thirdCriterionForce3, thirdCriterionForce4,
                thirdCriterionCriFactor, thirdCriterionName,

                cellStyleResultsMaxNormal, cellStyleResultsMaxBold,
                cellStyleResultsMaxCriterion,
                fourthCriterionBoltId, fourthCriterionLCid,
                fourthCriterionForce1, fourthCriterionForce2, fourthCriterionForce3, fourthCriterionForce4,
                fourthCriterionCriFactor, fourthCriterionName
        );

        // 1.3 MAX FATIGUE ---------------------------------------------------------------------------------------------
        // --------- 1 ------------------ Fax
        firstCriterionName = "MAX";
        firstCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFaxial();
        firstCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFaxial_shear1();
        firstCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFaxial_shear2();
        firstCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFaxial_shearTotal();

        firstCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFaxialLcId();
        firstCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFaxialBoltId();
        firstCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFaxialCriFactor();

        // --------- 2 ------------------ Fshear1
        secondCriterionName = "MAX";
        secondCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshear1_axial();
        secondCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshear1();
        secondCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshear1_shear2();
        secondCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshear1_shearTotal();

        secondCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshear1LcId();
        secondCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshear1BoltId();
        secondCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshear1CriFactor();

        // --------- 3 ------------------ Fshear2
        thirdCriterionName = "MAX";
        thirdCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshear2_axial();
        thirdCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshear2_shear1();
        thirdCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshear2();
        thirdCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshear2_shearTotal();

        thirdCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshear2LcId();
        thirdCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshear2BoltId();
        thirdCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshear2CriFactor();

        // --------- 4 ------------------ FshearTotal
        fourthCriterionName = "MAX";
        fourthCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshearTotal_axial();
        fourthCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshearTotal_shear1();
        fourthCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshearTotal_shear2();
        fourthCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshearTotal();

        fourthCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshearTotalLcId();
        fourthCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshearTotalBoltId();
        fourthCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshearTotalCriFactor();

        firstLCname = "FATIGUE";
        secondLCname = "FATIGUE";
        thirdLCname = "FATIGUE";
        fourthLCname = "FATIGUE";

        printHeaders = true;
        rowStart = 33;
        createExtraTableSummarySecondCBAR(
                sheet, rowStart, cellFirst,

                printHeaders,

                cellStyleHeaderTopFontCenter,
                cellStyleResultsMaxCriterion, firstLCname,
                cellStyleResultsMaxCriterion, secondLCname,
                cellStyleResultsMaxCriterion, thirdLCname,
                cellStyleResultsMaxCriterion, fourthLCname,

                cellStyleHeaderTopFontCenter,

                force1NameFirst, force2NameFirst, force3NameFirst, force4NameFirst,

                cellStyleResultsMaxNormal, cellStyleResultsMaxBold,
                cellStyleResultsMaxCriterion,
                firstCriterionBoltId, firstCriterionLCid,
                firstCriterionForce1, firstCriterionForce2, firstCriterionForce3, firstCriterionForce4,
                firstCriterionCriFactor, firstCriterionName,

                cellStyleResultsMaxNormal, cellStyleResultsMaxBold,
                cellStyleResultsMaxCriterion,
                secondCriterionBoltId, secondCriterionLCid,
                secondCriterionForce1, secondCriterionForce2, secondCriterionForce3, secondCriterionForce4,
                secondCriterionCriFactor, secondCriterionName,

                cellStyleResultsMaxNormal, cellStyleResultsMaxBold,
                cellStyleResultsMaxCriterion,
                thirdCriterionBoltId, thirdCriterionLCid,
                thirdCriterionForce1, thirdCriterionForce2, thirdCriterionForce3, thirdCriterionForce4,
                thirdCriterionCriFactor, thirdCriterionName,

                cellStyleResultsMaxNormal, cellStyleResultsMaxBold,
                cellStyleResultsMaxCriterion,
                fourthCriterionBoltId, fourthCriterionLCid,
                fourthCriterionForce1, fourthCriterionForce2, fourthCriterionForce3, fourthCriterionForce4,
                fourthCriterionCriFactor, fourthCriterionName
        );

        // 2.1 MIN STATIC ----------------------------------------------------------------------------------------------
        // --------- 1 ------------------ Fax
        firstCriterionName = "MIN";
        firstCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFaxial();
        firstCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFaxial_shear1();
        firstCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFaxial_shear2();
        firstCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFaxial_shearTotal();

        firstCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFaxialLcId();
        firstCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFaxialBoltId();
        firstCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFaxialCriFactor();

        // --------- 2 ------------------ Fshear1
        secondCriterionName = "MIN";
        secondCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshear1_axial();
        secondCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshear1();
        secondCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshear1_shear2();
        secondCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshear1_shearTotal();

        secondCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshear1LcId();
        secondCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshear1BoltId();
        secondCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshear1CriFactor();

        // --------- 3 ------------------ Fshear2
        thirdCriterionName = "MIN";
        thirdCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshear2_axial();
        thirdCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshear2_shear1();
        thirdCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshear2();
        thirdCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshear2_shearTotal();

        thirdCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshear2LcId();
        thirdCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshear2BoltId();
        thirdCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshear2CriFactor();

        // --------- 4 ------------------ FshearTotal
        fourthCriterionName = "MIN";
        fourthCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshearTotal_axial();
        fourthCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshearTotal_shear1();
        fourthCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshearTotal_shear2();
        fourthCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshearTotal();

        fourthCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshearTotalLcId();
        fourthCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshearTotalBoltId();
        fourthCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshearTotalCriFactor();

        firstLCname = "STATIC";
        secondLCname = "STATIC";
        thirdLCname = "STATIC";
        fourthLCname = "STATIC";

        printHeaders = true;
        rowStart = 40;
        createExtraTableSummarySecondCBAR(
                sheet, rowStart, cellFirst,

                printHeaders,

                cellStyleHeaderTopFontCenter,
                cellStyleResultsMinCriterion, firstLCname,
                cellStyleResultsMinCriterion, secondLCname,
                cellStyleResultsMinCriterion, thirdLCname,
                cellStyleResultsMinCriterion, fourthLCname,

                cellStyleHeaderTopFontCenter,

                force1NameFirst, force2NameFirst, force3NameFirst, force4NameFirst,

                cellStyleResultsMinNormal, cellStyleResultsMinBold,
                cellStyleResultsMinCriterion,
                firstCriterionBoltId, firstCriterionLCid,
                firstCriterionForce1, firstCriterionForce2, firstCriterionForce3, firstCriterionForce4,
                firstCriterionCriFactor, firstCriterionName,

                cellStyleResultsMinNormal, cellStyleResultsMinBold,
                cellStyleResultsMinCriterion,
                secondCriterionBoltId, secondCriterionLCid,
                secondCriterionForce1, secondCriterionForce2, secondCriterionForce3, secondCriterionForce4,
                secondCriterionCriFactor, secondCriterionName,

                cellStyleResultsMinNormal, cellStyleResultsMinBold,
                cellStyleResultsMinCriterion,
                thirdCriterionBoltId, thirdCriterionLCid,
                thirdCriterionForce1, thirdCriterionForce2, thirdCriterionForce3, thirdCriterionForce4,
                thirdCriterionCriFactor, thirdCriterionName,

                cellStyleResultsMinNormal, cellStyleResultsMinBold,
                cellStyleResultsMinCriterion,
                fourthCriterionBoltId, fourthCriterionLCid,
                fourthCriterionForce1, fourthCriterionForce2, fourthCriterionForce3, fourthCriterionForce4,
                fourthCriterionCriFactor, fourthCriterionName
        );

        // 2.2 MIN CRASH -----------------------------------------------------------------------------------------------
        // --------- 1 ------------------ Fax
        firstCriterionName = "MIN";
        firstCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFaxial();
        firstCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFaxial_shear1();
        firstCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFaxial_shear2();
        firstCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFaxial_shearTotal();

        firstCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFaxialLcId();
        firstCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFaxialBoltId();
        firstCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFaxialCriFactor();

        // --------- 2 ------------------ Fshear1
        secondCriterionName = "MIN";
        secondCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshear1_axial();
        secondCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshear1();
        secondCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshear1_shear2();
        secondCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshear1_shearTotal();

        secondCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshear1LcId();
        secondCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshear1BoltId();
        secondCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshear1CriFactor();

        // --------- 3 ------------------ Fshear2
        thirdCriterionName = "MIN";
        thirdCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshear2_axial();
        thirdCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshear2_shear1();
        thirdCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshear2();
        thirdCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshear2_shearTotal();

        thirdCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshear2LcId();
        thirdCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshear2BoltId();
        thirdCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshear2CriFactor();

        // --------- 4 ------------------ FshearTotal
        fourthCriterionName = "MIN";
        fourthCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshearTotal_axial();
        fourthCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshearTotal_shear1();
        fourthCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshearTotal_shear2();
        fourthCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshearTotal();

        fourthCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshearTotalLcId();
        fourthCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshearTotalBoltId();
        fourthCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshearTotalCriFactor();

        firstLCname = "CRASH";
        secondLCname = "CRASH";
        thirdLCname = "CRASH";
        fourthLCname = "CRASH";

        printHeaders = true;
        rowStart = 45;
        createExtraTableSummarySecondCBAR(
                sheet, rowStart, cellFirst,

                printHeaders,

                cellStyleHeaderTopFontCenter,
                cellStyleResultsMinCriterion, firstLCname,
                cellStyleResultsMinCriterion, secondLCname,
                cellStyleResultsMinCriterion, thirdLCname,
                cellStyleResultsMinCriterion, fourthLCname,

                cellStyleHeaderTopFontCenter,

                force1NameFirst, force2NameFirst, force3NameFirst, force4NameFirst,

                cellStyleResultsMinNormal, cellStyleResultsMinBold,
                cellStyleResultsMinCriterion,
                firstCriterionBoltId, firstCriterionLCid,
                firstCriterionForce1, firstCriterionForce2, firstCriterionForce3, firstCriterionForce4,
                firstCriterionCriFactor, firstCriterionName,

                cellStyleResultsMinNormal, cellStyleResultsMinBold,
                cellStyleResultsMinCriterion,
                secondCriterionBoltId, secondCriterionLCid,
                secondCriterionForce1, secondCriterionForce2, secondCriterionForce3, secondCriterionForce4,
                secondCriterionCriFactor, secondCriterionName,

                cellStyleResultsMinNormal, cellStyleResultsMinBold,
                cellStyleResultsMinCriterion,
                thirdCriterionBoltId, thirdCriterionLCid,
                thirdCriterionForce1, thirdCriterionForce2, thirdCriterionForce3, thirdCriterionForce4,
                thirdCriterionCriFactor, thirdCriterionName,

                cellStyleResultsMinNormal, cellStyleResultsMinBold,
                cellStyleResultsMinCriterion,
                fourthCriterionBoltId, fourthCriterionLCid,
                fourthCriterionForce1, fourthCriterionForce2, fourthCriterionForce3, fourthCriterionForce4,
                fourthCriterionCriFactor, fourthCriterionName
        );

        // 2.3 MIN FATIGUE ---------------------------------------------------------------------------------------------
        // --------- 1 ------------------ Fax
        firstCriterionName = "MIN";
        firstCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFaxial();
        firstCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFaxial_shear1();
        firstCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFaxial_shear2();
        firstCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFaxial_shearTotal();

        firstCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFaxialLcId();
        firstCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFaxialBoltId();
        firstCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFaxialCriFactor();

        // --------- 2 ------------------ Fshear1
        secondCriterionName = "MIN";
        secondCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshear1_axial();
        secondCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshear1();
        secondCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshear1_shear2();
        secondCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshear1_shearTotal();

        secondCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshear1LcId();
        secondCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshear1BoltId();
        secondCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshear1CriFactor();

        // --------- 3 ------------------ Fshear2
        thirdCriterionName = "MIN";
        thirdCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshear2_axial();
        thirdCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshear2_shear1();
        thirdCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshear2();
        thirdCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshear2_shearTotal();

        thirdCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshear2LcId();
        thirdCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshear2BoltId();
        thirdCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshear2CriFactor();

        // --------- 4 ------------------ FshearTotal
        fourthCriterionName = "MIN";
        fourthCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshearTotal_axial();
        fourthCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshearTotal_shear1();
        fourthCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshearTotal_shear2();
        fourthCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshearTotal();

        fourthCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshearTotalLcId();
        fourthCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshearTotalBoltId();
        fourthCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshearTotalCriFactor();

        firstLCname = "FATIGUE";
        secondLCname = "FATIGUE";
        thirdLCname = "FATIGUE";
        fourthLCname = "FATIGUE";

        printHeaders = true;
        rowStart = 50;
        createExtraTableSummarySecondCBAR(
                sheet, rowStart, cellFirst,

                printHeaders,

                cellStyleHeaderTopFontCenter,
                cellStyleResultsMinCriterion, firstLCname,
                cellStyleResultsMinCriterion, secondLCname,
                cellStyleResultsMinCriterion, thirdLCname,
                cellStyleResultsMinCriterion, fourthLCname,

                cellStyleHeaderTopFontCenter,

                force1NameFirst, force2NameFirst, force3NameFirst, force4NameFirst,

                cellStyleResultsMinNormal, cellStyleResultsMinBold,
                cellStyleResultsMinCriterion,
                firstCriterionBoltId, firstCriterionLCid,
                firstCriterionForce1, firstCriterionForce2, firstCriterionForce3, firstCriterionForce4,
                firstCriterionCriFactor, firstCriterionName,

                cellStyleResultsMinNormal, cellStyleResultsMinBold,
                cellStyleResultsMinCriterion,
                secondCriterionBoltId, secondCriterionLCid,
                secondCriterionForce1, secondCriterionForce2, secondCriterionForce3, secondCriterionForce4,
                secondCriterionCriFactor, secondCriterionName,

                cellStyleResultsMinNormal, cellStyleResultsMinBold,
                cellStyleResultsMinCriterion,
                thirdCriterionBoltId, thirdCriterionLCid,
                thirdCriterionForce1, thirdCriterionForce2, thirdCriterionForce3, thirdCriterionForce4,
                thirdCriterionCriFactor, thirdCriterionName,

                cellStyleResultsMinNormal, cellStyleResultsMinBold,
                cellStyleResultsMinCriterion,
                fourthCriterionBoltId, fourthCriterionLCid,
                fourthCriterionForce1, fourthCriterionForce2, fourthCriterionForce3, fourthCriterionForce4,
                fourthCriterionCriFactor, fourthCriterionName
        );

        // 3.1 ABS MIN STATIC ----------------------------------------------------------------------------------------------
        // --------- 1 ------------------ Fax
        firstCriterionName = "A MIN";
        firstCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFaxial();
        firstCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFaxial_shear1();
        firstCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFaxial_shear2();
        firstCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFaxial_shearTotal();

        firstCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFaxialLcId();
        firstCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFaxialBoltId();
        firstCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFaxialCriFactor();

        // --------- 2 ------------------ Fshear1
        secondCriterionName = "A MIN";
        secondCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshear1_axial();
        secondCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshear1();
        secondCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshear1_shear2();
        secondCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshear1_shearTotal();

        secondCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshear1LcId();
        secondCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshear1BoltId();
        secondCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshear1CriFactor();

        // --------- 3 ------------------ Fshear2
        thirdCriterionName = "A MIN";
        thirdCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshear2_axial();
        thirdCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshear2_shear1();
        thirdCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshear2();
        thirdCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshear2_shearTotal();

        thirdCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshear2LcId();
        thirdCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshear2BoltId();
        thirdCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshear2CriFactor();

        // --------- 4 ------------------ FshearTotal
        fourthCriterionName = "A MIN";
        fourthCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshearTotal_axial();
        fourthCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshearTotal_shear1();
        fourthCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshearTotal_shear2();
        fourthCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshearTotal();

        fourthCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshearTotalLcId();
        fourthCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshearTotalBoltId();
        fourthCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshearTotalCriFactor();

        firstLCname = "STATIC";
        secondLCname = "STATIC";
        thirdLCname = "STATIC";
        fourthLCname = "STATIC";

        printHeaders = true;
        rowStart = 57;
        createExtraTableSummarySecondCBAR(
                sheet, rowStart, cellFirst,

                printHeaders,

                cellStyleHeaderTopFontCenter,
                cellStyleResultsAbsMinCriterion, firstLCname,
                cellStyleResultsAbsMinCriterion, secondLCname,
                cellStyleResultsAbsMinCriterion, thirdLCname,
                cellStyleResultsAbsMinCriterion, fourthLCname,

                cellStyleHeaderTopFontCenter,

                force1NameFirst, force2NameFirst, force3NameFirst, force4NameFirst,

                cellStyleResultsAbsMinNormal, cellStyleResultsAbsMinBold,
                cellStyleResultsAbsMinCriterion,
                firstCriterionBoltId, firstCriterionLCid,
                firstCriterionForce1, firstCriterionForce2, firstCriterionForce3, firstCriterionForce4,
                firstCriterionCriFactor, firstCriterionName,

                cellStyleResultsAbsMinNormal, cellStyleResultsAbsMinBold,
                cellStyleResultsAbsMinCriterion,
                secondCriterionBoltId, secondCriterionLCid,
                secondCriterionForce1, secondCriterionForce2, secondCriterionForce3, secondCriterionForce4,
                secondCriterionCriFactor, secondCriterionName,

                cellStyleResultsAbsMinNormal, cellStyleResultsAbsMinBold,
                cellStyleResultsAbsMinCriterion,
                thirdCriterionBoltId, thirdCriterionLCid,
                thirdCriterionForce1, thirdCriterionForce2, thirdCriterionForce3, thirdCriterionForce4,
                thirdCriterionCriFactor, thirdCriterionName,

                cellStyleResultsAbsMinNormal, cellStyleResultsAbsMinBold,
                cellStyleResultsAbsMinCriterion,
                fourthCriterionBoltId, fourthCriterionLCid,
                fourthCriterionForce1, fourthCriterionForce2, fourthCriterionForce3, fourthCriterionForce4,
                fourthCriterionCriFactor, fourthCriterionName
        );

        // 3.2 ABS MIN CRASH -----------------------------------------------------------------------------------------------
        // --------- 1 ------------------ Fax
        firstCriterionName = "A MIN";
        firstCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFaxial();
        firstCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFaxial_shear1();
        firstCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFaxial_shear2();
        firstCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFaxial_shearTotal();

        firstCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFaxialLcId();
        firstCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFaxialBoltId();
        firstCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFaxialCriFactor();

        // --------- 2 ------------------ Fshear1
        secondCriterionName = "A MIN";
        secondCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshear1_axial();
        secondCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshear1();
        secondCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshear1_shear2();
        secondCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshear1_shearTotal();

        secondCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshear1LcId();
        secondCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshear1BoltId();
        secondCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshear1CriFactor();

        // --------- 3 ------------------ Fshear2
        thirdCriterionName = "A MIN";
        thirdCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshear2_axial();
        thirdCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshear2_shear1();
        thirdCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshear2();
        thirdCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshear2_shearTotal();

        thirdCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshear2LcId();
        thirdCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshear2BoltId();
        thirdCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshear2CriFactor();

        // --------- 4 ------------------ FshearTotal
        fourthCriterionName = "A MIN";
        fourthCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshearTotal_axial();
        fourthCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshearTotal_shear1();
        fourthCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshearTotal_shear2();
        fourthCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshearTotal();

        fourthCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshearTotalLcId();
        fourthCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshearTotalBoltId();
        fourthCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshearTotalCriFactor();

        firstLCname = "CRASH";
        secondLCname = "CRASH";
        thirdLCname = "CRASH";
        fourthLCname = "CRASH";

        printHeaders = true;
        rowStart = 62;
        createExtraTableSummarySecondCBAR(
                sheet, rowStart, cellFirst,

                printHeaders,

                cellStyleHeaderTopFontCenter,
                cellStyleResultsAbsMinCriterion, firstLCname,
                cellStyleResultsAbsMinCriterion, secondLCname,
                cellStyleResultsAbsMinCriterion, thirdLCname,
                cellStyleResultsAbsMinCriterion, fourthLCname,

                cellStyleHeaderTopFontCenter,

                force1NameFirst, force2NameFirst, force3NameFirst, force4NameFirst,

                cellStyleResultsAbsMinNormal, cellStyleResultsAbsMinBold,
                cellStyleResultsAbsMinCriterion,
                firstCriterionBoltId, firstCriterionLCid,
                firstCriterionForce1, firstCriterionForce2, firstCriterionForce3, firstCriterionForce4,
                firstCriterionCriFactor, firstCriterionName,

                cellStyleResultsAbsMinNormal, cellStyleResultsAbsMinBold,
                cellStyleResultsAbsMinCriterion,
                secondCriterionBoltId, secondCriterionLCid,
                secondCriterionForce1, secondCriterionForce2, secondCriterionForce3, secondCriterionForce4,
                secondCriterionCriFactor, secondCriterionName,

                cellStyleResultsAbsMinNormal, cellStyleResultsAbsMinBold,
                cellStyleResultsAbsMinCriterion,
                thirdCriterionBoltId, thirdCriterionLCid,
                thirdCriterionForce1, thirdCriterionForce2, thirdCriterionForce3, thirdCriterionForce4,
                thirdCriterionCriFactor, thirdCriterionName,

                cellStyleResultsAbsMinNormal, cellStyleResultsAbsMinBold,
                cellStyleResultsAbsMinCriterion,
                fourthCriterionBoltId, fourthCriterionLCid,
                fourthCriterionForce1, fourthCriterionForce2, fourthCriterionForce3, fourthCriterionForce4,
                fourthCriterionCriFactor, fourthCriterionName
        );

        // 3.3 ABS MIN FATIGUE ---------------------------------------------------------------------------------------------
        // --------- 1 ------------------ Fax
        firstCriterionName = "A MIN";
        firstCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFaxial();
        firstCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFaxial_shear1();
        firstCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFaxial_shear2();
        firstCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFaxial_shearTotal();

        firstCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFaxialLcId();
        firstCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFaxialBoltId();
        firstCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFaxialCriFactor();

        // --------- 2 ------------------ Fshear1
        secondCriterionName = "A MIN";
        secondCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshear1_axial();
        secondCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshear1();
        secondCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshear1_shear2();
        secondCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshear1_shearTotal();

        secondCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshear1LcId();
        secondCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshear1BoltId();
        secondCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshear1CriFactor();

        // --------- 3 ------------------ Fshear2
        thirdCriterionName = "A MIN";
        thirdCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshear2_axial();
        thirdCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshear2_shear1();
        thirdCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshear2();
        thirdCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshear2_shearTotal();

        thirdCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshear2LcId();
        thirdCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshear2BoltId();
        thirdCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshear2CriFactor();

        // --------- 4 ------------------ FshearTotal
        fourthCriterionName = "A MIN";
        fourthCriterionForce1 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshearTotal_axial();
        fourthCriterionForce2 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshearTotal_shear1();
        fourthCriterionForce3 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshearTotal_shear2();
        fourthCriterionForce4 = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshearTotal();

        fourthCriterionLCid = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshearTotalLcId();
        fourthCriterionBoltId = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshearTotalBoltId();
        fourthCriterionCriFactor = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshearTotalCriFactor();

        firstLCname = "FATIGUE";
        secondLCname = "FATIGUE";
        thirdLCname = "FATIGUE";
        fourthLCname = "FATIGUE";

        printHeaders = true;
        rowStart = 67;
        createExtraTableSummarySecondCBAR(
                sheet, rowStart, cellFirst,

                printHeaders,

                cellStyleHeaderTopFontCenter,
                cellStyleResultsAbsMinCriterion, firstLCname,
                cellStyleResultsAbsMinCriterion, secondLCname,
                cellStyleResultsAbsMinCriterion, thirdLCname,
                cellStyleResultsAbsMinCriterion, fourthLCname,

                cellStyleHeaderTopFontCenter,

                force1NameFirst, force2NameFirst, force3NameFirst, force4NameFirst,

                cellStyleResultsAbsMinNormal, cellStyleResultsAbsMinBold,
                cellStyleResultsAbsMinCriterion,
                firstCriterionBoltId, firstCriterionLCid,
                firstCriterionForce1, firstCriterionForce2, firstCriterionForce3, firstCriterionForce4,
                firstCriterionCriFactor, firstCriterionName,

                cellStyleResultsAbsMinNormal, cellStyleResultsAbsMinBold,
                cellStyleResultsAbsMinCriterion,
                secondCriterionBoltId, secondCriterionLCid,
                secondCriterionForce1, secondCriterionForce2, secondCriterionForce3, secondCriterionForce4,
                secondCriterionCriFactor, secondCriterionName,

                cellStyleResultsAbsMinNormal, cellStyleResultsAbsMinBold,
                cellStyleResultsAbsMinCriterion,
                thirdCriterionBoltId, thirdCriterionLCid,
                thirdCriterionForce1, thirdCriterionForce2, thirdCriterionForce3, thirdCriterionForce4,
                thirdCriterionCriFactor, thirdCriterionName,

                cellStyleResultsAbsMinNormal, cellStyleResultsAbsMinBold,
                cellStyleResultsAbsMinCriterion,
                fourthCriterionBoltId, fourthCriterionLCid,
                fourthCriterionForce1, fourthCriterionForce2, fourthCriterionForce3, fourthCriterionForce4,
                fourthCriterionCriFactor, fourthCriterionName
        );


        //--------------------------------------------------------------------------------------------------------------
        rowStart += 12;
        return rowStart;
    }

    private int generateExtraSummariesCriMaxMinAbsCBAR___old(String partName, int currentOffset, CsvParserResultExtractor csvParser) {
        int cellFirst = columnCurrentStartId + (COLUMN_OFFSET * currentOffset);
        int cellLast = cellFirst + COLUMN_OFFSET - 1; // currently not in use, delete later
        int currentRow = 10;

        // generete values
        ((CbarResults) csvParser.getResults().get(partName))
                .setMaxLoadCaseBolt(LoadCaseType.STATIC, bolTabSetup.getLcIdsForExtraTableStaticLC());
        ((CbarResults) csvParser.getResults().get(partName))
                .setMaxLoadCaseBolt(LoadCaseType.FATIGUE, bolTabSetup.getLcIdsForExtraTableFatigueLC());
        ((CbarResults) csvParser.getResults().get(partName))
                .setMaxLoadCaseBolt(LoadCaseType.CRASH, bolTabSetup.getLcIdsForExtraTableCrashLC());

        //--------------------------------------------------------------------------------------------------------------
        // max STATIC
        Boolean printHeaders = true;
        String maxLCGroup = "LC group";
        String maxLCName = "STATIC";

        String maxStaticForce1Name = "Fax [N]";
        String maxStaticForce2Name = "Fsh1 [N]";
        String maxStaticForce3Name = "Fsh2 [N]";
        String maxStaticForce4Name = "Fsht [N]";

        Double maxStaticValue1Force = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFaxial();
        Double maxStaticValue2Force = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshear1();
        Double maxStaticValue3Force = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshear2();
        Double maxStaticValue4Force = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshearTotal();

        Integer maxStaticValue1Lc = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFaxialLcId();
        Integer maxStaticValue2Lc = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshear1LcId();
        Integer maxStaticValue3Lc = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshear2LcId();
        Integer maxStaticValue4Lc = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshearTotalLcId();

        Integer maxStaticValue1Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFaxialBoltId();
        Integer maxStaticValue2Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshear1BoltId();
        Integer maxStaticValue3Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshear2BoltId();
        Integer maxStaticValue4Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshearTotalBoltId();

        Double maxStaticValue1CriFact = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFaxialCriFactor();
        Double maxStaticValue2CriFact = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshear1CriFactor();
        Double maxStaticValue3CriFact = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshear2CriFactor();
        Double maxStaticValue4CriFact = ((CbarResults) csvParser.getResults().get(partName)).getMaxStaticFshearTotalCriFactor();

        Integer maxStaticValue1Row = null;
        Integer maxStaticValue2Row = null;
        Integer maxStaticValue3Row = null;
        Integer maxStaticValue4Row = null;

        currentRow = 12;
        createTableSummaryForLoadCases(printHeaders,
                maxLCGroup, maxLCName, cellStyleHeaderLeftMAXLoadcase,
                "MAX", currentRow,
                maxStaticForce1Name, maxStaticForce2Name, maxStaticForce3Name, maxStaticForce4Name,
                cellFirst, sheet, cellStyleHeaderTopFontCenter, cellStyleHeaderLeftMAX, cellStyleResults0,
                maxStaticValue1Force, maxStaticValue2Force, maxStaticValue3Force, maxStaticValue4Force,
                maxStaticValue1Lc, maxStaticValue2Lc, maxStaticValue3Lc, maxStaticValue4Lc,
                maxStaticValue1Bolt, maxStaticValue2Bolt, maxStaticValue3Bolt, maxStaticValue4Bolt,
                maxStaticValue1CriFact, maxStaticValue2CriFact, maxStaticValue3CriFact, maxStaticValue4CriFact,
                maxStaticValue1Row, maxStaticValue2Row, maxStaticValue3Row, maxStaticValue4Row
        );

        // max CRASH
        printHeaders = false;
        maxLCGroup = "LC group";
        maxLCName = "CRASH";
        //CellStyle cellStyleHeaderLoadCaseName;

        String maxCrashForce1Name = "Fax [N]";
        String maxCrashForce2Name = "Fsh1 [N]";
        String maxCrashForce3Name = "Fsh2 [N]";
        String maxCrashForce4Name = "Fsht [N]";

        Double maxCrashValue1Force = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFaxial();
        Double maxCrashValue2Force = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshear1();
        Double maxCrashValue3Force = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshear2();
        Double maxCrashValue4Force = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshearTotal();

        Integer maxCrashValue1Lc = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFaxialLcId();
        Integer maxCrashValue2Lc = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshear1LcId();
        Integer maxCrashValue3Lc = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshear2LcId();
        Integer maxCrashValue4Lc = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshearTotalLcId();

        Integer maxCrashValue1Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFaxialBoltId();
        Integer maxCrashValue2Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshear1BoltId();
        Integer maxCrashValue3Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshear2BoltId();
        Integer maxCrashValue4Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshearTotalBoltId();

        Double maxCrashValue1CriFact = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFaxialCriFactor();
        Double maxCrashValue2CriFact = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshear1CriFactor();
        Double maxCrashValue3CriFact = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshear2CriFactor();
        Double maxCrashValue4CriFact = ((CbarResults) csvParser.getResults().get(partName)).getMaxCrashFshearTotalCriFactor();

        Integer maxCrashValue1Row = null;
        Integer maxCrashValue2Row = null;
        Integer maxCrashValue3Row = null;
        Integer maxCrashValue4Row = null;

        currentRow = 17;
        createTableSummaryForLoadCases(printHeaders,
                maxLCGroup, maxLCName, cellStyleHeaderLeftMAXLoadcase,
                "MAX", currentRow,
                maxCrashForce1Name, maxCrashForce2Name, maxCrashForce3Name, maxCrashForce4Name,
                cellFirst, sheet, cellStyleHeaderTopFontCenter, cellStyleHeaderLeftMAX, cellStyleResults0,
                maxCrashValue1Force, maxCrashValue2Force, maxCrashValue3Force, maxCrashValue4Force,
                maxCrashValue1Lc, maxCrashValue2Lc, maxCrashValue3Lc, maxCrashValue4Lc,
                maxCrashValue1Bolt, maxCrashValue2Bolt, maxCrashValue3Bolt, maxCrashValue4Bolt,
                maxCrashValue1CriFact, maxCrashValue2CriFact, maxCrashValue3CriFact, maxCrashValue4CriFact,
                maxCrashValue1Row, maxCrashValue2Row, maxCrashValue3Row, maxCrashValue4Row
        );
        // max FATIGUE
        printHeaders = false;
        maxLCGroup = "LC group";
        maxLCName = "FATIGUE";
        //CellStyle cellStyleHeaderLoadCaseName;

        String maxFatigueForce1Name = "Fax [N]";
        String maxFatigueForce2Name = "Fsh1 [N]";
        String maxFatigueForce3Name = "Fsh2 [N]";
        String maxFatigueForce4Name = "Fsht [N]";

        Double maxFatigueValue1Force = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFaxial();
        Double maxFatigueValue2Force = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshear1();
        Double maxFatigueValue3Force = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshear2();
        Double maxFatigueValue4Force = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshearTotal();

        Integer maxFatigueValue1Lc = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFaxialLcId();
        Integer maxFatigueValue2Lc = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshear1LcId();
        Integer maxFatigueValue3Lc = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshear2LcId();
        Integer maxFatigueValue4Lc = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshearTotalLcId();

        Integer maxFatigueValue1Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFaxialBoltId();
        Integer maxFatigueValue2Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshear1BoltId();
        Integer maxFatigueValue3Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshear2BoltId();
        Integer maxFatigueValue4Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshearTotalBoltId();

        Double maxFatigueValue1CriFact = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFaxialCriFactor();
        Double maxFatigueValue2CriFact = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshear1CriFactor();
        Double maxFatigueValue3CriFact = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshear2CriFactor();
        Double maxFatigueValue4CriFact = ((CbarResults) csvParser.getResults().get(partName)).getMaxFatigueFshearTotalCriFactor();

        Integer maxFatigueValue1Row = null;
        Integer maxFatigueValue2Row = null;
        Integer maxFatigueValue3Row = null;
        Integer maxFatigueValue4Row = null;

        currentRow = 22;
        createTableSummaryForLoadCases(printHeaders,
                maxLCGroup, maxLCName, cellStyleHeaderLeftMAXLoadcase,
                "MAX", currentRow,
                maxFatigueForce1Name, maxFatigueForce2Name, maxFatigueForce3Name, maxFatigueForce4Name,
                cellFirst, sheet, cellStyleHeaderTopFontCenter, cellStyleHeaderLeftMAX, cellStyleResults0,
                maxFatigueValue1Force, maxFatigueValue2Force, maxFatigueValue3Force, maxFatigueValue4Force,
                maxFatigueValue1Lc, maxFatigueValue2Lc, maxFatigueValue3Lc, maxFatigueValue4Lc,
                maxFatigueValue1Bolt, maxFatigueValue2Bolt, maxFatigueValue3Bolt, maxFatigueValue4Bolt,
                maxFatigueValue1CriFact, maxFatigueValue2CriFact, maxFatigueValue3CriFact, maxFatigueValue4CriFact,
                maxFatigueValue1Row, maxFatigueValue2Row, maxFatigueValue3Row, maxFatigueValue4Row
        );

        // -------------------------------------------------------------------------------------------------------------

        // min STATIC
        printHeaders = true;
        String minLCGroup = "LC group";
        String minLCName = "STATIC";

        String minStaticForce1Name = "Fax [N]";
        String minStaticForce2Name = "Fsh1 [N]";
        String minStaticForce3Name = "Fsh2 [N]";
        String minStaticForce4Name = "Fsht [N]";

        Double minStaticValue1Force = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFaxial();
        Double minStaticValue2Force = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshear1();
        Double minStaticValue3Force = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshear2();
        Double minStaticValue4Force = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshearTotal();

        Integer minStaticValue1Lc = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFaxialLcId();
        Integer minStaticValue2Lc = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshear1LcId();
        Integer minStaticValue3Lc = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshear2LcId();
        Integer minStaticValue4Lc = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshearTotalLcId();

        Integer minStaticValue1Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFaxialBoltId();
        Integer minStaticValue2Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshear1BoltId();
        Integer minStaticValue3Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshear2BoltId();
        Integer minStaticValue4Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshearTotalBoltId();

        Double minStaticValue1CriFact = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFaxialCriFactor();
        Double minStaticValue2CriFact = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshear1CriFactor();
        Double minStaticValue3CriFact = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshear2CriFactor();
        Double minStaticValue4CriFact = ((CbarResults) csvParser.getResults().get(partName)).getMinStaticFshearTotalCriFactor();

        Integer minStaticValue1Row = null;
        Integer minStaticValue2Row = null;
        Integer minStaticValue3Row = null;
        Integer minStaticValue4Row = null;

        currentRow = 29;
        createTableSummaryForLoadCases(printHeaders,
                minLCGroup, minLCName, cellStyleHeaderLeftMINLoadcase,
                "MIN", currentRow,
                minStaticForce1Name, minStaticForce2Name, minStaticForce3Name, minStaticForce4Name,
                cellFirst, sheet, cellStyleHeaderTopFontCenter, cellStyleHeaderLeftMIN, cellStyleResults0,
                minStaticValue1Force, minStaticValue2Force, minStaticValue3Force, minStaticValue4Force,
                minStaticValue1Lc, minStaticValue2Lc, minStaticValue3Lc, minStaticValue4Lc,
                minStaticValue1Bolt, minStaticValue2Bolt, minStaticValue3Bolt, minStaticValue4Bolt,
                minStaticValue1CriFact, minStaticValue2CriFact, minStaticValue3CriFact, minStaticValue4CriFact,
                minStaticValue1Row, minStaticValue2Row, minStaticValue3Row, minStaticValue4Row
        );

        // min CRASH
        printHeaders = false;
        minLCGroup = "LC group";
        minLCName = "CRASH";
        //CellStyle cellStyleHeaderLoadCaseName;

        String minCrashForce1Name = "Fax [N]";
        String minCrashForce2Name = "Fsh1 [N]";
        String minCrashForce3Name = "Fsh2 [N]";
        String minCrashForce4Name = "Fsht [N]";

        Double minCrashValue1Force = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFaxial();
        Double minCrashValue2Force = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshear1();
        Double minCrashValue3Force = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshear2();
        Double minCrashValue4Force = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshearTotal();

        Integer minCrashValue1Lc = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFaxialLcId();
        Integer minCrashValue2Lc = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshear1LcId();
        Integer minCrashValue3Lc = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshear2LcId();
        Integer minCrashValue4Lc = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshearTotalLcId();

        Integer minCrashValue1Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFaxialBoltId();
        Integer minCrashValue2Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshear1BoltId();
        Integer minCrashValue3Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshear2BoltId();
        Integer minCrashValue4Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshearTotalBoltId();

        Double minCrashValue1CriFact = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFaxialCriFactor();
        Double minCrashValue2CriFact = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshear1CriFactor();
        Double minCrashValue3CriFact = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshear2CriFactor();
        Double minCrashValue4CriFact = ((CbarResults) csvParser.getResults().get(partName)).getMinCrashFshearTotalCriFactor();

        Integer minCrashValue1Row = null;
        Integer minCrashValue2Row = null;
        Integer minCrashValue3Row = null;
        Integer minCrashValue4Row = null;

        currentRow = 34;
        createTableSummaryForLoadCases(printHeaders,
                minLCGroup, minLCName, cellStyleHeaderLeftMINLoadcase,
                "MIN", currentRow,
                minCrashForce1Name, minCrashForce2Name, minCrashForce3Name, minCrashForce4Name,
                cellFirst, sheet, cellStyleHeaderTopFontCenter, cellStyleHeaderLeftMIN, cellStyleResults0,
                minCrashValue1Force, minCrashValue2Force, minCrashValue3Force, minCrashValue4Force,
                minCrashValue1Lc, minCrashValue2Lc, minCrashValue3Lc, minCrashValue4Lc,
                minCrashValue1Bolt, minCrashValue2Bolt, minCrashValue3Bolt, minCrashValue4Bolt,
                minCrashValue1CriFact, minCrashValue2CriFact, minCrashValue3CriFact, minCrashValue4CriFact,
                minCrashValue1Row, minCrashValue2Row, minCrashValue3Row, minCrashValue4Row
        );
        // min FATIGUE
        printHeaders = false;
        minLCGroup = "LC group";
        minLCName = "FATIGUE";
        //CellStyle cellStyleHeaderLoadCaseName;

        String minFatigueForce1Name = "Fax [N]";
        String minFatigueForce2Name = "Fsh1 [N]";
        String minFatigueForce3Name = "Fsh2 [N]";
        String minFatigueForce4Name = "Fsht [N]";

        Double minFatigueValue1Force = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFaxial();
        Double minFatigueValue2Force = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshear1();
        Double minFatigueValue3Force = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshear2();
        Double minFatigueValue4Force = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshearTotal();

        Integer minFatigueValue1Lc = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFaxialLcId();
        Integer minFatigueValue2Lc = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshear1LcId();
        Integer minFatigueValue3Lc = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshear2LcId();
        Integer minFatigueValue4Lc = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshearTotalLcId();

        Integer minFatigueValue1Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFaxialBoltId();
        Integer minFatigueValue2Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshear1BoltId();
        Integer minFatigueValue3Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshear2BoltId();
        Integer minFatigueValue4Bolt = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshearTotalBoltId();

        Double minFatigueValue1CriFact = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFaxialCriFactor();
        Double minFatigueValue2CriFact = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshear1CriFactor();
        Double minFatigueValue3CriFact = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshear2CriFactor();
        Double minFatigueValue4CriFact = ((CbarResults) csvParser.getResults().get(partName)).getMinFatigueFshearTotalCriFactor();

        Integer minFatigueValue1Row = null;
        Integer minFatigueValue2Row = null;
        Integer minFatigueValue3Row = null;
        Integer minFatigueValue4Row = null;

        currentRow = 39;
        createTableSummaryForLoadCases(printHeaders,
                minLCGroup, minLCName, cellStyleHeaderLeftMINLoadcase,
                "MIN", currentRow,
                minFatigueForce1Name, minFatigueForce2Name, minFatigueForce3Name, minFatigueForce4Name,
                cellFirst, sheet, cellStyleHeaderTopFontCenter, cellStyleHeaderLeftMIN, cellStyleResults0,
                minFatigueValue1Force, minFatigueValue2Force, minFatigueValue3Force, minFatigueValue4Force,
                minFatigueValue1Lc, minFatigueValue2Lc, minFatigueValue3Lc, minFatigueValue4Lc,
                minFatigueValue1Bolt, minFatigueValue2Bolt, minFatigueValue3Bolt, minFatigueValue4Bolt,
                minFatigueValue1CriFact, minFatigueValue2CriFact, minFatigueValue3CriFact, minFatigueValue4CriFact,
                minFatigueValue1Row, minFatigueValue2Row, minFatigueValue3Row, minFatigueValue4Row
        );

        // -------------------------------------------------------------------------------------------------------------

        // min ABS MIN
        printHeaders = true;
        String absMinLCGroup = "LC group";
        String absMinLCName = "STATIC";

        String absMinStaticForce1Name = "Fax [N]";
        String absMinStaticForce2Name = "Fsh1 [N]";
        String absMinStaticForce3Name = "Fsh2 [N]";
        String absMinStaticForce4Name = "Fsht [N]";

        Double absMinStaticValue1Force = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFaxial();
        Double absMinStaticValue2Force = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshear1();
        Double absMinStaticValue3Force = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshear2();
        Double absMinStaticValue4Force = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshearTotal();

        Integer absMinStaticValue1Lc = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFaxialLcId();
        Integer absMinStaticValue2Lc = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshear1LcId();
        Integer absMinStaticValue3Lc = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshear2LcId();
        Integer absMinStaticValue4Lc = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshearTotalLcId();

        Integer absMinStaticValue1Bolt = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFaxialBoltId();
        Integer absMinStaticValue2Bolt = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshear1BoltId();
        Integer absMinStaticValue3Bolt = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshear2BoltId();
        Integer absMinStaticValue4Bolt = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshearTotalBoltId();

        Double absMinStaticValue1CriFact = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFaxialCriFactor();
        Double absMinStaticValue2CriFact = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshear1CriFactor();
        Double absMinStaticValue3CriFact = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshear2CriFactor();
        Double absMinStaticValue4CriFact = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinStaticFshearTotalCriFactor();

        Integer absMinStaticValue1Row = null;
        Integer absMinStaticValue2Row = null;
        Integer absMinStaticValue3Row = null;
        Integer absMinStaticValue4Row = null;

        currentRow = 46;
        createTableSummaryForLoadCases(printHeaders,
                absMinLCGroup, absMinLCName, cellStyleHeaderLeftAbsMINLoadcase,
                "F>0 MIN", currentRow,
                absMinStaticForce1Name, absMinStaticForce2Name, absMinStaticForce3Name, absMinStaticForce4Name,
                cellFirst, sheet, cellStyleHeaderTopFontCenter, cellStyleHeaderLeftAbsMIN, cellStyleResults0,
                absMinStaticValue1Force, absMinStaticValue2Force, absMinStaticValue3Force, absMinStaticValue4Force,
                absMinStaticValue1Lc, absMinStaticValue2Lc, absMinStaticValue3Lc, absMinStaticValue4Lc,
                absMinStaticValue1Bolt, absMinStaticValue2Bolt, absMinStaticValue3Bolt, absMinStaticValue4Bolt,
                absMinStaticValue1CriFact, absMinStaticValue2CriFact, absMinStaticValue3CriFact, absMinStaticValue4CriFact,
                absMinStaticValue1Row, absMinStaticValue2Row, absMinStaticValue3Row, absMinStaticValue4Row
        );

        // absMin CRASH
        printHeaders = false;
        absMinLCGroup = "LC group";
        absMinLCName = "CRASH";
        //CellStyle cellStyleHeaderLoadCaseName;

        String absMinCrashForce1Name = "Fax [N]";
        String absMinCrashForce2Name = "Fsh1 [N]";
        String absMinCrashForce3Name = "Fsh2 [N]";
        String absMinCrashForce4Name = "Fsht [N]";

        Double absMinCrashValue1Force = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFaxial();
        Double absMinCrashValue2Force = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshear1();
        Double absMinCrashValue3Force = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshear2();
        Double absMinCrashValue4Force = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshearTotal();

        Integer absMinCrashValue1Lc = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFaxialLcId();
        Integer absMinCrashValue2Lc = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshear1LcId();
        Integer absMinCrashValue3Lc = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshear2LcId();
        Integer absMinCrashValue4Lc = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshearTotalLcId();

        Integer absMinCrashValue1Bolt = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFaxialBoltId();
        Integer absMinCrashValue2Bolt = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshear1BoltId();
        Integer absMinCrashValue3Bolt = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshear2BoltId();
        Integer absMinCrashValue4Bolt = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshearTotalBoltId();

        Double absMinCrashValue1CriFact = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFaxialCriFactor();
        Double absMinCrashValue2CriFact = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshear1CriFactor();
        Double absMinCrashValue3CriFact = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshear2CriFactor();
        Double absMinCrashValue4CriFact = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinCrashFshearTotalCriFactor();

        Integer absMinCrashValue1Row = null;
        Integer absMinCrashValue2Row = null;
        Integer absMinCrashValue3Row = null;
        Integer absMinCrashValue4Row = null;

        currentRow = 51;
        createTableSummaryForLoadCases(printHeaders,
                absMinLCGroup, absMinLCName, cellStyleHeaderLeftAbsMINLoadcase,
                "F>0 MIN", currentRow,
                absMinCrashForce1Name, absMinCrashForce2Name, absMinCrashForce3Name, absMinCrashForce4Name,
                cellFirst, sheet, cellStyleHeaderTopFontCenter, cellStyleHeaderLeftAbsMIN, cellStyleResults0,
                absMinCrashValue1Force, absMinCrashValue2Force, absMinCrashValue3Force, absMinCrashValue4Force,
                absMinCrashValue1Lc, absMinCrashValue2Lc, absMinCrashValue3Lc, absMinCrashValue4Lc,
                absMinCrashValue1Bolt, absMinCrashValue2Bolt, absMinCrashValue3Bolt, absMinCrashValue4Bolt,
                absMinCrashValue1CriFact, absMinCrashValue2CriFact, absMinCrashValue3CriFact, absMinCrashValue4CriFact,
                absMinCrashValue1Row, absMinCrashValue2Row, absMinCrashValue3Row, absMinCrashValue4Row
        );
        // absMin FATIGUE
        printHeaders = false;
        absMinLCGroup = "LC group";
        absMinLCName = "FATIGUE";
        //CellStyle cellStyleHeaderLoadCaseName;

        String absMinFatigueForce1Name = "Fax [N]";
        String absMinFatigueForce2Name = "Fsh1 [N]";
        String absMinFatigueForce3Name = "Fsh2 [N]";
        String absMinFatigueForce4Name = "Fsht [N]";

        Double absMinFatigueValue1Force = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFaxial();
        Double absMinFatigueValue2Force = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshear1();
        Double absMinFatigueValue3Force = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshear2();
        Double absMinFatigueValue4Force = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshearTotal();

        Integer absMinFatigueValue1Lc = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFaxialLcId();
        Integer absMinFatigueValue2Lc = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshear1LcId();
        Integer absMinFatigueValue3Lc = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshear2LcId();
        Integer absMinFatigueValue4Lc = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshearTotalLcId();

        Integer absMinFatigueValue1Bolt = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFaxialBoltId();
        Integer absMinFatigueValue2Bolt = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshear1BoltId();
        Integer absMinFatigueValue3Bolt = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshear2BoltId();
        Integer absMinFatigueValue4Bolt = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshearTotalBoltId();

        Double absMinFatigueValue1CriFact = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFaxialCriFactor();
        Double absMinFatigueValue2CriFact = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshear1CriFactor();
        Double absMinFatigueValue3CriFact = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshear2CriFactor();
        Double absMinFatigueValue4CriFact = ((CbarResults) csvParser.getResults().get(partName)).getAbsMinFatigueFshearTotalCriFactor();

        Integer absMinFatigueValue1Row = null;
        Integer absMinFatigueValue2Row = null;
        Integer absMinFatigueValue3Row = null;
        Integer absMinFatigueValue4Row = null;

        currentRow = 56;
        createTableSummaryForLoadCases(printHeaders,
                absMinLCGroup, absMinLCName, cellStyleHeaderLeftAbsMINLoadcase,
                "F>0 MIN", currentRow,
                absMinFatigueForce1Name, absMinFatigueForce2Name, absMinFatigueForce3Name, absMinFatigueForce4Name,
                cellFirst, sheet, cellStyleHeaderTopFontCenter, cellStyleHeaderLeftAbsMIN, cellStyleResults0,
                absMinFatigueValue1Force, absMinFatigueValue2Force, absMinFatigueValue3Force, absMinFatigueValue4Force,
                absMinFatigueValue1Lc, absMinFatigueValue2Lc, absMinFatigueValue3Lc, absMinFatigueValue4Lc,
                absMinFatigueValue1Bolt, absMinFatigueValue2Bolt, absMinFatigueValue3Bolt, absMinFatigueValue4Bolt,
                absMinFatigueValue1CriFact, absMinFatigueValue2CriFact, absMinFatigueValue3CriFact, absMinFatigueValue4CriFact,
                absMinFatigueValue1Row, absMinFatigueValue2Row, absMinFatigueValue3Row, absMinFatigueValue4Row
        );

        currentRow += 8;
        return currentRow;
    }

    private int createTableSummaryForLoadCases(
            Boolean printHeaders,
            String LCGroup, String LCName, CellStyle cellStyleHeaderLoadCaseName,
            String SummaryTitle, int rowStart,
            String Force1Name, String Force2Name, String Force3Name, String Force4Name,
            int cellFirst, Sheet sheet, CellStyle cellStyleHeaderTopFontCenter,
            CellStyle cellStyleHeaderLeftCRI, CellStyle cellStyleResults0,
            Double value1Force, Double value2Force, Double value3Force, Double value4Force,
            Integer value1Lc, Integer value2Lc, Integer value3Lc, Integer value4Lc,
            Integer value1Bolt, Integer value2Bolt, Integer value3Bolt, Integer value4Bolt,
            Double value1CriFact, Double value2CriFact, Double value3CriFact, Double value4CriFact,
            Integer value1Row, Integer value2Row, Integer value3Row, Integer value4Row) {
        Row row;
        Cell cell;

        if (printHeaders) {
            // --- row rowStart: SummaryTitle table - headers
            if (sheet.getRow(rowStart) == null) {
                row = sheet.createRow(rowStart);
            } else {
                row = sheet.getRow(rowStart);
            }

            cell = row.createCell(cellFirst + 1);
            cell.setCellValue(LCGroup);
            cell.setCellStyle(cellStyleHeaderTopFontCenter);

            cell = row.createCell(cellFirst + 2);
            cell.setCellValue(SummaryTitle);
            cell.setCellStyle(cellStyleHeaderTopFontCenter);

            cell = row.createCell(cellFirst + 3);
            cell.setCellValue(Force1Name);
            cell.setCellStyle(cellStyleHeaderTopFontCenter);

            cell = row.createCell(cellFirst + 4);
            cell.setCellValue(Force2Name);
            cell.setCellStyle(cellStyleHeaderTopFontCenter);

            cell = row.createCell(cellFirst + 5);
            cell.setCellValue(Force3Name);
            cell.setCellStyle(cellStyleHeaderTopFontCenter);

            cell = row.createCell(cellFirst + 6);
            cell.setCellValue(Force4Name);
            cell.setCellStyle(cellStyleHeaderTopFontCenter);
        }

        // --- rowStart + 1: Value
        if (sheet.getRow(rowStart + 1) == null) {
            row = sheet.createRow(rowStart + 1);
        } else {
            row = sheet.getRow(rowStart + 1);
        }
        cell = row.createCell(cellFirst + 1);
        cell.setCellValue(LCName);
        cell.setCellStyle(cellStyleHeaderLoadCaseName);

        cell = row.createCell(cellFirst + 2);
        cell.setCellValue("Value");
        cell.setCellStyle(cellStyleHeaderLeftCRI);

        cell = row.createCell(cellFirst + 3);
        if (value1Force != null) cell.setCellValue(value1Force);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 4);
        if (value2Force != null) cell.setCellValue(value2Force);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 5);
        if (value3Force != null) cell.setCellValue(value3Force);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 6);
        if (value4Force != null) cell.setCellValue(value4Force);
        cell.setCellStyle(cellStyleResults0);

        // --- rowStart + 2: LC id
        if (sheet.getRow(rowStart + 2) == null) {
            row = sheet.createRow(rowStart + 2);
        } else {
            row = sheet.getRow(rowStart + 2);
        }

        cell = row.createCell(cellFirst + 1);
        //cell.setCellValue(LCName);
        cell.setCellStyle(cellStyleHeaderLoadCaseName);

        cell = row.createCell(cellFirst + 2);
        cell.setCellValue("LC id");
        cell.setCellStyle(cellStyleHeaderLeftCRI);

        cell = row.createCell(cellFirst + 3);
        if (value1Lc != null) cell.setCellValue(value1Lc);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 4);
        if (value2Lc != null) cell.setCellValue(value2Lc);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 5);
        if (value3Lc != null) cell.setCellValue(value3Lc);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 6);
        if (value4Lc != null) cell.setCellValue(value4Lc);
        cell.setCellStyle(cellStyleResults0);

        // --- rowStart + 3: BOLT id
        if (sheet.getRow(rowStart + 3) == null) {
            row = sheet.createRow(rowStart + 3);
        } else {
            row = sheet.getRow(rowStart + 3);
        }

        cell = row.createCell(cellFirst + 1);
        //cell.setCellValue(LCName);
        cell.setCellStyle(cellStyleHeaderLoadCaseName);

        cell = row.createCell(cellFirst + 2);
        cell.setCellValue("Bolt id");
        cell.setCellStyle(cellStyleHeaderLeftCRI);

        cell = row.createCell(cellFirst + 3);
        if (value1Bolt != null) cell.setCellValue(value1Bolt);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 4);
        if (value2Bolt != null) cell.setCellValue(value2Bolt);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 5);
        if (value3Bolt != null) cell.setCellValue(value3Bolt);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 6);
        if (value4Bolt != null) cell.setCellValue(value4Bolt);
        cell.setCellStyle(cellStyleResults0);

        // --- rowStart + 4: cri factor
        if (sheet.getRow(rowStart + 4) == null) {
            row = sheet.createRow(rowStart + 4);
        } else {
            row = sheet.getRow(rowStart + 4);
        }

        cell = row.createCell(cellFirst + 1);
        //cell.setCellValue(LCName);
        cell.setCellStyle(cellStyleHeaderLoadCaseName);

        cell = row.createCell(cellFirst + 2);
        cell.setCellValue("cri factor");
        cell.setCellStyle(cellStyleHeaderLeftCRI);

        cell = row.createCell(cellFirst + 3);
        if (value1CriFact != null) cell.setCellValue(value1CriFact);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 4);
        if (value2CriFact != null) cell.setCellValue(value2CriFact);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 5);
        if (value3CriFact != null) cell.setCellValue(value3CriFact);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 6);
        if (value4CriFact != null) cell.setCellValue(value4CriFact);
        cell.setCellStyle(cellStyleResults0);

        // --- rowStart + 5: extra info
        if (sheet.getRow(rowStart + 5) == null) {
            row = sheet.createRow(rowStart + 5);
        } else {
            row = sheet.getRow(rowStart + 5);
        }

        cell = row.createCell(cellFirst + 1);
        //cell.setCellValue(LCName);
        cell.setCellStyle(cellStyleHeaderLoadCaseName);

        cell = row.createCell(cellFirst + 2);
        cell.setCellValue("extra info");
        cell.setCellStyle(cellStyleHeaderLeftCRI);

        cell = row.createCell(cellFirst + 3);
        if (value1Row != null) cell.setCellValue(value1Row);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 4);
        if (value2Row != null) cell.setCellValue(value2Row);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 5);
        if (value3Row != null) cell.setCellValue(value3Row);
        cell.setCellStyle(cellStyleResults0);

        cell = row.createCell(cellFirst + 6);
        if (value4Row != null) cell.setCellValue(value4Row);
        cell.setCellStyle(cellStyleResults0);

        // merge cells
        sheet.addMergedRegion(new CellRangeAddress(rowStart + 1, rowStart + 5, cellFirst + 1, cellFirst + 1)); //TODO check it

        return cellFirst + 6;
    }

    private int generateExtraResultsTableCBAR(LoadCaseType loadCaseType, int currentRow, String partName, int currentOffset, CsvParserResultExtractor csvParser) {
        int cellFirst = columnCurrentStartId + (COLUMN_OFFSET * currentOffset);
        int cellLast = cellFirst + COLUMN_OFFSET - 1; // currently not in use, delete later
        int currentRowLocation = currentRow;

        String headerLC;
        switch (loadCaseType) {
            case STATIC:
                headerLC = "STATIC LC";
                break;
            case CRASH:
                headerLC = "CRASH LC";
                break;
            case FATIGUE:
                headerLC = "FATIGUE LC";
                break;
            default:
                headerLC = "SOME LC";
        }

        String headerBolt = "BOLT";
        String criFactor = "cri factor";
        String description = "extra info";
        String resultsForce1Name = "Fax [N]";
        String resultsForce2Name = "Fsh1 [N]";
        String resultsForce3Name = "Fsh2 [N]";
        String resultsForce4Name = "Fsht [N]";

        createHeaderForTableResult(currentRowLocation, cellFirst, 0, sheet,
                cellStyleHeaderTopFontLeft, cellStyleHeaderTopFontCenter, cellStyleDescriptionBlank,
                headerBolt, headerLC,
                resultsForce1Name, resultsForce2Name, resultsForce3Name, resultsForce4Name,
                criFactor, description);

        currentRowLocation++;

        // --- row 12-...: results table

        // current row and cell in row to write data
        Cell cell;
        Row row;

        List<Integer> boltIds = new ArrayList<>();
        int backgroundId = 1;

        // get table boltId - lcId: location 0 = boltId, location 1 = lcId
        for (Integer[] pairBoltIdLcId : ((CbarResults) csvParser.getResults().get(partName)).getPairBoltIdLcIdForFaxial().keySet()) {

            // check if load case is on boltab setup list
            if (headerLC.equals("STATIC LC") && !bolTabSetup.getLcIdsForExtraTableStaticLC().contains(pairBoltIdLcId[1])) {
                continue;
            } else if (headerLC.equals("CRASH LC") && !bolTabSetup.getLcIdsForExtraTableCrashLC().contains(pairBoltIdLcId[1])) {
                continue;
            } else if (headerLC.equals("FATIGUE LC") && !bolTabSetup.getLcIdsForExtraTableFatigueLC().contains(pairBoltIdLcId[1])) {
                continue;
            }

            // mechanism to change background - change colour after passing all load cases per bolt
            if (boltIds.contains(pairBoltIdLcId[0])) {
                boltIds = new ArrayList<>();
                backgroundId++;
                boltIds.add(pairBoltIdLcId[0]);
            } else {
                boltIds.add(pairBoltIdLcId[0]);
            }

            // bolt id
            if (sheet.getRow(currentRowLocation) == null) {
                row = sheet.createRow(currentRowLocation++);
            } else {
                row = sheet.getRow(currentRowLocation++);
            }
            cell = row.createCell(cellFirst + 2);
            cell.setCellValue(pairBoltIdLcId[0]);
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleBolt1);
            } else {
                cell.setCellStyle(cellStyleBolt2);
            }

            // lc id
            cell = row.createCell(cellFirst + 3);
            cell.setCellValue(pairBoltIdLcId[1]);
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleLC1);
            } else {
                cell.setCellStyle(cellStyleLC2);
            }

            // VALUE Faxial
            cell = row.createCell(cellFirst + 4);
            cell.setCellValue(((CbarResults) csvParser.getResults().get(partName)).getPairBoltIdLcIdForFaxial().get(pairBoltIdLcId));
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleResults1);
            } else {
                cell.setCellStyle(cellStyleResults2);
            }

            // VALUE Fshear1
            cell = row.createCell(cellFirst + 5);
            cell.setCellValue(((CbarResults) csvParser.getResults().get(partName)).getPairBoltIdLcIdForFshear1().get(pairBoltIdLcId));
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleResults1);
            } else {
                cell.setCellStyle(cellStyleResults2);
            }

            // VALUE Fshear2
            cell = row.createCell(cellFirst + 6);
            cell.setCellValue(((CbarResults) csvParser.getResults().get(partName)).getPairBoltIdLcIdForFshear2().get(pairBoltIdLcId));
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleResults1);
            } else {
                cell.setCellStyle(cellStyleResults2);
            }

            // VALUE Fshear total
            cell = row.createCell(cellFirst + 7);
            cell.setCellValue(((CbarResults) csvParser.getResults().get(partName)).getPairBoltIdLcIdForFshearTotal().get(pairBoltIdLcId));
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleResults1);
            } else {
                cell.setCellStyle(cellStyleResults2);
            }

            // VALUE CRI FACTOR
            cell = row.createCell(cellFirst + 8);
            cell.setCellValue(((CbarResults) csvParser.getResults().get(partName)).getPairBoltIdLcIdForCriFactor().get(pairBoltIdLcId));
            if (backgroundId % 2 == 0) {
                cell.setCellStyle(cellStyleResults1);
            } else {
                cell.setCellStyle(cellStyleResults2);
            }
        }
        currentRowLocation++;
        return currentRowLocation;
    }

    private String generateFileNameToSave() {
        StringBuilder fileName = new StringBuilder();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss");

        fileName.append(LocalDateTime.now().format(dateTimeFormatter));
        fileName.append("_Bolts_");
        fileName.append(System.getProperty("user.name").toUpperCase());
        fileName.append(".xlsx");

        return fileName.toString();
    }

    public void saveToFile() {
        // do not save if results is other
        if (shouldSetPathToSave) {
            System.out.println("\t[!!] Nothing has been done.");
            return;
        }

        // generate file name
        String fileNameWithExtension = generateFileNameToSave();

        // write to file
        String filePathSlashesToSave = csvFilePathWithoutFileName + fileNameWithExtension;
        System.out.println("[OK] Saving to a file: " + filePathSlashesToSave);

        try (FileOutputStream outputStream = new FileOutputStream(filePathSlashesToSave)) {
            workbook.write(outputStream);
            workbook.close();
        } catch (IOException exc) {
            System.out.println("[!!] Problem with writing a file: " + filePathSlashesToSave + ". Ask MR.");
        }
    }

    private String updateBackSlashes2Slashes(String filePath) {
        return filePath.replaceAll("\\\\", "/");
    }

    private String getFilePathWithoutFileName(String filePathSlashesWithFileName) {
        String fileName = filePathSlashesWithFileName.replaceAll(".*[/]", "");
        String filePath = filePathSlashesWithFileName.replaceAll(fileName, "");
        return filePath;
    }

    public void setResultType(ResultType resultType) {
        this.resultType = resultType;
    }

    // FIXME redundant methods - delete them later

    private String getFileNameWithExtension(String filePath) {
        return filePath.replaceAll(".*[/]", "");
    }

    private String getReplacedExtension(String csvFilePath, String extensionFrom, String extensionTo) {
        return csvFilePath.replaceAll((extensionFrom + "$"), extensionTo);
    }
}
