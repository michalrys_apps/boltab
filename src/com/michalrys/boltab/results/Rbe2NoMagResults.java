package com.michalrys.boltab.results;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Rbe2NoMagResults implements FemResults {
    private String partName;
    private Integer currentBoltId;
    private Integer currentLCId;
    private double currentCrifactor;
    private double currentFmag;
    private double currentFx;
    private double currentFy;
    private double currentFz;
    private double frictionCoefficient = 0.15;
    private boolean isNAasValue = false;

    private Map<Integer[], Double> pairBoltIdLcIdForFy = new LinkedHashMap<>();
    private Map<Integer[], Double> pairBoltIdLcIdForFmag = new LinkedHashMap<>();
    private Map<Integer[], Double> pairBoltIdLcIdForFx = new LinkedHashMap<>();
    private Map<Integer[], Double> pairBoltIdLcIdForFz = new LinkedHashMap<>();
    private Map<Integer[], Double> pairBoltIdLcIdForCriFactor = new LinkedHashMap<>();

    private Double criFy;
    private Double criFmag;
    private Double criFx;
    private Double criFz;
    private Double criFactor;
    private Integer criBoltId;
    private Integer criLCId;

    private double maxFy;
    private int maxFyBoltId;
    private int maxFyLcId;
    private Double maxFmag;
    private Integer maxFmagBoltId;
    private Integer maxFmagLcId;
    private double maxFx;
    private int maxFxBoltId;
    private int maxFxLcId;
    private double maxFz;
    private int maxFzBoltId;
    private int maxFzLcId;
    private Double maxFyCriFactor;
    private Double maxFmagCriFactor;
    private Double maxFxCriFactor;
    private Double maxFzCriFactor;

    private double minFy;
    private int minFyBoltId;
    private int minFyLcId;
    private Double minFmag;
    private Integer minFmagBoltId;
    private Integer minFmagLcId;
    private double minFx;
    private int minFxBoltId;
    private int minFxLcId;
    private double minFz;
    private int minFzBoltId;
    private int minFzLcId;
    private Double minFyCriFactor;
    private Double minFmagCriFactor;
    private Double minFxCriFactor;
    private Double minFzCriFactor;

    // abs <=> F>0
    private double absMinFy;
    private int absMinFyBoltId;
    private int absMinFyLcId;
    private Double absMinFmag;
    private Integer absMinFmagBoltId;
    private Integer absMinFmagLcId;
    private double absMinFx;
    private int absMinFxBoltId;
    private int absMinFxLcId;
    private double absMinFz;
    private int absMinFzBoltId;
    private int absMinFzLcId;
    private Double absMinFyCriFactor;
    private Double absMinFmagCriFactor;
    private Double absMinFxCriFactor;
    private Double absMinFzCriFactor;

    public Rbe2NoMagResults(String results) {
        // csv file with RBE2 results without Mag:
        //       0     1 2 3   4         5       6         7          8         9          10           11            12
        // #elementsID,X,Y,Z,SystemID,PartName,Subcase,Simulation,ResultType,CBAR Shear1,CBAR Shear2,CBAR Axial Force
        //    #nodesID,X,Y,Z,SystemID,PartName,Subcase,Simulation,ResultType,       X,           Y,               Z
        String resultsSplitted[] = results.replaceAll(":", "-").split(",");

        partName = resultsSplitted[5];
        // read current bolt id and lc
        currentBoltId = Integer.valueOf(resultsSplitted[0]);
        currentLCId = Integer.valueOf(resultsSplitted[6].split(" ")[1]);

        // read forces
        Integer[] currentBoltIdAndCurrentLCid = {currentBoltId, currentLCId};

        // if there is no results - N/A - set isNAasValue true and null for everything
        if (resultsSplitted[9].equals("N/A")) {
            isNAasValue = true;
            pairBoltIdLcIdForFx.put(currentBoltIdAndCurrentLCid, null);
            return;
        }
        if (resultsSplitted[10].equals("N/A")) {
            isNAasValue = true;
            pairBoltIdLcIdForFy.put(currentBoltIdAndCurrentLCid, null);
            return;
        }
        if (resultsSplitted[11].equals("N/A")) {
            isNAasValue = true;
            pairBoltIdLcIdForFz.put(currentBoltIdAndCurrentLCid, null);
            return;
        }
        pairBoltIdLcIdForFx.put(currentBoltIdAndCurrentLCid, Double.valueOf(resultsSplitted[9]));
        pairBoltIdLcIdForFy.put(currentBoltIdAndCurrentLCid, Double.valueOf(resultsSplitted[10]));
        pairBoltIdLcIdForFz.put(currentBoltIdAndCurrentLCid, Double.valueOf(resultsSplitted[11]));
    }

    public void addResults(String results) {
        // csv file with RBE2 results without Mag:
        //       0     1 2 3   4         5       6         7          8         9          10           11            12
        // #elementsID,X,Y,Z,SystemID,PartName,Subcase,Simulation,ResultType,CBAR Shear1,CBAR Shear2,CBAR Axial Force
        //    #nodesID,X,Y,Z,SystemID,PartName,Subcase,Simulation,ResultType,        X,            Y,             Z
        String resultsSplitted[] = results.replaceAll(":", "-").split(",");

        // add new bolt results only if results come from the same part
        if (partName.equals(resultsSplitted[5])) {
            // read current bolt id and lc
            currentBoltId = Integer.valueOf(resultsSplitted[0]);
            currentLCId = Integer.valueOf(resultsSplitted[6].split(" ")[1]);

            // read forces
            Integer[] currentBoltIdAndCurrentLCid = {currentBoltId, currentLCId};

            // if there is no results - N/A - set isNAasValue true and null for everything
            if (resultsSplitted[9].equals("N/A")) {
                isNAasValue = true;
                pairBoltIdLcIdForFx.put(currentBoltIdAndCurrentLCid, null);
                return;
            }
            if (resultsSplitted[10].equals("N/A")) {
                isNAasValue = true;
                pairBoltIdLcIdForFy.put(currentBoltIdAndCurrentLCid, null);
                return;
            }
            if (resultsSplitted[11].equals("N/A")) {
                isNAasValue = true;
                pairBoltIdLcIdForFz.put(currentBoltIdAndCurrentLCid, null);
                return;
            }
            pairBoltIdLcIdForFx.put(currentBoltIdAndCurrentLCid, Double.valueOf(resultsSplitted[9]));
            pairBoltIdLcIdForFy.put(currentBoltIdAndCurrentLCid, Double.valueOf(resultsSplitted[10]));
            pairBoltIdLcIdForFz.put(currentBoltIdAndCurrentLCid, Double.valueOf(resultsSplitted[11]));
        }
    }

    @Override
    public void calculateCriFactor() {
        if (isNAasValue) {
            // do nothing
            return;
        }
        // TODO user must define which force should be combined
        for (Integer[] currentBoltIdAndCurrentLCid : pairBoltIdLcIdForFx.keySet()) {
            // calculate cri factor
            currentCrifactor = 0 + (0 / frictionCoefficient);  // TODO user must define which force should be combined
            pairBoltIdLcIdForCriFactor.put(currentBoltIdAndCurrentLCid, currentCrifactor);
        }
    }

    public void setMaxMinAbove0Min() {
        // if there was not results -> N/A -> set null for everything
        if (isNAasValue) {
            System.out.println("\t[!!] For part name: " + partName + " there are not results. Values have N/A - check it.");
            return;
        }

        // critical bolt
        // there should be null at the momonet TODO make user defined force definition
        criBoltId = null;
        criLCId = null;
        criFactor = null;
        criFy = null;
        criFmag = null;
        criFx = null;
        criFz = null;

        // max Fy
        Map<Integer[], Double> maxBoltsIdFy = findMax(pairBoltIdLcIdForFy);
        for (Integer[] boltIdAndLcId : maxBoltsIdFy.keySet()) {
            maxFyBoltId = boltIdAndLcId[0];
            maxFyLcId = boltIdAndLcId[1];
            maxFy = maxBoltsIdFy.get(boltIdAndLcId);
            maxFyCriFactor = null;
        }

        // max Fmag
        Map<Integer[], Double> maxBoltsIdFmag = findMax(pairBoltIdLcIdForFmag);
        for (Integer[] boltIdAndLcId : maxBoltsIdFmag.keySet()) {
            maxFmagBoltId = null;
            maxFmagLcId = null;
            maxFmag = null;
            maxFmagCriFactor = null;
        }

        // max Fx
        Map<Integer[], Double> maxBoltsIdFx = findMax(pairBoltIdLcIdForFx);
        for (Integer[] boltIdAndLcId : maxBoltsIdFx.keySet()) {
            maxFxBoltId = boltIdAndLcId[0];
            maxFxLcId = boltIdAndLcId[1];
            maxFx = maxBoltsIdFx.get(boltIdAndLcId);
            maxFxCriFactor = null;
        }

        // max Fz
        Map<Integer[], Double> maxBoltsIdFz = findMax(pairBoltIdLcIdForFz);
        for (Integer[] boltIdAndLcId : maxBoltsIdFz.keySet()) {
            maxFzBoltId = boltIdAndLcId[0];
            maxFzLcId = boltIdAndLcId[1];
            maxFz = maxBoltsIdFz.get(boltIdAndLcId);
            maxFzCriFactor = null;
        }
        // -------------------- min
        // min Fy
        Map<Integer[], Double> minBoltsIdFy = findMin(pairBoltIdLcIdForFy);
        for (Integer[] boltIdAndLcId : minBoltsIdFy.keySet()) {
            minFyBoltId = boltIdAndLcId[0];
            minFyLcId = boltIdAndLcId[1];
            minFy = minBoltsIdFy.get(boltIdAndLcId);
            minFyCriFactor = null;
        }

        // min Fmag
        Map<Integer[], Double> minBoltsIdFmag = findMin(pairBoltIdLcIdForFmag);
        for (Integer[] boltIdAndLcId : minBoltsIdFmag.keySet()) {
            minFmagBoltId = null;
            minFmagLcId = null;
            minFmag = null;
            minFmagCriFactor = null;
        }

        // min Fx
        Map<Integer[], Double> minBoltsIdFx = findMin(pairBoltIdLcIdForFx);
        for (Integer[] boltIdAndLcId : minBoltsIdFx.keySet()) {
            minFxBoltId = boltIdAndLcId[0];
            minFxLcId = boltIdAndLcId[1];
            minFx = minBoltsIdFx.get(boltIdAndLcId);
            minFxCriFactor = null;
        }

        // min Fz
        Map<Integer[], Double> minBoltsIdFz = findMin(pairBoltIdLcIdForFz);
        for (Integer[] boltIdAndLcId : minBoltsIdFz.keySet()) {
            minFzBoltId = boltIdAndLcId[0];
            minFzLcId = boltIdAndLcId[1];
            minFz = minBoltsIdFz.get(boltIdAndLcId);
            minFzCriFactor = null;
        }

        // -------------F>0 min
        // F>0Min Fy
        Map<Integer[], Double> absMinBoltsIdFy = findFabove0Min(pairBoltIdLcIdForFy);
        for (Integer[] boltIdAndLcId : absMinBoltsIdFy.keySet()) {
            absMinFyBoltId = boltIdAndLcId[0];
            absMinFyLcId = boltIdAndLcId[1];
            absMinFy = absMinBoltsIdFy.get(boltIdAndLcId);
            absMinFyCriFactor = null;
        }

        // F>0Min Fmag
        Map<Integer[], Double> absMinBoltsIdFmag = findFabove0Min(pairBoltIdLcIdForFmag);
        for (Integer[] boltIdAndLcId : absMinBoltsIdFmag.keySet()) {
            absMinFmagBoltId = null;
            absMinFmagLcId = null;
            absMinFmag = null;
            absMinFmagCriFactor = null;
        }

        // F>0Min Fx
        Map<Integer[], Double> absMinBoltsIdFx = findFabove0Min(pairBoltIdLcIdForFx);
        for (Integer[] boltIdAndLcId : absMinBoltsIdFx.keySet()) {
            absMinFxBoltId = boltIdAndLcId[0];
            absMinFxLcId = boltIdAndLcId[1];
            absMinFx = absMinBoltsIdFx.get(boltIdAndLcId);
            absMinFxCriFactor = null;
        }

        // F>0Min Fz
        Map<Integer[], Double> absMinBoltsIdFz = findFabove0Min(pairBoltIdLcIdForFz);
        for (Integer[] boltIdAndLcId : absMinBoltsIdFz.keySet()) {
            absMinFzBoltId = boltIdAndLcId[0];
            absMinFzLcId = boltIdAndLcId[1];
            absMinFz = absMinBoltsIdFz.get(boltIdAndLcId);
            absMinFzCriFactor = null;
        }
    }


    private Map<Integer[], Double> findFabove0Min(Map<Integer[], Double> boltIdAndLcIdForValues) {
        Map<Integer[], Double> boltIdAndLcIdForAbsMinValue = new HashMap<>(1);

        Double absMinValue = 1e50;
        Integer[] boltIdForAbsMinValue = null;
        boolean didnotFoundValue = true;

        for (Integer[] boltIdAndLcId : boltIdAndLcIdForValues.keySet()) {
            if (boltIdAndLcIdForValues.get(boltIdAndLcId) < absMinValue && boltIdAndLcIdForValues.get(boltIdAndLcId) > 0) {
                boltIdForAbsMinValue = boltIdAndLcId;
                absMinValue = boltIdAndLcIdForValues.get(boltIdAndLcId);
                didnotFoundValue = false;
            }
        }
        if (didnotFoundValue) {
            for (Integer[] boltIdAndLcId : boltIdAndLcIdForValues.keySet()) {
                if (boltIdAndLcIdForValues.get(boltIdAndLcId) < absMinValue) {
                    boltIdForAbsMinValue = boltIdAndLcId;
                    absMinValue = boltIdAndLcIdForValues.get(boltIdAndLcId);
                }
            }
            absMinValue = 0.0;
            boltIdAndLcIdForAbsMinValue.put(boltIdForAbsMinValue, absMinValue);
            return boltIdAndLcIdForAbsMinValue;
        }

        boltIdAndLcIdForAbsMinValue.put(boltIdForAbsMinValue, absMinValue);

        return boltIdAndLcIdForAbsMinValue;
    }

    private Map<Integer[], Double> findMin(Map<Integer[], Double> boltIdAndLcIdForValues) {
        Map<Integer[], Double> boltIdAndLcIdForMinValue = new HashMap<>(1);

        Double minValue = 1e50;
        Integer[] boltIdForMinValue = null;

        for (Integer[] boltIdAndLcId : boltIdAndLcIdForValues.keySet()) {
            if (boltIdAndLcIdForValues.get(boltIdAndLcId) <= minValue) {
                boltIdForMinValue = boltIdAndLcId;
                minValue = boltIdAndLcIdForValues.get(boltIdAndLcId);
            }
        }
        boltIdAndLcIdForMinValue.put(boltIdForMinValue, minValue);

        return boltIdAndLcIdForMinValue;
    }

    private Map<Integer[], Double> findMax(Map<Integer[], Double> boltIdAndLcIdForValues) {
        Map<Integer[], Double> boltIdAndLcIdForMaxValue = new HashMap<>(1);

        Double maxValue = -1e50; // with Double.MIN_VALUE was null pointer exception
        Integer[] boltIdForMaxValue = null;

        for (Integer[] boltIdAndLcId : boltIdAndLcIdForValues.keySet()) {
            if (boltIdAndLcIdForValues.get(boltIdAndLcId) >= maxValue) {
                boltIdForMaxValue = boltIdAndLcId;
                maxValue = boltIdAndLcIdForValues.get(boltIdAndLcId);
            }
        }

        boltIdAndLcIdForMaxValue.put(boltIdForMaxValue, maxValue);

        return boltIdAndLcIdForMaxValue;
    }

    public String getPartName() {
        return partName;
    }

    public Integer getCurrentBoltId() {
        return currentBoltId;
    }

    public Integer getCurrentLCId() {
        return currentLCId;
    }

    public double getCurrentCrifactor() {
        return currentCrifactor;
    }

    public double getCurrentFz() {
        return currentFz;
    }

    public double getCurrentFmag() {
        return currentFmag;
    }

    public double getCurrentFx() {
        return currentFx;
    }

    public double getCurrentFy() {
        return currentFy;
    }

    public Map<Integer[], Double> getPairBoltIdLcIdForFy() {
        return pairBoltIdLcIdForFy;
    }

    public Map<Integer[], Double> getPairBoltIdLcIdForFmag() {
        return pairBoltIdLcIdForFmag;
    }

    public Map<Integer[], Double> getPairBoltIdLcIdForFx() {
        return pairBoltIdLcIdForFx;
    }

    public Map<Integer[], Double> getPairBoltIdLcIdForFz() {
        return pairBoltIdLcIdForFz;
    }

    public Map<Integer[], Double> getPairBoltIdLcIdForCriFactor() {
        return pairBoltIdLcIdForCriFactor;
    }

    public double getCriFy() {
        return criFy;
    }

    public double getCriFmag() {
        return criFmag;
    }

    public double getCriFx() {
        return criFx;
    }

    public double getCriFz() {
        return criFz;
    }

    public double getCriFactor() {
        return criFactor;
    }

    public int getCriBoltId() {
        return criBoltId;
    }

    public int getCriLCId() {
        return criLCId;
    }

    public double getMaxFy() {
        return maxFy;
    }

    public int getMaxFyBoltId() {
        return maxFyBoltId;
    }

    public int getMaxFyLcId() {
        return maxFyLcId;
    }

    public double getMaxFmag() {
        return maxFmag;
    }

    public int getMaxFmagBoltId() {
        return maxFmagBoltId;
    }

    public int getMaxFmagLcId() {
        return maxFmagLcId;
    }

    public double getMaxFx() {
        return maxFx;
    }

    public int getMaxFxBoltId() {
        return maxFxBoltId;
    }

    public int getMaxFxLcId() {
        return maxFxLcId;
    }

    public double getMaxFz() {
        return maxFz;
    }

    public int getMaxFzBoltId() {
        return maxFzBoltId;
    }

    public int getMaxFzLcId() {
        return maxFzLcId;
    }

    public double getMaxFyCriFactor() {
        return maxFyCriFactor;
    }

    public double getMaxFmagCriFactor() {
        return maxFmagCriFactor;
    }

    public double getMaxFxCriFactor() {
        return maxFxCriFactor;
    }

    public double getMaxFzCriFactor() {
        return maxFzCriFactor;
    }

    public double getMinFy() {
        return minFy;
    }

    public int getMinFyBoltId() {
        return minFyBoltId;
    }

    public int getMinFyLcId() {
        return minFyLcId;
    }

    public double getMinFmag() {
        return minFmag;
    }

    public int getMinFmagBoltId() {
        return minFmagBoltId;
    }

    public int getMinFmagLcId() {
        return minFmagLcId;
    }

    public double getMinFx() {
        return minFx;
    }

    public int getMinFxBoltId() {
        return minFxBoltId;
    }

    public int getMinFxLcId() {
        return minFxLcId;
    }

    public double getMinFz() {
        return minFz;
    }

    public int getMinFzBoltId() {
        return minFzBoltId;
    }

    public int getMinFzLcId() {
        return minFzLcId;
    }

    public double getMinFyCriFactor() {
        return minFyCriFactor;
    }

    public double getMinFmagCriFactor() {
        return minFmagCriFactor;
    }

    public double getMinFxCriFactor() {
        return minFxCriFactor;
    }

    public double getMinFzCriFactor() {
        return minFzCriFactor;
    }

    public double getAbsMinFy() {
        return absMinFy;
    }

    public int getAbsMinFyBoltId() {
        return absMinFyBoltId;
    }

    public int getAbsMinFyLcId() {
        return absMinFyLcId;
    }

    public double getAbsMinFmag() {
        return absMinFmag;
    }

    public int getAbsMinFmagBoltId() {
        return absMinFmagBoltId;
    }

    public int getAbsMinFmagLcId() {
        return absMinFmagLcId;
    }

    public double getAbsMinFx() {
        return absMinFx;
    }

    public int getAbsMinFxBoltId() {
        return absMinFxBoltId;
    }

    public int getAbsMinFxLcId() {
        return absMinFxLcId;
    }

    public double getAbsMinFz() {
        return absMinFz;
    }

    public int getAbsMinFzBoltId() {
        return absMinFzBoltId;
    }

    public int getAbsMinFzLcId() {
        return absMinFzLcId;
    }

    public double getAbsMinFyCriFactor() {
        return absMinFyCriFactor;
    }

    public double getAbsMinFmagCriFactor() {
        return absMinFmagCriFactor;
    }

    public double getAbsMinFxCriFactor() {
        return absMinFxCriFactor;
    }

    public double getAbsMinFzCriFactor() {
        return absMinFzCriFactor;
    }

    @Override
    public void setFrictionCoefficient(double frictionCoefficient) {
        this.frictionCoefficient = frictionCoefficient;
    }

    @Override
    public double getFrictionCoefficient() {
        return frictionCoefficient;
    }

    public boolean isNAasValue() {
        return isNAasValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rbe2NoMagResults that = (Rbe2NoMagResults) o;

        return partName != null ? partName.equals(that.partName) : that.partName == null;
    }

    @Override
    public int hashCode() {
        return partName != null ? partName.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "<rbe2 results for part " + partName + ">";
    }
}
