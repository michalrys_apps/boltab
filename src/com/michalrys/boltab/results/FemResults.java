package com.michalrys.boltab.results;

public interface FemResults {
    void addResults(String results);

    void setMaxMinAbove0Min();

    boolean isNAasValue();

    void calculateCriFactor();

    void setFrictionCoefficient(double frictionCoefficient);

    double getFrictionCoefficient();

    // FIXME redundant methods - delete them later
}
