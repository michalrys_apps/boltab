package com.michalrys.boltab.results;

import com.michalrys.boltab.boltabsetup.LoadCaseType;

import java.util.*;

public class CbarResults implements FemResults {
    private String partName;
    private Integer currentBoltId;
    private Integer currentLCId;
    private double currentCrifactor;
    private double currentShearTotal;
    private double currentShear1;
    private double currentShear2;
    private double currentAxial;
    private double frictionCoefficient;
    private boolean isNAasValue = false;

    private Map<Integer[], Double> pairBoltIdLcIdForFaxial = new LinkedHashMap<>();
    private Map<Integer[], Double> pairBoltIdLcIdForFshear1 = new LinkedHashMap<>();
    private Map<Integer[], Double> pairBoltIdLcIdForFshear2 = new LinkedHashMap<>();
    private Map<Integer[], Double> pairBoltIdLcIdForFshearTotal = new LinkedHashMap<>();
    private Map<Integer[], Double> pairBoltIdLcIdForCriFactor = new LinkedHashMap<>();

    private double criFaxial;
    private double criFshear1;
    private double criFshear2;
    private double criFshearTotal;
    private double criFactor;
    private int criBoltId;
    private int criLCId;

    private Double criStaticFaxial;
    private Double criStaticFshear1;
    private Double criStaticFshear2;
    private Double criStaticFshearTotal;
    private Double criStaticFactor;
    private Integer criStaticBoltId;
    private Integer criStaticLCId;

    private Double criCrashFaxial;
    private Double criCrashFshear1;
    private Double criCrashFshear2;
    private Double criCrashFshearTotal;
    private Double criCrashFactor;
    private Integer criCrashBoltId;
    private Integer criCrashLCId;

    private Double criFatigueFaxial;
    private Double criFatigueFshear1;
    private Double criFatigueFshear2;
    private Double criFatigueFshearTotal;
    private Double criFatigueFactor;
    private Integer criFatigueBoltId;
    private Integer criFatigueLCId;

    private double maxFaxial;
    private int maxFaxialBoltId;
    private int maxFaxialLcId;
    private double maxFaxialCriFactor;
    private double maxFaxial_shear1;
    private double maxFaxial_shear2;
    private double maxFaxial_shearTotal;

    private double maxFshear1;
    private double maxFshear1CriFactor;
    private int maxFshear1BoltId;
    private int maxFshear1LcId;
    private double maxFshear1_axial;
    private double maxFshear1_shear2;
    private double maxFshear1_shearTotal;

    private double maxFshear2;
    private double maxFshear2CriFactor;
    private int maxFshear2BoltId;
    private int maxFshear2LcId;
    private double maxFshear2_axial;
    private double maxFshear2_shear1;
    private double maxFshear2_shearTotal;

    private double maxFshearTotal;
    private double maxFshearTotalCriFactor;
    private int maxFshearTotalBoltId;
    private int maxFshearTotalLcId;
    private double maxFshearTotal_axial;
    private double maxFshearTotal_shear1;
    private double maxFshearTotal_shear2;

    private Double maxStaticFaxial;
    private Double maxStaticFaxial_shear1;
    private Double maxStaticFaxial_shear2;
    private Double maxStaticFaxial_shearTotal;
    private Double maxStaticFaxialCriFactor;
    private Integer maxStaticFaxialBoltId;
    private Integer maxStaticFaxialLcId;

    private Double maxStaticFshear1;
    private Double maxStaticFshear1_axial;
    private Double maxStaticFshear1_shear2;
    private Double maxStaticFshear1_shearTotal;
    private Double maxStaticFshear1CriFactor;
    private Integer maxStaticFshear1BoltId;
    private Integer maxStaticFshear1LcId;

    private Double maxStaticFshear2;
    private Double maxStaticFshear2_axial;
    private Double maxStaticFshear2_shear1;
    private Double maxStaticFshear2_shearTotal;
    private Double maxStaticFshear2CriFactor;
    private Integer maxStaticFshear2BoltId;
    private Integer maxStaticFshear2LcId;

    private Double maxStaticFshearTotal;
    private Double maxStaticFshearTotal_axial;
    private Double maxStaticFshearTotal_shear1;
    private Double maxStaticFshearTotal_shear2;
    private Double maxStaticFshearTotalCriFactor;
    private Integer maxStaticFshearTotalBoltId;
    private Integer maxStaticFshearTotalLcId;

    private Double maxCrashFaxial;
    private Double maxCrashFaxial_shear1;
    private Double maxCrashFaxial_shear2;
    private Double maxCrashFaxial_shearTotal;
    private Integer maxCrashFaxialBoltId;
    private Integer maxCrashFaxialLcId;
    private Double maxCrashFaxialCriFactor;

    private Double maxCrashFshear1;
    private Double maxCrashFshear1_axial;
    private Double maxCrashFshear1_shear2;
    private Double maxCrashFshear1_shearTotal;
    private Integer maxCrashFshear1BoltId;
    private Integer maxCrashFshear1LcId;
    private Double maxCrashFshear1CriFactor;

    private Double maxCrashFshear2;
    private Double maxCrashFshear2_axial;
    private Double maxCrashFshear2_shear1;
    private Double maxCrashFshear2_shearTotal;
    private Integer maxCrashFshear2BoltId;
    private Integer maxCrashFshear2LcId;
    private Double maxCrashFshear2CriFactor;

    private Double maxCrashFshearTotal;
    private Double maxCrashFshearTotal_shear1;
    private Double maxCrashFshearTotal_shear2;
    private Double maxCrashFshearTotal_axial;
    private Integer maxCrashFshearTotalBoltId;
    private Integer maxCrashFshearTotalLcId;
    private Double maxCrashFshearTotalCriFactor;

    private Double maxFatigueFaxial;
    private Double maxFatigueFaxial_shear1;
    private Double maxFatigueFaxial_shear2;
    private Double maxFatigueFaxial_shearTotal;
    private Integer maxFatigueFaxialBoltId;
    private Integer maxFatigueFaxialLcId;
    private Double maxFatigueFaxialCriFactor;

    private Double maxFatigueFshear1;
    private Double maxFatigueFshear1_axial;
    private Double maxFatigueFshear1_shear2;
    private Double maxFatigueFshear1_shearTotal;
    private Integer maxFatigueFshear1BoltId;
    private Integer maxFatigueFshear1LcId;
    private Double maxFatigueFshear1CriFactor;

    private Double maxFatigueFshear2;
    private Double maxFatigueFshear2_axial;
    private Double maxFatigueFshear2_shear1;
    private Double maxFatigueFshear2_shearTotal;
    private Integer maxFatigueFshear2BoltId;
    private Integer maxFatigueFshear2LcId;
    private Double maxFatigueFshear2CriFactor;

    private Double maxFatigueFshearTotal;
    private Double maxFatigueFshearTotal_axial;
    private Double maxFatigueFshearTotal_shear1;
    private Double maxFatigueFshearTotal_shear2;
    private Integer maxFatigueFshearTotalBoltId;
    private Integer maxFatigueFshearTotalLcId;
    private Double maxFatigueFshearTotalCriFactor;

    private double minFaxial;
    private double minFaxialCriFactor;
    private int minFaxialBoltId;
    private int minFaxialLcId;
    private double minFaxial_shear1;
    private double minFaxial_shear2;
    private double minFaxial_shearTotal;

    private double minFshear1;
    private double minFshear1CriFactor;
    private int minFshear1BoltId;
    private int minFshear1LcId;
    private double minFshear1_axial;
    private double minFshear1_shear2;
    private double minFshear1_shearTotal;

    private double minFshear2;
    private double minFshear2CriFactor;
    private int minFshear2BoltId;
    private int minFshear2LcId;
    private double minFshear2_axial;
    private double minFshear2_shear1;
    private double minFshear2_shearTotal;

    private double minFshearTotal;
    private double minFshearTotalCriFactor;
    private int minFshearTotalBoltId;
    private int minFshearTotalLcId;
    private double minFshearTotal_axial;
    private double minFshearTotal_shear1;
    private double minFshearTotal_shear2;

    private Double minStaticFaxial;
    private Double minStaticFaxial_shear1;
    private Double minStaticFaxial_shear2;
    private Double minStaticFaxial_shearTotal;
    private Integer minStaticFaxialBoltId;
    private Integer minStaticFaxialLcId;
    private Double minStaticFaxialCriFactor;

    private Double minStaticFshear1;
    private Double minStaticFshear1_axial;
    private Double minStaticFshear1_shear2;
    private Double minStaticFshear1_shearTotal;
    private Integer minStaticFshear1BoltId;
    private Integer minStaticFshear1LcId;
    private Double minStaticFshear1CriFactor;

    private Double minStaticFshear2;
    private Double minStaticFshear2_axial;
    private Double minStaticFshear2_shear1;
    private Double minStaticFshear2_shearTotal;
    private Integer minStaticFshear2BoltId;
    private Integer minStaticFshear2LcId;
    private Double minStaticFshear2CriFactor;

    private Double minStaticFshearTotal;
    private Double minStaticFshearTotal_axial;
    private Double minStaticFshearTotal_shear1;
    private Double minStaticFshearTotal_shear2;
    private Integer minStaticFshearTotalBoltId;
    private Integer minStaticFshearTotalLcId;
    private Double minStaticFshearTotalCriFactor;

    private Double minFatigueFaxial;
    private Double minFatigueFaxial_shear1;
    private Double minFatigueFaxial_shear2;
    private Double minFatigueFaxial_shearTotal;
    private Integer minFatigueFaxialBoltId;
    private Integer minFatigueFaxialLcId;
    private Double minFatigueFaxialCriFactor;

    private Double minFatigueFshear1;
    private Double minFatigueFshear1_axial;
    private Double minFatigueFshear1_shear2;
    private Double minFatigueFshear1_shearTotal;
    private Integer minFatigueFshear1BoltId;
    private Integer minFatigueFshear1LcId;
    private Double minFatigueFshear1CriFactor;

    private Double minFatigueFshear2;
    private Double minFatigueFshear2_axial;
    private Double minFatigueFshear2_shear1;
    private Double minFatigueFshear2_shearTotal;
    private Integer minFatigueFshear2BoltId;
    private Integer minFatigueFshear2LcId;
    private Double minFatigueFshear2CriFactor;

    private Double minFatigueFshearTotal;
    private Double minFatigueFshearTotal_axial;
    private Double minFatigueFshearTotal_shear1;
    private Double minFatigueFshearTotal_shear2;
    private Integer minFatigueFshearTotalBoltId;
    private Integer minFatigueFshearTotalLcId;
    private Double minFatigueFshearTotalCriFactor;

    private Double minCrashFaxial;
    private Double minCrashFaxial_shear1;
    private Double minCrashFaxial_shear2;
    private Double minCrashFaxial_shearTotal;
    private Integer minCrashFaxialBoltId;
    private Integer minCrashFaxialLcId;
    private Double minCrashFaxialCriFactor;

    private Double minCrashFshear1;
    private Double minCrashFshear1_axial;
    private Double minCrashFshear1_shear2;
    private Double minCrashFshear1_shearTotal;
    private Integer minCrashFshear1BoltId;
    private Integer minCrashFshear1LcId;
    private Double minCrashFshear1CriFactor;

    private Double minCrashFshear2;
    private Double minCrashFshear2_axial;
    private Double minCrashFshear2_shear1;
    private Double minCrashFshear2_shearTotal;
    private Integer minCrashFshear2BoltId;
    private Integer minCrashFshear2LcId;
    private Double minCrashFshear2CriFactor;

    private Double minCrashFshearTotal;
    private Double minCrashFshearTotal_axial;
    private Double minCrashFshearTotal_shear1;
    private Double minCrashFshearTotal_shear2;
    private Integer minCrashFshearTotalBoltId;
    private Integer minCrashFshearTotalLcId;
    private Double minCrashFshearTotalCriFactor;

    // abs <=> F>0
    private double absMinFaxial;
    private double absMinFaxialCriFactor;
    private int absMinFaxialBoltId;
    private int absMinFaxialLcId;
    private double absMinFaxial_shear1;
    private double absMinFaxial_shear2;
    private double absMinFaxial_shearTotal;

    private double absMinFshear1;
    private double absMinFshear1CriFactor;
    private int absMinFshear1BoltId;
    private int absMinFshear1LcId;
    private double absMinFshear1_axial;
    private double absMinFshear1_shear2;
    private double absMinFshear1_shearTotal;

    private double absMinFshear2;
    private double absMinFshear2CriFactor;
    private int absMinFshear2BoltId;
    private int absMinFshear2LcId;
    private double absMinFshear2_axial;
    private double absMinFshear2_shear1;
    private double absMinFshear2_shearTotal;

    private double absMinFshearTotal;
    private double absMinFshearTotalCriFactor;
    private int absMinFshearTotalBoltId;
    private int absMinFshearTotalLcId;
    private double absMinFshearTotal_axial;
    private double absMinFshearTotal_shear1;
    private double absMinFshearTotal_shear2;

    private Double absMinStaticFaxial;
    private Double absMinStaticFaxial_shear1;
    private Double absMinStaticFaxial_shear2;
    private Double absMinStaticFaxial_shearTotal;
    private Integer absMinStaticFaxialBoltId;
    private Integer absMinStaticFaxialLcId;
    private Double absMinStaticFaxialCriFactor;

    private Double absMinStaticFshear1;
    private Double absMinStaticFshear1_axial;
    private Double absMinStaticFshear1_shear2;
    private Double absMinStaticFshear1_shearTotal;
    private Integer absMinStaticFshear1BoltId;
    private Integer absMinStaticFshear1LcId;
    private Double absMinStaticFshear1CriFactor;

    private Double absMinStaticFshear2;
    private Double absMinStaticFshear2_axial;
    private Double absMinStaticFshear2_shear1;
    private Double absMinStaticFshear2_shearTotal;
    private Integer absMinStaticFshear2BoltId;
    private Integer absMinStaticFshear2LcId;
    private Double absMinStaticFshear2CriFactor;

    private Double absMinStaticFshearTotal;
    private Double absMinStaticFshearTotal_axial;
    private Double absMinStaticFshearTotal_shear1;
    private Double absMinStaticFshearTotal_shear2;
    private Integer absMinStaticFshearTotalBoltId;
    private Integer absMinStaticFshearTotalLcId;
    private Double absMinStaticFshearTotalCriFactor;

    private Double absMinCrashFaxial;
    private Double absMinCrashFaxial_shear1;
    private Double absMinCrashFaxial_shear2;
    private Double absMinCrashFaxial_shearTotal;
    private Integer absMinCrashFaxialBoltId;
    private Integer absMinCrashFaxialLcId;
    private Double absMinCrashFaxialCriFactor;

    private Double absMinCrashFshear1;
    private Double absMinCrashFshear1_axial;
    private Double absMinCrashFshear1_shear2;
    private Double absMinCrashFshear1_shearTotal;
    private Integer absMinCrashFshear1BoltId;
    private Integer absMinCrashFshear1LcId;
    private Double absMinCrashFshear1CriFactor;

    private Double absMinCrashFshear2;
    private Double absMinCrashFshear2_axial;
    private Double absMinCrashFshear2_shear1;
    private Double absMinCrashFshear2_shearTotal;
    private Integer absMinCrashFshear2BoltId;
    private Integer absMinCrashFshear2LcId;
    private Double absMinCrashFshear2CriFactor;

    private Double absMinCrashFshearTotal;
    private Double absMinCrashFshearTotal_axial;
    private Double absMinCrashFshearTotal_shear1;
    private Double absMinCrashFshearTotal_shear2;
    private Integer absMinCrashFshearTotalBoltId;
    private Integer absMinCrashFshearTotalLcId;
    private Double absMinCrashFshearTotalCriFactor;

    private Double absMinFatigueFaxial;
    private Double absMinFatigueFaxial_shear1;
    private Double absMinFatigueFaxial_shear2;
    private Double absMinFatigueFaxial_shearTotal;
    private Integer absMinFatigueFaxialBoltId;
    private Integer absMinFatigueFaxialLcId;
    private Double absMinFatigueFaxialCriFactor;

    private Double absMinFatigueFshear1;
    private Double absMinFatigueFshear1_axial;
    private Double absMinFatigueFshear1_shear2;
    private Double absMinFatigueFshear1_shearTotal;
    private Integer absMinFatigueFshear1BoltId;
    private Integer absMinFatigueFshear1LcId;
    private Double absMinFatigueFshear1CriFactor;

    private Double absMinFatigueFshear2;
    private Double absMinFatigueFshear2_axial;
    private Double absMinFatigueFshear2_shear1;
    private Double absMinFatigueFshear2_shearTotal;
    private Integer absMinFatigueFshear2BoltId;
    private Integer absMinFatigueFshear2LcId;
    private Double absMinFatigueFshear2CriFactor;

    private Double absMinFatigueFshearTotal;
    private Double absMinFatigueFshearTotal_axial;
    private Double absMinFatigueFshearTotal_shear1;
    private Double absMinFatigueFshearTotal_shear2;
    private Integer absMinFatigueFshearTotalBoltId;
    private Integer absMinFatigueFshearTotalLcId;
    private Double absMinFatigueFshearTotalCriFactor;

    public CbarResults(String results) {
        // csv file with CBAR results:
        //       0     1 2 3   4         5       6         7          8         9          10           11
        // #elementsID,X,Y,Z,SystemID,PartName,Subcase,Simulation,ResultType,CBAR Shear1,CBAR Shear2,CBAR Axial Force
        String resultsSplitted[] = results.replaceAll(":", "-").split(",");

        partName = resultsSplitted[5];

        currentBoltId = Integer.valueOf(resultsSplitted[0]);
        currentLCId = Integer.valueOf(resultsSplitted[6].split(" ")[1]);
        currentCrifactor = 0.0;

        Integer[] currentBoltIdAndCurrentLCid = {currentBoltId, currentLCId};
        // if there is no results - N/A - set isNAasValue true and null for everything
        if (resultsSplitted[9].equals("N/A")) {
            isNAasValue = true;
            pairBoltIdLcIdForFshear1.put(currentBoltIdAndCurrentLCid, null);
            return;
        }
        if (resultsSplitted[10].equals("N/A")) {
            isNAasValue = true;
            pairBoltIdLcIdForFshear2.put(currentBoltIdAndCurrentLCid, null);
            return;
        }
        if (resultsSplitted[11].equals("N/A")) {
            isNAasValue = true;
            pairBoltIdLcIdForFaxial.put(currentBoltIdAndCurrentLCid, null);
            return;
        }

        // read forces
        pairBoltIdLcIdForFshear1.put(currentBoltIdAndCurrentLCid, Double.valueOf(resultsSplitted[9]));
        pairBoltIdLcIdForFshear2.put(currentBoltIdAndCurrentLCid, Double.valueOf(resultsSplitted[10]));
        pairBoltIdLcIdForFaxial.put(currentBoltIdAndCurrentLCid, Double.valueOf(resultsSplitted[11]));

        // calculate shear total
        currentShear1 = pairBoltIdLcIdForFshear1.get(currentBoltIdAndCurrentLCid);
        currentShear2 = pairBoltIdLcIdForFshear2.get(currentBoltIdAndCurrentLCid);
        currentShearTotal = Math.sqrt((currentShear1 * currentShear1) + (currentShear2 * currentShear2));

        pairBoltIdLcIdForFshearTotal.put(currentBoltIdAndCurrentLCid, currentShearTotal);
    }

    public void addResults(String results) {
        // csv file with CBAR results:
        //       0     1 2 3   4         5       6         7          8         9          10           11
        // #elementsID,X,Y,Z,SystemID,PartName,Subcase,Simulation,ResultType,CBAR Shear1,CBAR Shear2,CBAR Axial Force
        String resultsSplitted[] = results.replaceAll(":", "-").split(",");

        // add new bolt results only if results come from the same part
        if (partName.equals(resultsSplitted[5])) {

            currentBoltId = Integer.valueOf(resultsSplitted[0]);
            currentLCId = Integer.valueOf(resultsSplitted[6].split(" ")[1]);
            currentCrifactor = 0.0;

            Integer[] currentBoltIdAndCurrentLCid = {currentBoltId, currentLCId};
            // if there is no results - N/A - set isNAasValue true and null for everything
            if (resultsSplitted[9].equals("N/A")) {
                isNAasValue = true;
                pairBoltIdLcIdForFshear1.put(currentBoltIdAndCurrentLCid, null);
                return;
            }
            if (resultsSplitted[10].equals("N/A")) {
                isNAasValue = true;
                pairBoltIdLcIdForFshear2.put(currentBoltIdAndCurrentLCid, null);
                return;
            }
            if (resultsSplitted[11].equals("N/A")) {
                isNAasValue = true;
                pairBoltIdLcIdForFaxial.put(currentBoltIdAndCurrentLCid, null);
                return;
            }

            // read forces
            pairBoltIdLcIdForFshear1.put(currentBoltIdAndCurrentLCid, Double.valueOf(resultsSplitted[9]));
            pairBoltIdLcIdForFshear2.put(currentBoltIdAndCurrentLCid, Double.valueOf(resultsSplitted[10]));
            pairBoltIdLcIdForFaxial.put(currentBoltIdAndCurrentLCid, Double.valueOf(resultsSplitted[11]));

            // calculate shear total
            currentShear1 = pairBoltIdLcIdForFshear1.get(currentBoltIdAndCurrentLCid);
            currentShear2 = pairBoltIdLcIdForFshear2.get(currentBoltIdAndCurrentLCid);
            currentShearTotal = Math.sqrt((currentShear1 * currentShear1) + (currentShear2 * currentShear2));

            pairBoltIdLcIdForFshearTotal.put(currentBoltIdAndCurrentLCid, currentShearTotal);
        }
    }

    @Override
    public void calculateCriFactor() {
        if (isNAasValue) {
            // do nothing
            return;
        }
        for (Integer[] currentBoltIdAndCurrentLCid : pairBoltIdLcIdForFaxial.keySet()) {
            // calculate cri factor
            currentShearTotal = pairBoltIdLcIdForFshearTotal.get(currentBoltIdAndCurrentLCid);
            currentAxial = pairBoltIdLcIdForFaxial.get(currentBoltIdAndCurrentLCid);
            if (currentAxial < 0) {
                currentAxial = 0;
            }
            currentCrifactor = currentAxial + (currentShearTotal / frictionCoefficient);
            pairBoltIdLcIdForCriFactor.put(currentBoltIdAndCurrentLCid, currentCrifactor);
        }
    }

    @Override
    public void setMaxMinAbove0Min() {
        // if there was no results -> N/A -> set null for everything
        if (isNAasValue) {
            System.out.println("\t[!!] For part name: " + partName + " there are not results. Values have N/A - check it.");
            return;
        }

        // critical bolt
        Map<Integer[], Double> criFactors = findMax(pairBoltIdLcIdForCriFactor);
        for (Integer[] boltIdAndLcId : criFactors.keySet()) {
            criBoltId = boltIdAndLcId[0];
            criLCId = boltIdAndLcId[1];
            criFactor = criFactors.get(boltIdAndLcId);
            criFaxial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
            criFshear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
            criFshear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
            criFshearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
        }

        // max axial
        Map<Integer[], Double> maxBoltsIdFaxial = findMax(pairBoltIdLcIdForFaxial);
        for (Integer[] boltIdAndLcId : maxBoltsIdFaxial.keySet()) {
            maxFaxialBoltId = boltIdAndLcId[0];
            maxFaxialLcId = boltIdAndLcId[1];
            maxFaxial = maxBoltsIdFaxial.get(boltIdAndLcId);
            maxFaxialCriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

            maxFaxial_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
            maxFaxial_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
            maxFaxial_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
        }

        // max shear1
        Map<Integer[], Double> maxBoltsIdFshear1 = findMax(pairBoltIdLcIdForFshear1);
        for (Integer[] boltIdAndLcId : maxBoltsIdFshear1.keySet()) {
            maxFshear1BoltId = boltIdAndLcId[0];
            maxFshear1LcId = boltIdAndLcId[1];
            maxFshear1 = maxBoltsIdFshear1.get(boltIdAndLcId);
            maxFshear1CriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

            maxFshear1_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
            maxFshear1_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
            maxFshear1_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
        }

        // max shear2
        Map<Integer[], Double> maxBoltsIdFshear2 = findMax(pairBoltIdLcIdForFshear2);
        for (Integer[] boltIdAndLcId : maxBoltsIdFshear2.keySet()) {
            maxFshear2BoltId = boltIdAndLcId[0];
            maxFshear2LcId = boltIdAndLcId[1];
            maxFshear2 = maxBoltsIdFshear2.get(boltIdAndLcId);
            maxFshear2CriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

            maxFshear2_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
            maxFshear2_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
            maxFshear2_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
        }

        // max shear total
        Map<Integer[], Double> maxBoltsIdFshearTotal = findMax(pairBoltIdLcIdForFshearTotal);
        for (Integer[] boltIdAndLcId : maxBoltsIdFshearTotal.keySet()) {
            maxFshearTotalBoltId = boltIdAndLcId[0];
            maxFshearTotalLcId = boltIdAndLcId[1];
            maxFshearTotal = maxBoltsIdFshearTotal.get(boltIdAndLcId);
            maxFshearTotalCriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

            maxFshearTotal_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
            maxFshearTotal_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
            maxFshearTotal_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
        }
        // -------------------- min
        // min axial
        Map<Integer[], Double> minBoltsIdFaxial = findMin(pairBoltIdLcIdForFaxial);
        for (Integer[] boltIdAndLcId : minBoltsIdFaxial.keySet()) {
            minFaxialBoltId = boltIdAndLcId[0];
            minFaxialLcId = boltIdAndLcId[1];
            minFaxial = minBoltsIdFaxial.get(boltIdAndLcId);
            minFaxialCriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

            minFaxial_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
            minFaxial_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
            minFaxial_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
        }

        // min shear1
        Map<Integer[], Double> minBoltsIdFshear1 = findMin(pairBoltIdLcIdForFshear1);
        for (Integer[] boltIdAndLcId : minBoltsIdFshear1.keySet()) {
            minFshear1BoltId = boltIdAndLcId[0];
            minFshear1LcId = boltIdAndLcId[1];
            minFshear1 = minBoltsIdFshear1.get(boltIdAndLcId);
            minFshear1CriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

            minFshear1_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
            minFshear1_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
            minFshear1_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
        }

        // min shear2
        Map<Integer[], Double> minBoltsIdFshear2 = findMin(pairBoltIdLcIdForFshear2);
        for (Integer[] boltIdAndLcId : minBoltsIdFshear2.keySet()) {
            minFshear2BoltId = boltIdAndLcId[0];
            minFshear2LcId = boltIdAndLcId[1];
            minFshear2 = minBoltsIdFshear2.get(boltIdAndLcId);
            minFshear2CriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

            minFshear2_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
            minFshear2_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
            minFshear2_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
        }

        // min shear total
        Map<Integer[], Double> minBoltsIdFshearTotal = findMin(pairBoltIdLcIdForFshearTotal);
        for (Integer[] boltIdAndLcId : minBoltsIdFshearTotal.keySet()) {
            minFshearTotalBoltId = boltIdAndLcId[0];
            minFshearTotalLcId = boltIdAndLcId[1];
            minFshearTotal = minBoltsIdFshearTotal.get(boltIdAndLcId);
            minFshearTotalCriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

            minFshearTotal_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
            minFshearTotal_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
            minFshearTotal_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
        }

        // -------------F>0 min
        // F>0Min axial
        Map<Integer[], Double> absMinBoltsIdFaxial = findFabove0Min(pairBoltIdLcIdForFaxial);
        for (Integer[] boltIdAndLcId : absMinBoltsIdFaxial.keySet()) {
            absMinFaxialBoltId = boltIdAndLcId[0];
            absMinFaxialLcId = boltIdAndLcId[1];
            absMinFaxial = absMinBoltsIdFaxial.get(boltIdAndLcId);
            absMinFaxialCriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

            absMinFaxial_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
            absMinFaxial_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
            absMinFaxial_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
        }

        // F>0Min shear1
        Map<Integer[], Double> absMinBoltsIdFshear1 = findFabove0Min(pairBoltIdLcIdForFshear1);
        for (Integer[] boltIdAndLcId : absMinBoltsIdFshear1.keySet()) {
            absMinFshear1BoltId = boltIdAndLcId[0];
            absMinFshear1LcId = boltIdAndLcId[1];
            absMinFshear1 = absMinBoltsIdFshear1.get(boltIdAndLcId);
            absMinFshear1CriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

            absMinFshear1_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
            absMinFshear1_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
            absMinFshear1_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
        }

        // F>0Min shear2
        Map<Integer[], Double> absMinBoltsIdFshear2 = findFabove0Min(pairBoltIdLcIdForFshear2);
        for (Integer[] boltIdAndLcId : absMinBoltsIdFshear2.keySet()) {
            absMinFshear2BoltId = boltIdAndLcId[0];
            absMinFshear2LcId = boltIdAndLcId[1];
            absMinFshear2 = absMinBoltsIdFshear2.get(boltIdAndLcId);
            absMinFshear2CriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

            absMinFshear2_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
            absMinFshear2_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
            absMinFshear2_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
        }

        // F>0Min shear total
        Map<Integer[], Double> absMinBoltsIdFshearTotal = findFabove0Min(pairBoltIdLcIdForFshearTotal);
        for (Integer[] boltIdAndLcId : absMinBoltsIdFshearTotal.keySet()) {
            absMinFshearTotalBoltId = boltIdAndLcId[0];
            absMinFshearTotalLcId = boltIdAndLcId[1];
            absMinFshearTotal = absMinBoltsIdFshearTotal.get(boltIdAndLcId);
            absMinFshearTotalCriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

            absMinFshearTotal_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
            absMinFshearTotal_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
            absMinFshearTotal_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
        }

    }

    public void setCriLoadCaseBolt(LoadCaseType loadCaseType, List<Integer> listOfLC) {
        Map<Integer[], Double> boltIdAndLcIdForCriticalBoltForCurrentLC = new HashMap<>(1);

        switch (loadCaseType) {
            case STATIC:
                boltIdAndLcIdForCriticalBoltForCurrentLC = findMaxForListOfLC(pairBoltIdLcIdForCriFactor, listOfLC);
                if (boltIdAndLcIdForCriticalBoltForCurrentLC.isEmpty() || boltIdAndLcIdForCriticalBoltForCurrentLC.containsKey(null)) {
                    criStaticFaxial = null;
                    criStaticFshear1 = null;
                    criStaticFshear2 = null;
                    criStaticFshearTotal = null;
                    criStaticFactor = null;
                    criStaticBoltId = null;
                    criStaticLCId = null;
                    break;
                }

                for (Integer[] boltIdAndLcId : boltIdAndLcIdForCriticalBoltForCurrentLC.keySet()) {
                    criStaticBoltId = boltIdAndLcId[0];
                    criStaticLCId = boltIdAndLcId[1];
                    criStaticFactor = boltIdAndLcIdForCriticalBoltForCurrentLC.get(boltIdAndLcId);
                    criStaticFaxial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    criStaticFshear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    criStaticFshear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                    criStaticFshearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
            case CRASH:
                boltIdAndLcIdForCriticalBoltForCurrentLC = findMaxForListOfLC(pairBoltIdLcIdForCriFactor, listOfLC);
                if (boltIdAndLcIdForCriticalBoltForCurrentLC.isEmpty() || boltIdAndLcIdForCriticalBoltForCurrentLC.containsKey(null)) {
                    criCrashFaxial = null;
                    criCrashFshear1 = null;
                    criCrashFshear2 = null;
                    criCrashFshearTotal = null;
                    criCrashFactor = null;
                    criCrashBoltId = null;
                    criCrashLCId = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForCriticalBoltForCurrentLC.keySet()) {
                    criCrashBoltId = boltIdAndLcId[0];
                    criCrashLCId = boltIdAndLcId[1];
                    criCrashFactor = boltIdAndLcIdForCriticalBoltForCurrentLC.get(boltIdAndLcId);
                    criCrashFaxial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    criCrashFshear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    criCrashFshear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                    criCrashFshearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
            case FATIGUE:
                boltIdAndLcIdForCriticalBoltForCurrentLC = findMaxForListOfLC(pairBoltIdLcIdForCriFactor, listOfLC);
                if (boltIdAndLcIdForCriticalBoltForCurrentLC.isEmpty() || boltIdAndLcIdForCriticalBoltForCurrentLC.containsKey(null)) {
                    criFatigueFaxial = null;
                    criFatigueFshear1 = null;
                    criFatigueFshear2 = null;
                    criFatigueFshearTotal = null;
                    criFatigueFactor = null;
                    criFatigueBoltId = null;
                    criFatigueLCId = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForCriticalBoltForCurrentLC.keySet()) {
                    criFatigueBoltId = boltIdAndLcId[0];
                    criFatigueLCId = boltIdAndLcId[1];
                    criFatigueFactor = boltIdAndLcIdForCriticalBoltForCurrentLC.get(boltIdAndLcId);
                    criFatigueFaxial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    criFatigueFshear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    criFatigueFshear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                    criFatigueFshearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
        }
    }

    public void setMaxLoadCaseBolt(LoadCaseType loadCaseType, List<Integer> listOfLC) {
        Map<Integer[], Double> boltIdAndLcIdForMaxBoltForCurrentLC;
        // max axial
        switch (loadCaseType) {
            case STATIC:
                boltIdAndLcIdForMaxBoltForCurrentLC = findMaxForListOfLC(pairBoltIdLcIdForFaxial, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    maxStaticFaxialBoltId = null;
                    maxStaticFaxialLcId = null;
                    maxStaticFaxial = null;
                    maxStaticFaxialCriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    maxStaticFaxialBoltId = boltIdAndLcId[0];
                    maxStaticFaxialLcId = boltIdAndLcId[1];
                    maxStaticFaxial = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    maxStaticFaxialCriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    maxStaticFaxial_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    maxStaticFaxial_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                    maxStaticFaxial_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
            case CRASH:
                boltIdAndLcIdForMaxBoltForCurrentLC = findMaxForListOfLC(pairBoltIdLcIdForFaxial, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    maxCrashFaxialBoltId = null;
                    maxCrashFaxialLcId = null;
                    maxCrashFaxial = null;
                    maxCrashFaxialCriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    maxCrashFaxialBoltId = boltIdAndLcId[0];
                    maxCrashFaxialLcId = boltIdAndLcId[1];
                    maxCrashFaxial = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    maxCrashFaxialCriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    maxCrashFaxial_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    maxCrashFaxial_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                    maxCrashFaxial_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
            case FATIGUE:
                boltIdAndLcIdForMaxBoltForCurrentLC = findMaxForListOfLC(pairBoltIdLcIdForFaxial, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    maxFatigueFaxialBoltId = null;
                    maxFatigueFaxialLcId = null;
                    maxFatigueFaxial = null;
                    maxFatigueFaxialCriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    maxFatigueFaxialBoltId = boltIdAndLcId[0];
                    maxFatigueFaxialLcId = boltIdAndLcId[1];
                    maxFatigueFaxial = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    maxFatigueFaxialCriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    maxFatigueFaxial_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    maxFatigueFaxial_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                    maxFatigueFaxial_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
        }

        // max shear1
        switch (loadCaseType) {
            case STATIC:
                boltIdAndLcIdForMaxBoltForCurrentLC = findMaxForListOfLC(pairBoltIdLcIdForFshear1, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    maxStaticFshear1BoltId = null;
                    maxStaticFshear1LcId = null;
                    maxStaticFshear1 = null;
                    maxStaticFshear1CriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    maxStaticFshear1BoltId = boltIdAndLcId[0];
                    maxStaticFshear1LcId = boltIdAndLcId[1];
                    maxStaticFshear1 = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    maxStaticFshear1CriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    maxStaticFshear1_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    maxStaticFshear1_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                    maxStaticFshear1_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
            case CRASH:
                boltIdAndLcIdForMaxBoltForCurrentLC = findMaxForListOfLC(pairBoltIdLcIdForFshear1, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    maxCrashFshear1BoltId = null;
                    maxCrashFshear1LcId = null;
                    maxCrashFshear1 = null;
                    maxCrashFshear1CriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    maxCrashFshear1BoltId = boltIdAndLcId[0];
                    maxCrashFshear1LcId = boltIdAndLcId[1];
                    maxCrashFshear1 = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    maxCrashFshear1CriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    maxCrashFshear1_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    maxCrashFshear1_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                    maxCrashFshear1_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
            case FATIGUE:
                boltIdAndLcIdForMaxBoltForCurrentLC = findMaxForListOfLC(pairBoltIdLcIdForFshear1, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    maxFatigueFshear1BoltId = null;
                    maxFatigueFshear1LcId = null;
                    maxFatigueFshear1 = null;
                    maxFatigueFshear1CriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    maxFatigueFshear1BoltId = boltIdAndLcId[0];
                    maxFatigueFshear1LcId = boltIdAndLcId[1];
                    maxFatigueFshear1 = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    maxFatigueFshear1CriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    maxFatigueFshear1_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    maxFatigueFshear1_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                    maxFatigueFshear1_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
        }

        // max shear2
        switch (loadCaseType) {
            case STATIC:
                boltIdAndLcIdForMaxBoltForCurrentLC = findMaxForListOfLC(pairBoltIdLcIdForFshear2, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    maxStaticFshear2BoltId = null;
                    maxStaticFshear2LcId = null;
                    maxStaticFshear2 = null;
                    maxStaticFshear2CriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    maxStaticFshear2BoltId = boltIdAndLcId[0];
                    maxStaticFshear2LcId = boltIdAndLcId[1];
                    maxStaticFshear2 = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    maxStaticFshear2CriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    maxStaticFshear2_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    maxStaticFshear2_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    maxStaticFshear2_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
            case CRASH:
                boltIdAndLcIdForMaxBoltForCurrentLC = findMaxForListOfLC(pairBoltIdLcIdForFshear2, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    maxCrashFshear2BoltId = null;
                    maxCrashFshear2LcId = null;
                    maxCrashFshear2 = null;
                    maxCrashFshear2CriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    maxCrashFshear2BoltId = boltIdAndLcId[0];
                    maxCrashFshear2LcId = boltIdAndLcId[1];
                    maxCrashFshear2 = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    maxCrashFshear2CriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    maxCrashFshear2_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    maxCrashFshear2_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    maxCrashFshear2_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
            case FATIGUE:
                boltIdAndLcIdForMaxBoltForCurrentLC = findMaxForListOfLC(pairBoltIdLcIdForFshear2, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    maxFatigueFshear2BoltId = null;
                    maxFatigueFshear2LcId = null;
                    maxFatigueFshear2 = null;
                    maxFatigueFshear2CriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    maxFatigueFshear2BoltId = boltIdAndLcId[0];
                    maxFatigueFshear2LcId = boltIdAndLcId[1];
                    maxFatigueFshear2 = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    maxFatigueFshear2CriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    maxFatigueFshear2_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    maxFatigueFshear2_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    maxFatigueFshear2_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
        }

        // max shear total
        switch (loadCaseType) {
            case STATIC:
                boltIdAndLcIdForMaxBoltForCurrentLC = findMaxForListOfLC(pairBoltIdLcIdForFshearTotal, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    maxStaticFshearTotalBoltId = null;
                    maxStaticFshearTotalLcId = null;
                    maxStaticFshearTotal = null;
                    maxStaticFshearTotalCriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    maxStaticFshearTotalBoltId = boltIdAndLcId[0];
                    maxStaticFshearTotalLcId = boltIdAndLcId[1];
                    maxStaticFshearTotal = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    maxStaticFshearTotalCriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    maxStaticFshearTotal_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    maxStaticFshearTotal_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    maxStaticFshearTotal_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                }
                break;
            case CRASH:
                boltIdAndLcIdForMaxBoltForCurrentLC = findMaxForListOfLC(pairBoltIdLcIdForFshearTotal, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    maxCrashFshearTotalBoltId = null;
                    maxCrashFshearTotalLcId = null;
                    maxCrashFshearTotal = null;
                    maxCrashFshearTotalCriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    maxCrashFshearTotalBoltId = boltIdAndLcId[0];
                    maxCrashFshearTotalLcId = boltIdAndLcId[1];
                    maxCrashFshearTotal = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    maxCrashFshearTotalCriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    maxCrashFshearTotal_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    maxCrashFshearTotal_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    maxCrashFshearTotal_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                }
                break;
            case FATIGUE:
                boltIdAndLcIdForMaxBoltForCurrentLC = findMaxForListOfLC(pairBoltIdLcIdForFshearTotal, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    maxFatigueFshearTotalBoltId = null;
                    maxFatigueFshearTotalLcId = null;
                    maxFatigueFshearTotal = null;
                    maxFatigueFshearTotalCriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    maxFatigueFshearTotalBoltId = boltIdAndLcId[0];
                    maxFatigueFshearTotalLcId = boltIdAndLcId[1];
                    maxFatigueFshearTotal = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    maxFatigueFshearTotalCriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    maxFatigueFshearTotal_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    maxFatigueFshearTotal_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    maxFatigueFshearTotal_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                }
                break;
        }

        // -------------------------- min
        // min axial
        switch (loadCaseType) {
            case STATIC:
                boltIdAndLcIdForMaxBoltForCurrentLC = findMinForListOfLC(pairBoltIdLcIdForFaxial, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    minStaticFaxialBoltId = null;
                    minStaticFaxialLcId = null;
                    minStaticFaxial = null;
                    minStaticFaxialCriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    minStaticFaxialBoltId = boltIdAndLcId[0];
                    minStaticFaxialLcId = boltIdAndLcId[1];
                    minStaticFaxial = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    minStaticFaxialCriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    minStaticFaxial_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    minStaticFaxial_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                    minStaticFaxial_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
            case CRASH:
                boltIdAndLcIdForMaxBoltForCurrentLC = findMinForListOfLC(pairBoltIdLcIdForFaxial, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    minCrashFaxialBoltId = null;
                    minCrashFaxialLcId = null;
                    minCrashFaxial = null;
                    minCrashFaxialCriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    minCrashFaxialBoltId = boltIdAndLcId[0];
                    minCrashFaxialLcId = boltIdAndLcId[1];
                    minCrashFaxial = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    minCrashFaxialCriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    minCrashFaxial_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    minCrashFaxial_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                    minCrashFaxial_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
            case FATIGUE:
                boltIdAndLcIdForMaxBoltForCurrentLC = findMinForListOfLC(pairBoltIdLcIdForFaxial, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    minFatigueFaxialBoltId = null;
                    minFatigueFaxialLcId = null;
                    minFatigueFaxial = null;
                    minFatigueFaxialCriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    minFatigueFaxialBoltId = boltIdAndLcId[0];
                    minFatigueFaxialLcId = boltIdAndLcId[1];
                    minFatigueFaxial = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    minFatigueFaxialCriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    minFatigueFaxial_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    minFatigueFaxial_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                    minFatigueFaxial_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
        }

        // min shear1
        switch (loadCaseType) {
            case STATIC:
                boltIdAndLcIdForMaxBoltForCurrentLC = findMinForListOfLC(pairBoltIdLcIdForFshear1, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    minStaticFshear1BoltId = null;
                    minStaticFshear1LcId = null;
                    minStaticFshear1 = null;
                    minStaticFshear1CriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    minStaticFshear1BoltId = boltIdAndLcId[0];
                    minStaticFshear1LcId = boltIdAndLcId[1];
                    minStaticFshear1 = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    minStaticFshear1CriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    minStaticFshear1_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    minStaticFshear1_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                    minStaticFshear1_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
            case CRASH:
                boltIdAndLcIdForMaxBoltForCurrentLC = findMinForListOfLC(pairBoltIdLcIdForFshear1, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    minCrashFshear1BoltId = null;
                    minCrashFshear1LcId = null;
                    minCrashFshear1 = null;
                    minCrashFshear1CriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    minCrashFshear1BoltId = boltIdAndLcId[0];
                    minCrashFshear1LcId = boltIdAndLcId[1];
                    minCrashFshear1 = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    minCrashFshear1CriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    minCrashFshear1_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    minCrashFshear1_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                    minCrashFshear1_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
            case FATIGUE:
                boltIdAndLcIdForMaxBoltForCurrentLC = findMinForListOfLC(pairBoltIdLcIdForFshear1, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    minFatigueFshear1BoltId = null;
                    minFatigueFshear1LcId = null;
                    minFatigueFshear1 = null;
                    minFatigueFshear1CriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    minFatigueFshear1BoltId = boltIdAndLcId[0];
                    minFatigueFshear1LcId = boltIdAndLcId[1];
                    minFatigueFshear1 = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    minFatigueFshear1CriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    minFatigueFshear1_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    minFatigueFshear1_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                    minFatigueFshear1_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
        }

        // min shear2
        switch (loadCaseType) {
            case STATIC:
                boltIdAndLcIdForMaxBoltForCurrentLC = findMinForListOfLC(pairBoltIdLcIdForFshear2, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    minStaticFshear2BoltId = null;
                    minStaticFshear2LcId = null;
                    minStaticFshear2 = null;
                    minStaticFshear2CriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    minStaticFshear2BoltId = boltIdAndLcId[0];
                    minStaticFshear2LcId = boltIdAndLcId[1];
                    minStaticFshear2 = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    minStaticFshear2CriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    minStaticFshear2_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    minStaticFshear2_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    minStaticFshear2_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
            case CRASH:
                boltIdAndLcIdForMaxBoltForCurrentLC = findMinForListOfLC(pairBoltIdLcIdForFshear2, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    minCrashFshear2BoltId = null;
                    minCrashFshear2LcId = null;
                    minCrashFshear2 = null;
                    minCrashFshear2CriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    minCrashFshear2BoltId = boltIdAndLcId[0];
                    minCrashFshear2LcId = boltIdAndLcId[1];
                    minCrashFshear2 = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    minCrashFshear2CriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    minCrashFshear2_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    minCrashFshear2_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    minCrashFshear2_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
            case FATIGUE:
                boltIdAndLcIdForMaxBoltForCurrentLC = findMinForListOfLC(pairBoltIdLcIdForFshear2, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    minFatigueFshear2BoltId = null;
                    minFatigueFshear2LcId = null;
                    minFatigueFshear2 = null;
                    minFatigueFshear2CriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    minFatigueFshear2BoltId = boltIdAndLcId[0];
                    minFatigueFshear2LcId = boltIdAndLcId[1];
                    minFatigueFshear2 = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    minFatigueFshear2CriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    minFatigueFshear2_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    minFatigueFshear2_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    minFatigueFshear2_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
        }

        // min shear total
        switch (loadCaseType) {
            case STATIC:
                boltIdAndLcIdForMaxBoltForCurrentLC = findMinForListOfLC(pairBoltIdLcIdForFshearTotal, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    minStaticFshearTotalBoltId = null;
                    minStaticFshearTotalLcId = null;
                    minStaticFshearTotal = null;
                    minStaticFshearTotalCriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    minStaticFshearTotalBoltId = boltIdAndLcId[0];
                    minStaticFshearTotalLcId = boltIdAndLcId[1];
                    minStaticFshearTotal = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    minStaticFshearTotalCriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    minStaticFshearTotal_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    minStaticFshearTotal_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    minStaticFshearTotal_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                }
                break;
            case CRASH:
                boltIdAndLcIdForMaxBoltForCurrentLC = findMinForListOfLC(pairBoltIdLcIdForFshearTotal, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    minCrashFshearTotalBoltId = null;
                    minCrashFshearTotalLcId = null;
                    minCrashFshearTotal = null;
                    minCrashFshearTotalCriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    minCrashFshearTotalBoltId = boltIdAndLcId[0];
                    minCrashFshearTotalLcId = boltIdAndLcId[1];
                    minCrashFshearTotal = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    minCrashFshearTotalCriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    minCrashFshearTotal_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    minCrashFshearTotal_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    minCrashFshearTotal_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                }
                break;
            case FATIGUE:
                boltIdAndLcIdForMaxBoltForCurrentLC = findMinForListOfLC(pairBoltIdLcIdForFshearTotal, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    minFatigueFshearTotalBoltId = null;
                    minFatigueFshearTotalLcId = null;
                    minFatigueFshearTotal = null;
                    minFatigueFshearTotalCriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    minFatigueFshearTotalBoltId = boltIdAndLcId[0];
                    minFatigueFshearTotalLcId = boltIdAndLcId[1];
                    minFatigueFshearTotal = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    minFatigueFshearTotalCriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    minFatigueFshearTotal_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    minFatigueFshearTotal_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    minFatigueFshearTotal_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                }
                break;
        }

        // -------------------------- absMin
        // absMin axial
        switch (loadCaseType) {
            case STATIC:
                boltIdAndLcIdForMaxBoltForCurrentLC = findFabove0MinForListOfLC(pairBoltIdLcIdForFaxial, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    absMinStaticFaxialBoltId = null;
                    absMinStaticFaxialLcId = null;
                    absMinStaticFaxial = null;
                    absMinStaticFaxialCriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    absMinStaticFaxialBoltId = boltIdAndLcId[0];
                    absMinStaticFaxialLcId = boltIdAndLcId[1];
                    absMinStaticFaxial = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    absMinStaticFaxialCriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    absMinStaticFaxial_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    absMinStaticFaxial_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                    absMinStaticFaxial_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
            case CRASH:
                boltIdAndLcIdForMaxBoltForCurrentLC = findFabove0MinForListOfLC(pairBoltIdLcIdForFaxial, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    absMinCrashFaxialBoltId = null;
                    absMinCrashFaxialLcId = null;
                    absMinCrashFaxial = null;
                    absMinCrashFaxialCriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    absMinCrashFaxialBoltId = boltIdAndLcId[0];
                    absMinCrashFaxialLcId = boltIdAndLcId[1];
                    absMinCrashFaxial = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    absMinCrashFaxialCriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    absMinCrashFaxial_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    absMinCrashFaxial_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                    absMinCrashFaxial_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
            case FATIGUE:
                boltIdAndLcIdForMaxBoltForCurrentLC = findFabove0MinForListOfLC(pairBoltIdLcIdForFaxial, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    absMinFatigueFaxialBoltId = null;
                    absMinFatigueFaxialLcId = null;
                    absMinFatigueFaxial = null;
                    absMinFatigueFaxialCriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    absMinFatigueFaxialBoltId = boltIdAndLcId[0];
                    absMinFatigueFaxialLcId = boltIdAndLcId[1];
                    absMinFatigueFaxial = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    absMinFatigueFaxialCriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    absMinFatigueFaxial_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    absMinFatigueFaxial_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                    absMinFatigueFaxial_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
        }

        // absMin shear1
        switch (loadCaseType) {
            case STATIC:
                boltIdAndLcIdForMaxBoltForCurrentLC = findFabove0MinForListOfLC(pairBoltIdLcIdForFshear1, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    absMinStaticFshear1BoltId = null;
                    absMinStaticFshear1LcId = null;
                    absMinStaticFshear1 = null;
                    absMinStaticFshear1CriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    absMinStaticFshear1BoltId = boltIdAndLcId[0];
                    absMinStaticFshear1LcId = boltIdAndLcId[1];
                    absMinStaticFshear1 = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    absMinStaticFshear1CriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    absMinStaticFshear1_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    absMinStaticFshear1_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                    absMinStaticFshear1_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
            case CRASH:
                boltIdAndLcIdForMaxBoltForCurrentLC = findFabove0MinForListOfLC(pairBoltIdLcIdForFshear1, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    absMinCrashFshear1BoltId = null;
                    absMinCrashFshear1LcId = null;
                    absMinCrashFshear1 = null;
                    absMinCrashFshear1CriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    absMinCrashFshear1BoltId = boltIdAndLcId[0];
                    absMinCrashFshear1LcId = boltIdAndLcId[1];
                    absMinCrashFshear1 = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    absMinCrashFshear1CriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    absMinCrashFshear1_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    absMinCrashFshear1_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                    absMinCrashFshear1_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
            case FATIGUE:
                boltIdAndLcIdForMaxBoltForCurrentLC = findFabove0MinForListOfLC(pairBoltIdLcIdForFshear1, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    absMinFatigueFshear1BoltId = null;
                    absMinFatigueFshear1LcId = null;
                    absMinFatigueFshear1 = null;
                    absMinFatigueFshear1CriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    absMinFatigueFshear1BoltId = boltIdAndLcId[0];
                    absMinFatigueFshear1LcId = boltIdAndLcId[1];
                    absMinFatigueFshear1 = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    absMinFatigueFshear1CriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    absMinFatigueFshear1_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    absMinFatigueFshear1_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                    absMinFatigueFshear1_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
        }

        // absMin shear2
        switch (loadCaseType) {
            case STATIC:
                boltIdAndLcIdForMaxBoltForCurrentLC = findFabove0MinForListOfLC(pairBoltIdLcIdForFshear2, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    absMinStaticFshear2BoltId = null;
                    absMinStaticFshear2LcId = null;
                    absMinStaticFshear2 = null;
                    absMinStaticFshear2CriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    absMinStaticFshear2BoltId = boltIdAndLcId[0];
                    absMinStaticFshear2LcId = boltIdAndLcId[1];
                    absMinStaticFshear2 = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    absMinStaticFshear2CriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    absMinStaticFshear2_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    absMinStaticFshear2_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    absMinStaticFshear2_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
            case CRASH:
                boltIdAndLcIdForMaxBoltForCurrentLC = findFabove0MinForListOfLC(pairBoltIdLcIdForFshear2, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    absMinCrashFshear2BoltId = null;
                    absMinCrashFshear2LcId = null;
                    absMinCrashFshear2 = null;
                    absMinCrashFshear2CriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    absMinCrashFshear2BoltId = boltIdAndLcId[0];
                    absMinCrashFshear2LcId = boltIdAndLcId[1];
                    absMinCrashFshear2 = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    absMinCrashFshear2CriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    absMinCrashFshear2_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    absMinCrashFshear2_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    absMinCrashFshear2_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
            case FATIGUE:
                boltIdAndLcIdForMaxBoltForCurrentLC = findFabove0MinForListOfLC(pairBoltIdLcIdForFshear2, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    absMinFatigueFshear2BoltId = null;
                    absMinFatigueFshear2LcId = null;
                    absMinFatigueFshear2 = null;
                    absMinFatigueFshear2CriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    absMinFatigueFshear2BoltId = boltIdAndLcId[0];
                    absMinFatigueFshear2LcId = boltIdAndLcId[1];
                    absMinFatigueFshear2 = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    absMinFatigueFshear2CriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    absMinFatigueFshear2_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    absMinFatigueFshear2_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    absMinFatigueFshear2_shearTotal = pairBoltIdLcIdForFshearTotal.get(boltIdAndLcId);
                }
                break;
        }

        // absMin shear total
        switch (loadCaseType) {
            case STATIC:
                boltIdAndLcIdForMaxBoltForCurrentLC = findFabove0MinForListOfLC(pairBoltIdLcIdForFshearTotal, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    absMinStaticFshearTotalBoltId = null;
                    absMinStaticFshearTotalLcId = null;
                    absMinStaticFshearTotal = null;
                    absMinStaticFshearTotalCriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    absMinStaticFshearTotalBoltId = boltIdAndLcId[0];
                    absMinStaticFshearTotalLcId = boltIdAndLcId[1];
                    absMinStaticFshearTotal = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    absMinStaticFshearTotalCriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    absMinStaticFshearTotal_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    absMinStaticFshearTotal_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    absMinStaticFshearTotal_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                }
                break;
            case CRASH:
                boltIdAndLcIdForMaxBoltForCurrentLC = findFabove0MinForListOfLC(pairBoltIdLcIdForFshearTotal, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    absMinCrashFshearTotalBoltId = null;
                    absMinCrashFshearTotalLcId = null;
                    absMinCrashFshearTotal = null;
                    absMinCrashFshearTotalCriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    absMinCrashFshearTotalBoltId = boltIdAndLcId[0];
                    absMinCrashFshearTotalLcId = boltIdAndLcId[1];
                    absMinCrashFshearTotal = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    absMinCrashFshearTotalCriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    absMinCrashFshearTotal_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    absMinCrashFshearTotal_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    absMinCrashFshearTotal_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                }
                break;
            case FATIGUE:
                boltIdAndLcIdForMaxBoltForCurrentLC = findFabove0MinForListOfLC(pairBoltIdLcIdForFshearTotal, listOfLC);
                if (boltIdAndLcIdForMaxBoltForCurrentLC.isEmpty() || boltIdAndLcIdForMaxBoltForCurrentLC.containsKey(null)) {
                    absMinFatigueFshearTotalBoltId = null;
                    absMinFatigueFshearTotalLcId = null;
                    absMinFatigueFshearTotal = null;
                    absMinFatigueFshearTotalCriFactor = null;
                    break;
                }
                for (Integer[] boltIdAndLcId : boltIdAndLcIdForMaxBoltForCurrentLC.keySet()) {
                    absMinFatigueFshearTotalBoltId = boltIdAndLcId[0];
                    absMinFatigueFshearTotalLcId = boltIdAndLcId[1];
                    absMinFatigueFshearTotal = boltIdAndLcIdForMaxBoltForCurrentLC.get(boltIdAndLcId);
                    absMinFatigueFshearTotalCriFactor = pairBoltIdLcIdForCriFactor.get(boltIdAndLcId);

                    absMinFatigueFshearTotal_axial = pairBoltIdLcIdForFaxial.get(boltIdAndLcId);
                    absMinFatigueFshearTotal_shear1 = pairBoltIdLcIdForFshear1.get(boltIdAndLcId);
                    absMinFatigueFshearTotal_shear2 = pairBoltIdLcIdForFshear2.get(boltIdAndLcId);
                }
                break;
        }
    }

    private Map<Integer[], Double> findFabove0Min(Map<Integer[], Double> boltIdAndLcIdForValues) {
        Map<Integer[], Double> boltIdAndLcIdForAbsMinValue = new HashMap<>(1);

        Double absMinValue = 1e50;
        Integer[] boltIdForAbsMinValue = null;
        boolean didnotFoundValue = true;

        for (Integer[] boltIdAndLcId : boltIdAndLcIdForValues.keySet()) {
            if (boltIdAndLcIdForValues.get(boltIdAndLcId) < absMinValue && boltIdAndLcIdForValues.get(boltIdAndLcId) > 0) {
                boltIdForAbsMinValue = boltIdAndLcId;
                absMinValue = boltIdAndLcIdForValues.get(boltIdAndLcId);
                didnotFoundValue = false;
            }
        }
        if (didnotFoundValue) {
            for (Integer[] boltIdAndLcId : boltIdAndLcIdForValues.keySet()) {
                if (boltIdAndLcIdForValues.get(boltIdAndLcId) < absMinValue) {
                    boltIdForAbsMinValue = boltIdAndLcId;
                    absMinValue = boltIdAndLcIdForValues.get(boltIdAndLcId);
                }
            }
            absMinValue = 0.0;
            boltIdAndLcIdForAbsMinValue.put(boltIdForAbsMinValue, absMinValue);
            return boltIdAndLcIdForAbsMinValue;
        }

        boltIdAndLcIdForAbsMinValue.put(boltIdForAbsMinValue, absMinValue);

        return boltIdAndLcIdForAbsMinValue;
    }

    private Map<Integer[], Double> findMin(Map<Integer[], Double> boltIdAndLcIdForValues) {
        Map<Integer[], Double> boltIdAndLcIdForMinValue = new HashMap<>(1);

        Double minValue = 1e50;
        Integer[] boltIdForMinValue = null;

        for (Integer[] boltIdAndLcId : boltIdAndLcIdForValues.keySet()) {
            if (boltIdAndLcIdForValues.get(boltIdAndLcId) <= minValue) {
                boltIdForMinValue = boltIdAndLcId;
                minValue = boltIdAndLcIdForValues.get(boltIdAndLcId);
            }
        }
        boltIdAndLcIdForMinValue.put(boltIdForMinValue, minValue);

        return boltIdAndLcIdForMinValue;
    }

    private Map<Integer[], Double> findMax(Map<Integer[], Double> boltIdAndLcIdForValues) {
        Map<Integer[], Double> boltIdAndLcIdForMaxValue = new HashMap<>(1);

        Double maxValue = -1e50; // with Double.MIN_VALUE was null pointer exception
        Integer[] boltIdForMaxValue = null;

        for (Integer[] boltIdAndLcId : boltIdAndLcIdForValues.keySet()) {
            // must add || boltIdAndLcIdForValues.get(boltIdAndLcId) == 0.0 because sometime, results are 0.000e00
            // || boltIdAndLcIdForValues.get(boltIdAndLcId) == 0.0
            if (boltIdAndLcIdForValues.get(boltIdAndLcId) >= maxValue) {
                boltIdForMaxValue = boltIdAndLcId;
                maxValue = boltIdAndLcIdForValues.get(boltIdAndLcId);
            }
        }

        boltIdAndLcIdForMaxValue.put(boltIdForMaxValue, maxValue);

        return boltIdAndLcIdForMaxValue;
    }

    private Map<Integer[], Double> findFabove0MinForListOfLC(Map<Integer[], Double> boltIdAndLcIdForValues, List<Integer> listOfLC) {
        // if given listOfLc is empty return null
        if (listOfLC.isEmpty()) {
            return null;
        }

        Map<Integer[], Double> boltIdAndLcIdForAbsMinValue = new HashMap<>(1);

        Double absMinValue = 1e50;
        Integer[] boltIdForAbsMinValue = null;
        boolean didnotFoundValue = true;

        for (Integer[] boltIdAndLcId : boltIdAndLcIdForValues.keySet()) {
            if (!listOfLC.contains(boltIdAndLcId[1])) {
                continue;
            }
            if (boltIdAndLcIdForValues.get(boltIdAndLcId) < absMinValue && boltIdAndLcIdForValues.get(boltIdAndLcId) > 0) {
                boltIdForAbsMinValue = boltIdAndLcId;
                absMinValue = boltIdAndLcIdForValues.get(boltIdAndLcId);
                didnotFoundValue = false;
            }
        }
        if (didnotFoundValue) {
            for (Integer[] boltIdAndLcId : boltIdAndLcIdForValues.keySet()) {
                if (boltIdAndLcIdForValues.get(boltIdAndLcId) < absMinValue) {
                    boltIdForAbsMinValue = boltIdAndLcId;
                    absMinValue = boltIdAndLcIdForValues.get(boltIdAndLcId);
                }
            }
            absMinValue = 0.0;
            boltIdAndLcIdForAbsMinValue.put(boltIdForAbsMinValue, absMinValue);
            return boltIdAndLcIdForAbsMinValue;
        }

        boltIdAndLcIdForAbsMinValue.put(boltIdForAbsMinValue, absMinValue);

        return boltIdAndLcIdForAbsMinValue;
    }

    private Map<Integer[], Double> findMinForListOfLC(Map<Integer[], Double> boltIdAndLcIdForValues, List<Integer> listOfLC) {
        // if given listOfLc is empty return null
        if (listOfLC.isEmpty()) {
            return null;
        }

        Map<Integer[], Double> boltIdAndLcIdForMinValue = new HashMap<>(1);

        Double minValue = 1e50; // with Double.MIN_VALUE was null pointer exception
        Integer[] boltIdForMinValue = null;

        for (Integer[] boltIdAndLcId : boltIdAndLcIdForValues.keySet()) {
            if (!listOfLC.contains(boltIdAndLcId[1])) {
                continue;
            }
            if (boltIdAndLcIdForValues.get(boltIdAndLcId) <= minValue) {
                boltIdForMinValue = boltIdAndLcId;
                minValue = boltIdAndLcIdForValues.get(boltIdAndLcId);
            }
        }

        boltIdAndLcIdForMinValue.put(boltIdForMinValue, minValue);

        return boltIdAndLcIdForMinValue;
    }

    private Map<Integer[], Double> findMaxForListOfLC(Map<Integer[], Double> boltIdAndLcIdForValues, List<Integer> listOfLC) {
        // if given listOfLc is empty return null
        if (listOfLC.isEmpty()) {
            return null;
        }

        Map<Integer[], Double> boltIdAndLcIdForMaxValue = new HashMap<>(1);

        Double maxValue = -1e50; // with Double.MIN_VALUE was null pointer exception
        Integer[] boltIdForMaxValue = null;

        for (Integer[] boltIdAndLcId : boltIdAndLcIdForValues.keySet()) {
            if (!listOfLC.contains(boltIdAndLcId[1])) {
                continue;
            }
            if (boltIdAndLcIdForValues.get(boltIdAndLcId) >= maxValue) {
                boltIdForMaxValue = boltIdAndLcId;
                maxValue = boltIdAndLcIdForValues.get(boltIdAndLcId);
            }
        }

        boltIdAndLcIdForMaxValue.put(boltIdForMaxValue, maxValue);

        return boltIdAndLcIdForMaxValue;
    }


    public String getPartName() {
        return partName;
    }

    public Integer getCurrentBoltId() {
        return currentBoltId;
    }

    public Integer getCurrentLCId() {
        return currentLCId;
    }

    public double getCurrentCrifactor() {
        return currentCrifactor;
    }

    public double getCurrentShearTotal() {
        return currentShearTotal;
    }

    public double getCurrentShear1() {
        return currentShear1;
    }

    public double getCurrentShear2() {
        return currentShear2;
    }

    public double getCurrentAxial() {
        return currentAxial;
    }

    public Map<Integer[], Double> getPairBoltIdLcIdForFaxial() {
        return pairBoltIdLcIdForFaxial;
    }

    public Map<Integer[], Double> getPairBoltIdLcIdForFshear1() {
        return pairBoltIdLcIdForFshear1;
    }

    public Map<Integer[], Double> getPairBoltIdLcIdForFshear2() {
        return pairBoltIdLcIdForFshear2;
    }

    public Map<Integer[], Double> getPairBoltIdLcIdForFshearTotal() {
        return pairBoltIdLcIdForFshearTotal;
    }

    public Map<Integer[], Double> getPairBoltIdLcIdForCriFactor() {
        return pairBoltIdLcIdForCriFactor;
    }

    public double getCriFaxial() {
        return criFaxial;
    }

    public double getCriFshear1() {
        return criFshear1;
    }

    public double getCriFshear2() {
        return criFshear2;
    }

    public double getCriFshearTotal() {
        return criFshearTotal;
    }

    public double getCriFactor() {
        return criFactor;
    }

    public int getCriBoltId() {
        return criBoltId;
    }

    public int getCriLCId() {
        return criLCId;
    }

    public double getMaxFaxial() {
        return maxFaxial;
    }

    public int getMaxFaxialBoltId() {
        return maxFaxialBoltId;
    }

    public int getMaxFaxialLcId() {
        return maxFaxialLcId;
    }

    public double getMaxFshear1() {
        return maxFshear1;
    }

    public int getMaxFshear1BoltId() {
        return maxFshear1BoltId;
    }

    public int getMaxFshear1LcId() {
        return maxFshear1LcId;
    }

    public double getMaxFshear2() {
        return maxFshear2;
    }

    public int getMaxFshear2BoltId() {
        return maxFshear2BoltId;
    }

    public int getMaxFshear2LcId() {
        return maxFshear2LcId;
    }

    public double getMaxFshearTotal() {
        return maxFshearTotal;
    }

    public int getMaxFshearTotalBoltId() {
        return maxFshearTotalBoltId;
    }

    public int getMaxFshearTotalLcId() {
        return maxFshearTotalLcId;
    }

    public double getMaxFaxialCriFactor() {
        return maxFaxialCriFactor;
    }

    public double getMaxFshear1CriFactor() {
        return maxFshear1CriFactor;
    }

    public double getMaxFshear2CriFactor() {
        return maxFshear2CriFactor;
    }

    public double getMaxFshearTotalCriFactor() {
        return maxFshearTotalCriFactor;
    }

    public double getMinFaxial() {
        return minFaxial;
    }

    public int getMinFaxialBoltId() {
        return minFaxialBoltId;
    }

    public int getMinFaxialLcId() {
        return minFaxialLcId;
    }

    public double getMinFshear1() {
        return minFshear1;
    }

    public int getMinFshear1BoltId() {
        return minFshear1BoltId;
    }

    public int getMinFshear1LcId() {
        return minFshear1LcId;
    }

    public double getMinFshear2() {
        return minFshear2;
    }

    public int getMinFshear2BoltId() {
        return minFshear2BoltId;
    }

    public int getMinFshear2LcId() {
        return minFshear2LcId;
    }

    public double getMinFshearTotal() {
        return minFshearTotal;
    }

    public int getMinFshearTotalBoltId() {
        return minFshearTotalBoltId;
    }

    public int getMinFshearTotalLcId() {
        return minFshearTotalLcId;
    }

    public double getMinFaxialCriFactor() {
        return minFaxialCriFactor;
    }

    public double getMinFshear1CriFactor() {
        return minFshear1CriFactor;
    }

    public double getMinFshear2CriFactor() {
        return minFshear2CriFactor;
    }

    public double getMinFshearTotalCriFactor() {
        return minFshearTotalCriFactor;
    }

    public double getAbsMinFaxial() {
        return absMinFaxial;
    }

    public int getAbsMinFaxialBoltId() {
        return absMinFaxialBoltId;
    }

    public int getAbsMinFaxialLcId() {
        return absMinFaxialLcId;
    }

    public double getAbsMinFshear1() {
        return absMinFshear1;
    }

    public int getAbsMinFshear1BoltId() {
        return absMinFshear1BoltId;
    }

    public int getAbsMinFshear1LcId() {
        return absMinFshear1LcId;
    }

    public double getAbsMinFshear2() {
        return absMinFshear2;
    }

    public int getAbsMinFshear2BoltId() {
        return absMinFshear2BoltId;
    }

    public int getAbsMinFshear2LcId() {
        return absMinFshear2LcId;
    }

    public double getAbsMinFshearTotal() {
        return absMinFshearTotal;
    }

    public int getAbsMinFshearTotalBoltId() {
        return absMinFshearTotalBoltId;
    }

    public int getAbsMinFshearTotalLcId() {
        return absMinFshearTotalLcId;
    }

    public double getAbsMinFaxialCriFactor() {
        return absMinFaxialCriFactor;
    }

    public double getAbsMinFshear1CriFactor() {
        return absMinFshear1CriFactor;
    }

    public double getAbsMinFshear2CriFactor() {
        return absMinFshear2CriFactor;
    }

    public double getAbsMinFshearTotalCriFactor() {
        return absMinFshearTotalCriFactor;
    }

    public Double getCriStaticFaxial() {
        return criStaticFaxial;
    }

    public Double getCriStaticFshear1() {
        return criStaticFshear1;
    }

    public Double getCriStaticFshear2() {
        return criStaticFshear2;
    }

    public Double getCriStaticFshearTotal() {
        return criStaticFshearTotal;
    }

    public Double getCriStaticFactor() {
        return criStaticFactor;
    }

    public Integer getCriStaticBoltId() {
        return criStaticBoltId;
    }

    public Integer getCriStaticLCId() {
        return criStaticLCId;
    }

    public Double getCriCrashFaxial() {
        return criCrashFaxial;
    }

    public Double getCriCrashFshear1() {
        return criCrashFshear1;
    }

    public Double getCriCrashFshear2() {
        return criCrashFshear2;
    }

    public Double getCriCrashFshearTotal() {
        return criCrashFshearTotal;
    }

    public Double getCriCrashFactor() {
        return criCrashFactor;
    }

    public Integer getCriCrashBoltId() {
        return criCrashBoltId;
    }

    public Integer getCriCrashLCId() {
        return criCrashLCId;
    }

    public Double getCriFatigueFaxial() {
        return criFatigueFaxial;
    }

    public Double getCriFatigueFshear1() {
        return criFatigueFshear1;
    }

    public Double getCriFatigueFshear2() {
        return criFatigueFshear2;
    }

    public Double getCriFatigueFshearTotal() {
        return criFatigueFshearTotal;
    }

    public Double getCriFatigueFactor() {
        return criFatigueFactor;
    }

    public Integer getCriFatigueBoltId() {
        return criFatigueBoltId;
    }

    public Integer getCriFatigueLCId() {
        return criFatigueLCId;
    }

    public Double getMaxStaticFaxial() {
        return maxStaticFaxial;
    }

    public Integer getMaxStaticFaxialBoltId() {
        return maxStaticFaxialBoltId;
    }

    public Integer getMaxStaticFaxialLcId() {
        return maxStaticFaxialLcId;
    }

    public Double getMaxStaticFshear1() {
        return maxStaticFshear1;
    }

    public Integer getMaxStaticFshear1BoltId() {
        return maxStaticFshear1BoltId;
    }

    public Integer getMaxStaticFshear1LcId() {
        return maxStaticFshear1LcId;
    }

    public Double getMaxStaticFshear2() {
        return maxStaticFshear2;
    }

    public Integer getMaxStaticFshear2BoltId() {
        return maxStaticFshear2BoltId;
    }

    public Integer getMaxStaticFshear2LcId() {
        return maxStaticFshear2LcId;
    }

    public Double getMaxStaticFshearTotal() {
        return maxStaticFshearTotal;
    }

    public Integer getMaxStaticFshearTotalBoltId() {
        return maxStaticFshearTotalBoltId;
    }

    public Integer getMaxStaticFshearTotalLcId() {
        return maxStaticFshearTotalLcId;
    }

    public Double getMaxStaticFaxialCriFactor() {
        return maxStaticFaxialCriFactor;
    }

    public Double getMaxStaticFshear1CriFactor() {
        return maxStaticFshear1CriFactor;
    }

    public Double getMaxStaticFshear2CriFactor() {
        return maxStaticFshear2CriFactor;
    }

    public Double getMaxStaticFshearTotalCriFactor() {
        return maxStaticFshearTotalCriFactor;
    }

    public Double getMaxCrashFaxial() {
        return maxCrashFaxial;
    }

    public Integer getMaxCrashFaxialBoltId() {
        return maxCrashFaxialBoltId;
    }

    public Integer getMaxCrashFaxialLcId() {
        return maxCrashFaxialLcId;
    }

    public Double getMaxCrashFshear1() {
        return maxCrashFshear1;
    }

    public Integer getMaxCrashFshear1BoltId() {
        return maxCrashFshear1BoltId;
    }

    public Integer getMaxCrashFshear1LcId() {
        return maxCrashFshear1LcId;
    }

    public Double getMaxCrashFshear2() {
        return maxCrashFshear2;
    }

    public Integer getMaxCrashFshear2BoltId() {
        return maxCrashFshear2BoltId;
    }

    public Integer getMaxCrashFshear2LcId() {
        return maxCrashFshear2LcId;
    }

    public Double getMaxCrashFshearTotal() {
        return maxCrashFshearTotal;
    }

    public Integer getMaxCrashFshearTotalBoltId() {
        return maxCrashFshearTotalBoltId;
    }

    public Integer getMaxCrashFshearTotalLcId() {
        return maxCrashFshearTotalLcId;
    }

    public Double getMaxCrashFaxialCriFactor() {
        return maxCrashFaxialCriFactor;
    }

    public Double getMaxCrashFshear1CriFactor() {
        return maxCrashFshear1CriFactor;
    }

    public Double getMaxCrashFshear2CriFactor() {
        return maxCrashFshear2CriFactor;
    }

    public Double getMaxCrashFshearTotalCriFactor() {
        return maxCrashFshearTotalCriFactor;
    }

    public Double getMaxFatigueFaxial() {
        return maxFatigueFaxial;
    }

    public Integer getMaxFatigueFaxialBoltId() {
        return maxFatigueFaxialBoltId;
    }

    public Integer getMaxFatigueFaxialLcId() {
        return maxFatigueFaxialLcId;
    }

    public Double getMaxFatigueFshear1() {
        return maxFatigueFshear1;
    }

    public Integer getMaxFatigueFshear1BoltId() {
        return maxFatigueFshear1BoltId;
    }

    public Integer getMaxFatigueFshear1LcId() {
        return maxFatigueFshear1LcId;
    }

    public Double getMaxFatigueFshear2() {
        return maxFatigueFshear2;
    }

    public Integer getMaxFatigueFshear2BoltId() {
        return maxFatigueFshear2BoltId;
    }

    public Integer getMaxFatigueFshear2LcId() {
        return maxFatigueFshear2LcId;
    }

    public Double getMaxFatigueFshearTotal() {
        return maxFatigueFshearTotal;
    }

    public Integer getMaxFatigueFshearTotalBoltId() {
        return maxFatigueFshearTotalBoltId;
    }

    public Integer getMaxFatigueFshearTotalLcId() {
        return maxFatigueFshearTotalLcId;
    }

    public Double getMaxFatigueFaxialCriFactor() {
        return maxFatigueFaxialCriFactor;
    }

    public Double getMaxFatigueFshear1CriFactor() {
        return maxFatigueFshear1CriFactor;
    }

    public Double getMaxFatigueFshear2CriFactor() {
        return maxFatigueFshear2CriFactor;
    }

    public Double getMaxFatigueFshearTotalCriFactor() {
        return maxFatigueFshearTotalCriFactor;
    }

    public Double getMinStaticFaxial() {
        return minStaticFaxial;
    }

    public Integer getMinStaticFaxialBoltId() {
        return minStaticFaxialBoltId;
    }

    public Integer getMinStaticFaxialLcId() {
        return minStaticFaxialLcId;
    }

    public Double getMinStaticFshear1() {
        return minStaticFshear1;
    }

    public Integer getMinStaticFshear1BoltId() {
        return minStaticFshear1BoltId;
    }

    public Integer getMinStaticFshear1LcId() {
        return minStaticFshear1LcId;
    }

    public Double getMinStaticFshear2() {
        return minStaticFshear2;
    }

    public Integer getMinStaticFshear2BoltId() {
        return minStaticFshear2BoltId;
    }

    public Integer getMinStaticFshear2LcId() {
        return minStaticFshear2LcId;
    }

    public Double getMinStaticFshearTotal() {
        return minStaticFshearTotal;
    }

    public Integer getMinStaticFshearTotalBoltId() {
        return minStaticFshearTotalBoltId;
    }

    public Integer getMinStaticFshearTotalLcId() {
        return minStaticFshearTotalLcId;
    }

    public Double getMinStaticFaxialCriFactor() {
        return minStaticFaxialCriFactor;
    }

    public Double getMinStaticFshear1CriFactor() {
        return minStaticFshear1CriFactor;
    }

    public Double getMinStaticFshear2CriFactor() {
        return minStaticFshear2CriFactor;
    }

    public Double getMinStaticFshearTotalCriFactor() {
        return minStaticFshearTotalCriFactor;
    }

    public Double getMinFatigueFaxial() {
        return minFatigueFaxial;
    }

    public Integer getMinFatigueFaxialBoltId() {
        return minFatigueFaxialBoltId;
    }

    public Integer getMinFatigueFaxialLcId() {
        return minFatigueFaxialLcId;
    }

    public Double getMinFatigueFshear1() {
        return minFatigueFshear1;
    }

    public Integer getMinFatigueFshear1BoltId() {
        return minFatigueFshear1BoltId;
    }

    public Integer getMinFatigueFshear1LcId() {
        return minFatigueFshear1LcId;
    }

    public Double getMinFatigueFshear2() {
        return minFatigueFshear2;
    }

    public Integer getMinFatigueFshear2BoltId() {
        return minFatigueFshear2BoltId;
    }

    public Integer getMinFatigueFshear2LcId() {
        return minFatigueFshear2LcId;
    }

    public Double getMinFatigueFshearTotal() {
        return minFatigueFshearTotal;
    }

    public Integer getMinFatigueFshearTotalBoltId() {
        return minFatigueFshearTotalBoltId;
    }

    public Integer getMinFatigueFshearTotalLcId() {
        return minFatigueFshearTotalLcId;
    }

    public Double getMinFatigueFaxialCriFactor() {
        return minFatigueFaxialCriFactor;
    }

    public Double getMinFatigueFshear1CriFactor() {
        return minFatigueFshear1CriFactor;
    }

    public Double getMinFatigueFshear2CriFactor() {
        return minFatigueFshear2CriFactor;
    }

    public Double getMinFatigueFshearTotalCriFactor() {
        return minFatigueFshearTotalCriFactor;
    }

    public Double getMinCrashFaxial() {
        return minCrashFaxial;
    }

    public Integer getMinCrashFaxialBoltId() {
        return minCrashFaxialBoltId;
    }

    public Integer getMinCrashFaxialLcId() {
        return minCrashFaxialLcId;
    }

    public Double getMinCrashFshear1() {
        return minCrashFshear1;
    }

    public Integer getMinCrashFshear1BoltId() {
        return minCrashFshear1BoltId;
    }

    public Integer getMinCrashFshear1LcId() {
        return minCrashFshear1LcId;
    }

    public Double getMinCrashFshear2() {
        return minCrashFshear2;
    }

    public Integer getMinCrashFshear2BoltId() {
        return minCrashFshear2BoltId;
    }

    public Integer getMinCrashFshear2LcId() {
        return minCrashFshear2LcId;
    }

    public Double getMinCrashFshearTotal() {
        return minCrashFshearTotal;
    }

    public Integer getMinCrashFshearTotalBoltId() {
        return minCrashFshearTotalBoltId;
    }

    public Integer getMinCrashFshearTotalLcId() {
        return minCrashFshearTotalLcId;
    }

    public Double getMinCrashFaxialCriFactor() {
        return minCrashFaxialCriFactor;
    }

    public Double getMinCrashFshear1CriFactor() {
        return minCrashFshear1CriFactor;
    }

    public Double getMinCrashFshear2CriFactor() {
        return minCrashFshear2CriFactor;
    }

    public Double getMinCrashFshearTotalCriFactor() {
        return minCrashFshearTotalCriFactor;
    }

    public Double getAbsMinStaticFaxial() {
        return absMinStaticFaxial;
    }

    public Integer getAbsMinStaticFaxialBoltId() {
        return absMinStaticFaxialBoltId;
    }

    public Integer getAbsMinStaticFaxialLcId() {
        return absMinStaticFaxialLcId;
    }

    public Double getAbsMinStaticFshear1() {
        return absMinStaticFshear1;
    }

    public Integer getAbsMinStaticFshear1BoltId() {
        return absMinStaticFshear1BoltId;
    }

    public Integer getAbsMinStaticFshear1LcId() {
        return absMinStaticFshear1LcId;
    }

    public Double getAbsMinStaticFshear2() {
        return absMinStaticFshear2;
    }

    public Integer getAbsMinStaticFshear2BoltId() {
        return absMinStaticFshear2BoltId;
    }

    public Integer getAbsMinStaticFshear2LcId() {
        return absMinStaticFshear2LcId;
    }

    public Double getAbsMinStaticFshearTotal() {
        return absMinStaticFshearTotal;
    }

    public Integer getAbsMinStaticFshearTotalBoltId() {
        return absMinStaticFshearTotalBoltId;
    }

    public Integer getAbsMinStaticFshearTotalLcId() {
        return absMinStaticFshearTotalLcId;
    }

    public Double getAbsMinStaticFaxialCriFactor() {
        return absMinStaticFaxialCriFactor;
    }

    public Double getAbsMinStaticFshear1CriFactor() {
        return absMinStaticFshear1CriFactor;
    }

    public Double getAbsMinStaticFshear2CriFactor() {
        return absMinStaticFshear2CriFactor;
    }

    public Double getAbsMinStaticFshearTotalCriFactor() {
        return absMinStaticFshearTotalCriFactor;
    }

    public Double getAbsMinCrashFaxial() {
        return absMinCrashFaxial;
    }

    public Integer getAbsMinCrashFaxialBoltId() {
        return absMinCrashFaxialBoltId;
    }

    public Integer getAbsMinCrashFaxialLcId() {
        return absMinCrashFaxialLcId;
    }

    public Double getAbsMinCrashFshear1() {
        return absMinCrashFshear1;
    }

    public Integer getAbsMinCrashFshear1BoltId() {
        return absMinCrashFshear1BoltId;
    }

    public Integer getAbsMinCrashFshear1LcId() {
        return absMinCrashFshear1LcId;
    }

    public Double getAbsMinCrashFshear2() {
        return absMinCrashFshear2;
    }

    public Integer getAbsMinCrashFshear2BoltId() {
        return absMinCrashFshear2BoltId;
    }

    public Integer getAbsMinCrashFshear2LcId() {
        return absMinCrashFshear2LcId;
    }

    public Double getAbsMinCrashFshearTotal() {
        return absMinCrashFshearTotal;
    }

    public Integer getAbsMinCrashFshearTotalBoltId() {
        return absMinCrashFshearTotalBoltId;
    }

    public Integer getAbsMinCrashFshearTotalLcId() {
        return absMinCrashFshearTotalLcId;
    }

    public Double getAbsMinCrashFaxialCriFactor() {
        return absMinCrashFaxialCriFactor;
    }

    public Double getAbsMinCrashFshear1CriFactor() {
        return absMinCrashFshear1CriFactor;
    }

    public Double getAbsMinCrashFshear2CriFactor() {
        return absMinCrashFshear2CriFactor;
    }

    public Double getAbsMinCrashFshearTotalCriFactor() {
        return absMinCrashFshearTotalCriFactor;
    }

    public Double getAbsMinFatigueFaxial() {
        return absMinFatigueFaxial;
    }

    public Integer getAbsMinFatigueFaxialBoltId() {
        return absMinFatigueFaxialBoltId;
    }

    public Integer getAbsMinFatigueFaxialLcId() {
        return absMinFatigueFaxialLcId;
    }

    public Double getAbsMinFatigueFshear1() {
        return absMinFatigueFshear1;
    }

    public Integer getAbsMinFatigueFshear1BoltId() {
        return absMinFatigueFshear1BoltId;
    }

    public Integer getAbsMinFatigueFshear1LcId() {
        return absMinFatigueFshear1LcId;
    }

    public Double getAbsMinFatigueFshear2() {
        return absMinFatigueFshear2;
    }

    public Integer getAbsMinFatigueFshear2BoltId() {
        return absMinFatigueFshear2BoltId;
    }

    public Integer getAbsMinFatigueFshear2LcId() {
        return absMinFatigueFshear2LcId;
    }

    public Double getAbsMinFatigueFshearTotal() {
        return absMinFatigueFshearTotal;
    }

    public Integer getAbsMinFatigueFshearTotalBoltId() {
        return absMinFatigueFshearTotalBoltId;
    }

    public Integer getAbsMinFatigueFshearTotalLcId() {
        return absMinFatigueFshearTotalLcId;
    }

    public Double getAbsMinFatigueFaxialCriFactor() {
        return absMinFatigueFaxialCriFactor;
    }

    public Double getAbsMinFatigueFshear1CriFactor() {
        return absMinFatigueFshear1CriFactor;
    }

    public Double getAbsMinFatigueFshear2CriFactor() {
        return absMinFatigueFshear2CriFactor;
    }

    public Double getAbsMinFatigueFshearTotalCriFactor() {
        return absMinFatigueFshearTotalCriFactor;
    }

    public double getMaxFaxial_shear1() {
        return maxFaxial_shear1;
    }

    public double getMaxFaxial_shear2() {
        return maxFaxial_shear2;
    }

    public double getMaxFaxial_shearTotal() {
        return maxFaxial_shearTotal;
    }

    public double getMaxFshear1_axial() {
        return maxFshear1_axial;
    }

    public double getMaxFshear1_shear2() {
        return maxFshear1_shear2;
    }

    public double getMaxFshear1_shearTotal() {
        return maxFshear1_shearTotal;
    }

    public double getMaxFshear2_axial() {
        return maxFshear2_axial;
    }

    public double getMaxFshear2_shear1() {
        return maxFshear2_shear1;
    }

    public double getMaxFshear2_shearTotal() {
        return maxFshear2_shearTotal;
    }

    public double getMaxFshearTotal_axial() {
        return maxFshearTotal_axial;
    }

    public double getMaxFshearTotal_shear1() {
        return maxFshearTotal_shear1;
    }

    public double getMaxFshearTotal_shear2() {
        return maxFshearTotal_shear2;
    }

    public double getMinFaxial_shear1() {
        return minFaxial_shear1;
    }

    public double getMinFaxial_shear2() {
        return minFaxial_shear2;
    }

    public double getMinFaxial_shearTotal() {
        return minFaxial_shearTotal;
    }

    public double getMinFshear1_axial() {
        return minFshear1_axial;
    }

    public double getMinFshear1_shear2() {
        return minFshear1_shear2;
    }

    public double getMinFshear1_shearTotal() {
        return minFshear1_shearTotal;
    }

    public double getMinFshear2_axial() {
        return minFshear2_axial;
    }

    public double getMinFshear2_shear1() {
        return minFshear2_shear1;
    }

    public double getMinFshear2_shearTotal() {
        return minFshear2_shearTotal;
    }

    public double getMinFshearTotal_axial() {
        return minFshearTotal_axial;
    }

    public double getMinFshearTotal_shear1() {
        return minFshearTotal_shear1;
    }

    public double getMinFshearTotal_shear2() {
        return minFshearTotal_shear2;
    }

    public double getAbsMinFaxial_shear1() {
        return absMinFaxial_shear1;
    }

    public double getAbsMinFaxial_shear2() {
        return absMinFaxial_shear2;
    }

    public double getAbsMinFaxial_shearTotal() {
        return absMinFaxial_shearTotal;
    }

    public double getAbsMinFshear1_axial() {
        return absMinFshear1_axial;
    }

    public double getAbsMinFshear1_shear2() {
        return absMinFshear1_shear2;
    }

    public double getAbsMinFshear1_shearTotal() {
        return absMinFshear1_shearTotal;
    }

    public double getAbsMinFshear2_axial() {
        return absMinFshear2_axial;
    }

    public double getAbsMinFshear2_shear1() {
        return absMinFshear2_shear1;
    }

    public double getAbsMinFshear2_shearTotal() {
        return absMinFshear2_shearTotal;
    }

    public double getAbsMinFshearTotal_axial() {
        return absMinFshearTotal_axial;
    }

    public double getAbsMinFshearTotal_shear1() {
        return absMinFshearTotal_shear1;
    }

    public double getAbsMinFshearTotal_shear2() {
        return absMinFshearTotal_shear2;
    }

    public Double getMaxStaticFaxial_shear1() {
        return maxStaticFaxial_shear1;
    }

    public Double getMaxStaticFaxial_shear2() {
        return maxStaticFaxial_shear2;
    }

    public Double getMaxStaticFaxial_shearTotal() {
        return maxStaticFaxial_shearTotal;
    }

    public Double getMaxStaticFshear1_axial() {
        return maxStaticFshear1_axial;
    }

    public Double getMaxStaticFshear1_shear2() {
        return maxStaticFshear1_shear2;
    }

    public Double getMaxStaticFshear1_shearTotal() {
        return maxStaticFshear1_shearTotal;
    }

    public Double getMaxStaticFshear2_axial() {
        return maxStaticFshear2_axial;
    }

    public Double getMaxStaticFshear2_shear1() {
        return maxStaticFshear2_shear1;
    }

    public Double getMaxStaticFshear2_shearTotal() {
        return maxStaticFshear2_shearTotal;
    }

    public Double getMaxStaticFshearTotal_axial() {
        return maxStaticFshearTotal_axial;
    }

    public Double getMaxStaticFshearTotal_shear1() {
        return maxStaticFshearTotal_shear1;
    }

    public Double getMaxStaticFshearTotal_shear2() {
        return maxStaticFshearTotal_shear2;
    }

    public Double getMaxCrashFaxial_shear1() {
        return maxCrashFaxial_shear1;
    }

    public Double getMaxCrashFaxial_shear2() {
        return maxCrashFaxial_shear2;
    }

    public Double getMaxCrashFaxial_shearTotal() {
        return maxCrashFaxial_shearTotal;
    }

    public Double getMaxCrashFshear1_axial() {
        return maxCrashFshear1_axial;
    }

    public Double getMaxCrashFshear1_shear2() {
        return maxCrashFshear1_shear2;
    }

    public Double getMaxCrashFshear1_shearTotal() {
        return maxCrashFshear1_shearTotal;
    }

    public Double getMaxCrashFshear2_axial() {
        return maxCrashFshear2_axial;
    }

    public Double getMaxCrashFshear2_shear1() {
        return maxCrashFshear2_shear1;
    }

    public Double getMaxCrashFshear2_shearTotal() {
        return maxCrashFshear2_shearTotal;
    }

    public Double getMaxCrashFshearTotal_shear1() {
        return maxCrashFshearTotal_shear1;
    }

    public Double getMaxCrashFshearTotal_shear2() {
        return maxCrashFshearTotal_shear2;
    }

    public Double getMaxCrashFshearTotal_axial() {
        return maxCrashFshearTotal_axial;
    }

    public Double getMaxFatigueFaxial_shear1() {
        return maxFatigueFaxial_shear1;
    }

    public Double getMaxFatigueFaxial_shear2() {
        return maxFatigueFaxial_shear2;
    }

    public Double getMaxFatigueFaxial_shearTotal() {
        return maxFatigueFaxial_shearTotal;
    }

    public Double getMaxFatigueFshear1_axial() {
        return maxFatigueFshear1_axial;
    }

    public Double getMaxFatigueFshear1_shear2() {
        return maxFatigueFshear1_shear2;
    }

    public Double getMaxFatigueFshear1_shearTotal() {
        return maxFatigueFshear1_shearTotal;
    }

    public Double getMaxFatigueFshear2_axial() {
        return maxFatigueFshear2_axial;
    }

    public Double getMaxFatigueFshear2_shear1() {
        return maxFatigueFshear2_shear1;
    }

    public Double getMaxFatigueFshear2_shearTotal() {
        return maxFatigueFshear2_shearTotal;
    }

    public Double getMaxFatigueFshearTotal_axial() {
        return maxFatigueFshearTotal_axial;
    }

    public Double getMaxFatigueFshearTotal_shear1() {
        return maxFatigueFshearTotal_shear1;
    }

    public Double getMaxFatigueFshearTotal_shear2() {
        return maxFatigueFshearTotal_shear2;
    }

    public Double getMinStaticFaxial_shear1() {
        return minStaticFaxial_shear1;
    }

    public Double getMinStaticFaxial_shear2() {
        return minStaticFaxial_shear2;
    }

    public Double getMinStaticFaxial_shearTotal() {
        return minStaticFaxial_shearTotal;
    }

    public Double getMinStaticFshear1_axial() {
        return minStaticFshear1_axial;
    }

    public Double getMinStaticFshear1_shear2() {
        return minStaticFshear1_shear2;
    }

    public Double getMinStaticFshear1_shearTotal() {
        return minStaticFshear1_shearTotal;
    }

    public Double getMinStaticFshear2_axial() {
        return minStaticFshear2_axial;
    }

    public Double getMinStaticFshear2_shear1() {
        return minStaticFshear2_shear1;
    }

    public Double getMinStaticFshear2_shearTotal() {
        return minStaticFshear2_shearTotal;
    }

    public Double getMinStaticFshearTotal_axial() {
        return minStaticFshearTotal_axial;
    }

    public Double getMinStaticFshearTotal_shear1() {
        return minStaticFshearTotal_shear1;
    }

    public Double getMinStaticFshearTotal_shear2() {
        return minStaticFshearTotal_shear2;
    }

    public Double getMinFatigueFaxial_shear1() {
        return minFatigueFaxial_shear1;
    }

    public Double getMinFatigueFaxial_shear2() {
        return minFatigueFaxial_shear2;
    }

    public Double getMinFatigueFaxial_shearTotal() {
        return minFatigueFaxial_shearTotal;
    }

    public Double getMinFatigueFshear1_axial() {
        return minFatigueFshear1_axial;
    }

    public Double getMinFatigueFshear1_shear2() {
        return minFatigueFshear1_shear2;
    }

    public Double getMinFatigueFshear1_shearTotal() {
        return minFatigueFshear1_shearTotal;
    }

    public Double getMinFatigueFshear2_axial() {
        return minFatigueFshear2_axial;
    }

    public Double getMinFatigueFshear2_shear1() {
        return minFatigueFshear2_shear1;
    }

    public Double getMinFatigueFshear2_shearTotal() {
        return minFatigueFshear2_shearTotal;
    }

    public Double getMinFatigueFshearTotal_axial() {
        return minFatigueFshearTotal_axial;
    }

    public Double getMinFatigueFshearTotal_shear1() {
        return minFatigueFshearTotal_shear1;
    }

    public Double getMinFatigueFshearTotal_shear2() {
        return minFatigueFshearTotal_shear2;
    }

    public Double getMinCrashFaxial_shear1() {
        return minCrashFaxial_shear1;
    }

    public Double getMinCrashFaxial_shear2() {
        return minCrashFaxial_shear2;
    }

    public Double getMinCrashFaxial_shearTotal() {
        return minCrashFaxial_shearTotal;
    }

    public Double getMinCrashFshear1_axial() {
        return minCrashFshear1_axial;
    }

    public Double getMinCrashFshear1_shear2() {
        return minCrashFshear1_shear2;
    }

    public Double getMinCrashFshear1_shearTotal() {
        return minCrashFshear1_shearTotal;
    }

    public Double getMinCrashFshear2_axial() {
        return minCrashFshear2_axial;
    }

    public Double getMinCrashFshear2_shear1() {
        return minCrashFshear2_shear1;
    }

    public Double getMinCrashFshear2_shearTotal() {
        return minCrashFshear2_shearTotal;
    }

    public Double getMinCrashFshearTotal_axial() {
        return minCrashFshearTotal_axial;
    }

    public Double getMinCrashFshearTotal_shear1() {
        return minCrashFshearTotal_shear1;
    }

    public Double getMinCrashFshearTotal_shear2() {
        return minCrashFshearTotal_shear2;
    }

    public Double getAbsMinStaticFaxial_shear1() {
        return absMinStaticFaxial_shear1;
    }

    public Double getAbsMinStaticFaxial_shear2() {
        return absMinStaticFaxial_shear2;
    }

    public Double getAbsMinStaticFaxial_shearTotal() {
        return absMinStaticFaxial_shearTotal;
    }

    public Double getAbsMinStaticFshear1_axial() {
        return absMinStaticFshear1_axial;
    }

    public Double getAbsMinStaticFshear1_shear2() {
        return absMinStaticFshear1_shear2;
    }

    public Double getAbsMinStaticFshear1_shearTotal() {
        return absMinStaticFshear1_shearTotal;
    }

    public Double getAbsMinStaticFshear2_axial() {
        return absMinStaticFshear2_axial;
    }

    public Double getAbsMinStaticFshear2_shear1() {
        return absMinStaticFshear2_shear1;
    }

    public Double getAbsMinStaticFshear2_shearTotal() {
        return absMinStaticFshear2_shearTotal;
    }

    public Double getAbsMinStaticFshearTotal_axial() {
        return absMinStaticFshearTotal_axial;
    }

    public Double getAbsMinStaticFshearTotal_shear1() {
        return absMinStaticFshearTotal_shear1;
    }

    public Double getAbsMinStaticFshearTotal_shear2() {
        return absMinStaticFshearTotal_shear2;
    }

    public Double getAbsMinCrashFaxial_shear1() {
        return absMinCrashFaxial_shear1;
    }

    public Double getAbsMinCrashFaxial_shear2() {
        return absMinCrashFaxial_shear2;
    }

    public Double getAbsMinCrashFaxial_shearTotal() {
        return absMinCrashFaxial_shearTotal;
    }

    public Double getAbsMinCrashFshear1_axial() {
        return absMinCrashFshear1_axial;
    }

    public Double getAbsMinCrashFshear1_shear2() {
        return absMinCrashFshear1_shear2;
    }

    public Double getAbsMinCrashFshear1_shearTotal() {
        return absMinCrashFshear1_shearTotal;
    }

    public Double getAbsMinCrashFshear2_axial() {
        return absMinCrashFshear2_axial;
    }

    public Double getAbsMinCrashFshear2_shear1() {
        return absMinCrashFshear2_shear1;
    }

    public Double getAbsMinCrashFshear2_shearTotal() {
        return absMinCrashFshear2_shearTotal;
    }

    public Double getAbsMinCrashFshearTotal_axial() {
        return absMinCrashFshearTotal_axial;
    }

    public Double getAbsMinCrashFshearTotal_shear1() {
        return absMinCrashFshearTotal_shear1;
    }

    public Double getAbsMinCrashFshearTotal_shear2() {
        return absMinCrashFshearTotal_shear2;
    }

    public Double getAbsMinFatigueFaxial_shear1() {
        return absMinFatigueFaxial_shear1;
    }

    public Double getAbsMinFatigueFaxial_shear2() {
        return absMinFatigueFaxial_shear2;
    }

    public Double getAbsMinFatigueFaxial_shearTotal() {
        return absMinFatigueFaxial_shearTotal;
    }

    public Double getAbsMinFatigueFshear1_axial() {
        return absMinFatigueFshear1_axial;
    }

    public Double getAbsMinFatigueFshear1_shear2() {
        return absMinFatigueFshear1_shear2;
    }

    public Double getAbsMinFatigueFshear1_shearTotal() {
        return absMinFatigueFshear1_shearTotal;
    }

    public Double getAbsMinFatigueFshear2_axial() {
        return absMinFatigueFshear2_axial;
    }

    public Double getAbsMinFatigueFshear2_shear1() {
        return absMinFatigueFshear2_shear1;
    }

    public Double getAbsMinFatigueFshear2_shearTotal() {
        return absMinFatigueFshear2_shearTotal;
    }

    public Double getAbsMinFatigueFshearTotal_axial() {
        return absMinFatigueFshearTotal_axial;
    }

    public Double getAbsMinFatigueFshearTotal_shear1() {
        return absMinFatigueFshearTotal_shear1;
    }

    public Double getAbsMinFatigueFshearTotal_shear2() {
        return absMinFatigueFshearTotal_shear2;
    }

    @Override
    public double getFrictionCoefficient() {
        return frictionCoefficient;
    }

    @Override
    public void setFrictionCoefficient(double frictionCoefficient) {
        this.frictionCoefficient = frictionCoefficient;
    }

    public boolean isNAasValue() {
        return isNAasValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CbarResults that = (CbarResults) o;

        return partName != null ? partName.equals(that.partName) : that.partName == null;
    }

    @Override
    public int hashCode() {
        return partName != null ? partName.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "<cbar results for part " + partName + ">";
    }
}
