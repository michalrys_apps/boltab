package com.michalrys.boltab.boltabsetup;

public enum LoadCaseType {
    STATIC("Static load cases"),
    CRASH("Crash load cases"),
    FATIGUE("Fatigue load cases");

    String description;

    LoadCaseType(String description) {
        this.description = description;
    }
}
