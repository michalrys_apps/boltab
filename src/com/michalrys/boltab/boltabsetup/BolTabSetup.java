package com.michalrys.boltab.boltabsetup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BolTabSetup {
    private static final double DEFAULT_FRICTION_COEFFICIENT = 0.15;
    private static final boolean DEFAULT_ARE_EXTRA_TABLES_GENERATED = false;
    private final String BOLTAB_SETUP_FILE_NAME = "BolTabSetup.json";
    private String bolTabSetupFilePath;
    private Double frictionCoefficient;
    private Double otherFrictionCoefficient;
    private Boolean isOtherFrictionCoefficientUsed;
    private ArrayList<String> partNamesForOtherFrictionCoefficient;
    private Boolean areExtraTablesGenerated;
    private List<String> partNamesForExtraTables;
    private List<Integer> lcIdsForExtraTableStaticLC;
    private List<Integer> lcIdsForExtraTableCrashLC;
    private List<Integer> lcIdsForExtraTableFatigueLC;

    public void read(String csvFilePath) {
        setBolTabSetupFilePath(csvFilePath);

        if (doesBolTabSetupFileExist()) {
            readJsonFileAndSetUp();
        } else {
            dontReadAnyFileAndSetUpDefaultConfiguration();
        }

    }

    private void dontReadAnyFileAndSetUpDefaultConfiguration() {
        frictionCoefficient = DEFAULT_FRICTION_COEFFICIENT;
        areExtraTablesGenerated = DEFAULT_ARE_EXTRA_TABLES_GENERATED;
        otherFrictionCoefficient = DEFAULT_FRICTION_COEFFICIENT;
        isOtherFrictionCoefficientUsed = false;

        System.out.println("[OK] Default boltab configuration was set up. Friction is = " + frictionCoefficient);
    }

    private void readJsonFileAndSetUp() {
        StringBuilder fileRead = new StringBuilder();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(bolTabSetupFilePath))) {
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                fileRead.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            // make json object
            JSONObject jsonObject = new JSONObject(fileRead.toString());

            // 1 - friction coefficient
            frictionCoefficient = jsonObject.getDouble("default friction coefficient");

            // 1.2 - other friction coefficient
            otherFrictionCoefficient = jsonObject.getDouble("other friction coefficient");

            // 1.3 - use other mi ?
            isOtherFrictionCoefficientUsed = jsonObject.getBoolean("use other friction coefficient");

            // 1.4 - other mi for parts
            JSONArray tempArray = jsonObject.getJSONArray("part names for other friction coefficient");
            partNamesForOtherFrictionCoefficient = new ArrayList<>();

            for (int i = 0; i < tempArray.length(); i++) {
                partNamesForOtherFrictionCoefficient.add(tempArray.get(i).toString());
            }

            // 2 - generate extra tables ?
            areExtraTablesGenerated = jsonObject.getBoolean("are extra tables generated");

            // 3 - part names extra
            tempArray = jsonObject.getJSONArray("part names for extra tables");
            partNamesForExtraTables = new ArrayList<>();

            for (int i = 0; i < tempArray.length(); i++) {
                partNamesForExtraTables.add(tempArray.get(i).toString());
            }

            // 4 - lc ids for static
            tempArray = jsonObject.getJSONArray("lc ids for static load cases");
            lcIdsForExtraTableStaticLC = new ArrayList<>();

            for (int i = 0; i < tempArray.length(); i++) {
                lcIdsForExtraTableStaticLC.add(tempArray.getInt(i));
            }

            // 5 - lc ids for crash
            tempArray = jsonObject.getJSONArray("lc ids for crash load cases");
            lcIdsForExtraTableCrashLC = new ArrayList<>();

            for (int i = 0; i < tempArray.length(); i++) {
                lcIdsForExtraTableCrashLC.add(tempArray.getInt(i));
            }

            // 6 - lc ids for fatigue
            tempArray = jsonObject.getJSONArray("lc ids for fatigue load cases");
            lcIdsForExtraTableFatigueLC = new ArrayList<>();

            for (int i = 0; i < tempArray.length(); i++) {
                lcIdsForExtraTableFatigueLC.add(tempArray.getInt(i));
            }

            System.out.println("[OK] Custom boltab configuration was set up:");
            System.out.println("\t + default friction coefficient is = " + frictionCoefficient);
            System.out.println("\t + other friction coefficient is = " + otherFrictionCoefficient);
            System.out.println("\t + use other friction  = " + isOtherFrictionCoefficientUsed);
            System.out.println("\t + for parts  = " + partNamesForOtherFrictionCoefficient.toString());
            System.out.println("\t + generate extra lc tables = " + areExtraTablesGenerated);
            System.out.println("\t + for parts = " + partNamesForExtraTables.toString());
            System.out.println("\t + static lc = " + lcIdsForExtraTableStaticLC.toString());
            System.out.println("\t + crash lc = " + lcIdsForExtraTableCrashLC.toString());
            System.out.println("\t + fatigue lc = " + lcIdsForExtraTableFatigueLC.toString());

        } catch (JSONException exc) {
            System.out.println("\t[!!] Error during reading setup file - there is sth wrong with this file. Check it:");
            System.out.println("\t\t>> " + exc.getMessage());
            dontReadAnyFileAndSetUpDefaultConfiguration();
        }
    }

    private boolean doesBolTabSetupFileExist() {
        File file = new File(bolTabSetupFilePath);
        return file.exists();
    }

    private void setBolTabSetupFilePath(String csvFilePath) {
        bolTabSetupFilePath = getFilePathWithoutFileName(csvFilePath) + "/" + BOLTAB_SETUP_FILE_NAME;
    }

    private String getFilePathWithoutFileName(String filePathSlashesWithFileName) {
        String fileName = filePathSlashesWithFileName.replaceAll(".*[/]", "");
        String filePath = filePathSlashesWithFileName.replaceAll(fileName, "");
        return filePath;
    }

    public Double getFrictionCoefficient() {
        return frictionCoefficient;
    }

    public Boolean getAreExtraTablesGenerated() {
        return areExtraTablesGenerated;
    }

    public List<String> getPartNamesForExtraTables() {
        return partNamesForExtraTables;
    }

    public List<Integer> getLcIdsForExtraTableStaticLC() {
        return lcIdsForExtraTableStaticLC;
    }

    public List<Integer> getLcIdsForExtraTableCrashLC() {
        return lcIdsForExtraTableCrashLC;
    }

    public List<Integer> getLcIdsForExtraTableFatigueLC() {
        return lcIdsForExtraTableFatigueLC;
    }

    public Double getOtherFrictionCoefficient() {
        return otherFrictionCoefficient;
    }

    public Boolean getIsOtherFrictionCoefficientUsed() {
        return isOtherFrictionCoefficientUsed;
    }

    public ArrayList<String> getPartNamesForOtherFrictionCoefficient() {
        return partNamesForOtherFrictionCoefficient;
    }
}
