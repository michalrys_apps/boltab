package com.michalrys.boltab.fixme;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import jdk.nashorn.internal.parser.JSONParser;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FixMeMainQuickChecks01 {
    public static void main(String[] args) {

        String filePath = "e:\\JAVA\\_ECE\\05_BOLTS_EXC\\JAVA_APP\\01_EXCEL_TESTS\\src\\com\\michalrys\\boltab\\BolTabSetup.json";

        StringBuilder fileRead = new StringBuilder();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath))) {
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                fileRead.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //System.out.println(fileRead);

        // make json object
        JSONObject jsonObject = new JSONObject(fileRead.toString());


        try {
            // 1 - mi
            Double frictionCoefficient = jsonObject.getDouble("friction coefficient");
            System.out.println("friction coefficient = " + frictionCoefficient);

            // 2 - generate extra tables ?
            Boolean areExtraTablesGenerated = jsonObject.getBoolean("are extra tables generated");

            System.out.println("are extra tables generated = " + areExtraTablesGenerated);

            // 3 - part names extra
            JSONArray tempArray = jsonObject.getJSONArray("part names for extra tables");
            List<String> partNamesForExtraTables = new ArrayList<>();

            for (int i = 0; i < tempArray.length(); i++) {
                partNamesForExtraTables.add(tempArray.get(i).toString());
            }
            System.out.println("part names for extra tables = " + partNamesForExtraTables.toString());

            // 4 - lc ids for static
            tempArray = jsonObject.getJSONArray("lc ids for static load cases");
            List<Integer> lcIdsForExtraTableStaticLC = new ArrayList<>();

            for (int i = 0; i < tempArray.length(); i++) {
                lcIdsForExtraTableStaticLC.add(tempArray.getInt(i));
            }
            System.out.println("lc ids for static load cases = " + lcIdsForExtraTableStaticLC.toString());

            // 5 - lc ids for crash
            tempArray = jsonObject.getJSONArray("lc ids for crash load cases");
            List<Integer> lcIdsForExtraTableCrashLC = new ArrayList<>();

            for (int i = 0; i < tempArray.length(); i++) {
                lcIdsForExtraTableCrashLC.add(tempArray.getInt(i));
            }
            System.out.println("lc ids for crash load cases = " + lcIdsForExtraTableCrashLC.toString());

            // 6 - lc ids for fatigue
            tempArray = jsonObject.getJSONArray("lc ids for fatigue load cases");
            List<Integer> lcIdsForExtraTableFatigueLC = new ArrayList<>();

            for (int i = 0; i < tempArray.length(); i++) {
                lcIdsForExtraTableFatigueLC.add(tempArray.getInt(i));
            }
            System.out.println("lc ids for fatigue load cases = " + lcIdsForExtraTableFatigueLC.toString());
            // -----------------
        } catch (org.json.JSONException exc) {
            System.out.println("Wrong name! Nothing was done.");
            exc.printStackTrace();
        }


    }
}
