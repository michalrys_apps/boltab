package com.michalrys.boltab;

import com.michalrys.boltab.bolttables.BoltTables;
import com.michalrys.boltab.csvparser.CsvParserResultExtractor;
import com.michalrys.boltab.csvparser.ResultType;
import org.json.JSONObject;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static final String MAIN_LOCATION_ON_SERVER = "//ece508/CAE_2/CAE_Baza_wiedzy/2018-02-07_MR_macros_MR/BOLTAB/AKTUALNA/";
    public static final String VERSION_ACTUAL_ON_SERVER = "//ece508/CAE_2/CAE_Baza_wiedzy/2018-02-07_MR_macros_MR/BOLTAB/AKTUALNA/BolTab.json";
    public static final String STATISTICS_ON_SERVER = "//ece508/CAE_2/CAE_Baza_wiedzy/2018-02-07_MR_macros_MR/BOLTAB/STATS/";
    public static final String VERSION = "1.1.0";
    public static final String DATE = "2019-03-18";
    public static final String AUTHOR = "Michał Ryś";

    public static void main(String[] args) {
        // read file path from table args
        args = getArgsForTests(); //TODO hide this line for production

        // application will be run in bat file - all souts will be shown to client
        printOutProgramHeader();

        // license and version validation
        if (!isValidationOk()) {
            return;
        }

        // check if args is empty
//        if(args.toString().length() == 0) {
//            System.out.println("[!!] Run this app with filepath as input args.");
//            return;
//        }

        // add statistics data
        addStatisticData("e:\\JAVA\\_ECE\\05_BOLTS_EXC\\JAVA_APP\\01_EXCEL_TESTS\\src\\com\\michalrys\\boltab\\");
        //TODO addStatisticData(STATISTICS_ON_SERVER);   //use it for production

        BoltTables boltTables = new BoltTables();
        boltTables.setUpWorkbook();
        CsvParserResultExtractor csvParser = new CsvParserResultExtractor();

        int currentTableOffset = 0;

        for (String currentFilePath : args) {
            boolean isSuccessful = csvParser.extractResults(currentFilePath);
            if (!isSuccessful) {
                // do not operate on wrong read data
                boltTables.setResultType(ResultType.WRONG_CSV_DATA);
                continue;
            }

            csvParser.sortResults();
            // csvParser contains several kind of results: for cbar, rbe2, other.
            currentTableOffset = boltTables.fillWorkBookWithData(currentTableOffset, currentFilePath, csvParser);

            // because there could be cbar.csv, rbe.csv, cbar.csv - different tables must be created, so current must be cleared
            csvParser.clearResults();
        }

        boltTables.saveToFile();
        System.out.println("[OK] Done. Have a nice day ;-)");
    }

    private static void printOutProgramHeader() {
        System.out.println("+---------------------------------------+");
        System.out.println("|  BolTab                               |");
        System.out.println("+---------------------------------------+");
        System.out.println("| version: " + VERSION + ".                       |");
        System.out.println("|    date: " + DATE + "                   |");
        System.out.println("|  author: Michał Ryś                   |");
        System.out.println("| license: freeware for EC-E employees  |");
        System.out.println("+ - - - - - - - - - - - - - - - - - - - +");
    }

    private static boolean isValidationOk() {
        return (isLicenseOk() && isVersionOk());
    }

    private static boolean isLicenseOk() {
        if (LocalDate.now().isBefore(LocalDate.of(2020, 12, 30))) {
            // nothing to print, because this is hidden validation
            return true;
        } else {
            System.out.println("[!!] Something went wrong. Ask " + AUTHOR + ".");
            return false;
        }
    }

    private static boolean isVersionOk() {
        String line;
        StringBuilder lineRead = new StringBuilder();
        //TODO                  for production change FileReader to FileReader(VERSION_ACTUAL_ON_SERVER)
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("e:\\JAVA\\_ECE\\05_BOLTS_EXC\\JAVA_APP\\01_EXCEL_TESTS\\src\\com\\michalrys\\boltab\\BolTab.json"))) {
            while ((line = bufferedReader.readLine()) != null) {
                lineRead.append(line);
            }
        } catch (IOException e) {
            System.out.println("[!!] Not able to check if a new version of application exists. Task is continued...");
            return true;
        }
        // parse version info as JSON data
        JSONObject jsonObject = new JSONObject(lineRead.toString());

        String versionOnServer = jsonObject.getString("version");
        String dateOnServer = jsonObject.getString("date");
        String descriptionOnServer = jsonObject.getString("description");

        if (versionOnServer.equals(VERSION)) {
            System.out.println("[OK] Version is up to date.");
            return true;
        }
        System.out.println("[!!] Running version is : " + VERSION);
        System.out.print(", but newer version is available: " + versionOnServer + ".");
        System.out.print("[!!] You have to use a newer one.");
        System.out.println(" Nothing has been done now.  ");
        System.out.println("[!!] Go to: " + MAIN_LOCATION_ON_SERVER);
        System.out.println("[info] Description of newer version: " + descriptionOnServer);
        System.out.println("[info] Date of realise of newer version: " + dateOnServer);
        return false;
    }

    private static void addStatisticData(String statisticsFolderPath) {
        StringBuilder fileStatisticPath = new StringBuilder();
        fileStatisticPath.append(statisticsFolderPath);
        fileStatisticPath.append(LocalDate.now().toString());
        fileStatisticPath.append(".csv");

        //FIXME System.out.println(fileStatisticPath.toString());

        try (BufferedWriter bf = new BufferedWriter(new FileWriter(fileStatisticPath.toString(), true))) {
            bf.write(System.getProperty("user.name") + ";" +
                    LocalDate.now().toString() + ";" +
                    LocalTime.now().toString() + ";" +
                    "1\n");
        } catch (IOException exc) {
            System.out.println("[!!] Problem with target file - statistics was not written.");
        }
    }

    private static String[] getArgsForTests() {
        List<String> argsForTestsList = new ArrayList<>();

//        String csvFile1ForTest = "e:\\JAVA\\_ECE\\05_BOLTS_EXC\\JAVA_APP\\01_EXCEL_TESTS\\src\\com\\michalrys\\boltab\\MULTI_CSV_EXAMPLE.csv";
//        String csvFile2ForTest = "e:\\JAVA\\_ECE\\05_BOLTS_EXC\\JAVA_APP\\01_EXCEL_TESTS\\src\\com\\michalrys\\boltab\\OTHER_CBAR_CSV_EXAMPLE.csv";
//        String csvFile3WrongDataForTest = "e:\\JAVA\\_ECE\\05_BOLTS_EXC\\JAVA_APP\\01_EXCEL_TESTS\\src\\com\\michalrys\\boltab\\OTHER_WRONG_CSV_EXAMPLE.csv";
//        String csvFile4RBE3DataForTest = "e:\\JAVA\\_ECE\\05_BOLTS_EXC\\JAVA_APP\\01_EXCEL_TESTS\\src\\com\\michalrys\\boltab\\RBE3_CSV_EXAMPLE.csv";

//        // quick check 1
//        argsForTestsList.add(csvFile1ForTest);
//        argsForTestsList.add(csvFile2ForTest);
//
//        // quick check 2
//        argsForTestsList.add(csvFile1ForTest);
//        argsForTestsList.add(csvFile3WrongDataForTest);
//        argsForTestsList.add(csvFile2ForTest);
//        argsForTestsList.add(csvFile4RBE3DataForTest);
//
//        // quick 3 check more tables
//        argsForTestsList.add(csvFile2ForTest);
//        argsForTestsList.add(csvFile2ForTest);
//        argsForTestsList.add(csvFile2ForTest);
//        argsForTestsList.add(csvFile2ForTest);
//        argsForTestsList.add(csvFile2ForTest);
//
//        // quick 4 other csv data from other people
//        argsForTestsList.add("e:\\JAVA\\_ECE\\05_BOLTS_EXC\\JAVA_APP\\01_EXCEL_TESTS\\src\\com\\michalrys\\boltab\\18017b_2018-10-05_frame_06V01_LC_CRA_CBAR_MR.csv");
//        argsForTestsList.add("e:\\JAVA\\_ECE\\05_BOLTS_EXC\\JAVA_APP\\01_EXCEL_TESTS\\src\\com\\michalrys\\boltab\\18017b_2018-10-05_frame_06V01_LC_FAT_CBAR_MR.csv");
//        argsForTestsList.add("e:\\JAVA\\_ECE\\05_BOLTS_EXC\\JAVA_APP\\01_EXCEL_TESTS\\src\\com\\michalrys\\boltab\\18017b_2018-10-05_frame_06V01_LC_FAT_prow_maznicy_07_MR.csv");
//        argsForTestsList.add("e:\\JAVA\\_ECE\\05_BOLTS_EXC\\JAVA_APP\\01_EXCEL_TESTS\\src\\com\\michalrys\\boltab\\18017b_2018-10-05_frame_06V01_LC_FAT_RBE3_MR.csv");
//        argsForTestsList.add("e:\\JAVA\\_ECE\\05_BOLTS_EXC\\JAVA_APP\\01_EXCEL_TESTS\\src\\com\\michalrys\\boltab\\18017b_2018-10-05_frame_06V01_LC_STA_CBAR_MR.csv");
//        argsForTestsList.add("e:\\JAVA\\_ECE\\05_BOLTS_EXC\\JAVA_APP\\01_EXCEL_TESTS\\src\\com\\michalrys\\boltab\\18017b_2018-10-05_frame_06V01_LC_STA_CRA_CBAR_MR.csv");
//        argsForTestsList.add("e:\\JAVA\\_ECE\\05_BOLTS_EXC\\JAVA_APP\\01_EXCEL_TESTS\\src\\com\\michalrys\\boltab\\18017b_2018-10-05_frame_06V01_LC_STA_CRA_RBE3_MR.csv");
//
//        // quick 5 problematic csv from K
//        argsForTestsList.add("e:\\JAVA\\_ECE\\05_BOLTS_EXC\\JAVA_APP\\01_EXCEL_TESTS\\src\\com\\michalrys\\boltab\\18017b_2018-11-06_frame_07V02_LC_CRA_CBAR_MR.csv");
//
//        // quick 6 not csv file as input
        //argsForTestsList.add("e:\\JAVA\\_ECE\\05_BOLTS_EXC\\JAVA_APP\\01_EXCEL_TESTS\\src\\com\\michalrys\\boltab\\BolTab.json");
//
        // quick 7 another files - from K and A
        //argsForTestsList.add("e:\\JAVA\\_ECE\\05_BOLTS_EXC\\JAVA_APP\\01_EXCEL_TESTS\\src\\com\\michalrys\\boltab\\csv\\my_csv.csv");
        //argsForTestsList.add("e:\\JAVA\\_ECE\\05_BOLTS_EXC\\JAVA_APP\\01_EXCEL_TESTS\\src\\com\\michalrys\\boltab\\18017b_2018-11-06_frame_07V02_LC_CRA_CBAR_MR.csv");
        //argsForTestsList.add("e:\\JAVA\\_ECE\\05_BOLTS_EXC\\JAVA_APP\\01_EXCEL_TESTS\\src\\com\\michalrys\\boltab\\18017b_2018-11-06_frame_07V02_LC_STA_CBAR_MR.txt");

        // quick 8 : check for customization
//        argsForTestsList.add("e:\\JAVA\\_ECE\\05_BOLTS_EXC\\JAVA_APP\\01_EXCEL_TESTS\\src\\com\\michalrys\\boltab\\18017b_2018-11-06_frame_07V02_LC_STA_CBAR_MR.csv");
//        argsForTestsList.add("e:\\JAVA\\_ECE\\05_BOLTS_EXC\\JAVA_APP\\01_EXCEL_TESTS\\src\\com\\michalrys\\boltab\\RBE3_CSV_EXAMPLE.csv");
//        argsForTestsList.add("e:\\JAVA\\_ECE\\05_BOLTS_EXC\\JAVA_APP\\01_EXCEL_TESTS\\src\\com\\michalrys\\boltab\\CBAR_TEST.csv");
//        argsForTestsList.add("e:\\JAVA\\_ECE\\05_BOLTS_EXC\\JAVA_APP\\01_EXCEL_TESTS\\src\\com\\michalrys\\boltab\\OTHER_CBAR_CSV_EXAMPLE.csv");
        argsForTestsList.add("e:\\JAVA\\_ECE\\05_BOLTS_EXC\\JAVA_APP\\01_EXCEL_TESTS\\src\\com\\michalrys\\boltab\\18017b_2018-10-05_frame_06V01_LC_STA_CRA_CBAR_MR.csv");

        return (String[]) argsForTestsList.toArray(new String[0]);
    }
}
